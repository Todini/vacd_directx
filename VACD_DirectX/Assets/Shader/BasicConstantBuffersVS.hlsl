/////////////
// GLOBALS //
/////////////
static const int MAXIMUM_STATIC_LIGHTS = 8;
static const int MAXIMUM_DYNAMIC_LIGHTS = 4;

//////////////////////
// CONSTANT BUFFERS //
//////////////////////
cbuffer CB_BasicMatrices : register(b0)
{
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer CB_BasicStaticLights : register(b1)
{
	matrix staticLightViewMatrix[MAXIMUM_STATIC_LIGHTS];
	matrix staticLightProjectionMatrix[MAXIMUM_STATIC_LIGHTS];
	float3 staticLightPosition[MAXIMUM_STATIC_LIGHTS];
	uint staticLightCount;
	float3 staticEyePosition;
	float PAD10;
}

cbuffer CB_BasicDynamicLights : register(b2)
{
	matrix dynamicLightViewMatrix[MAXIMUM_DYNAMIC_LIGHTS];
	matrix dynamicLightProjectionMatrix[MAXIMUM_DYNAMIC_LIGHTS];
	float3 dynamicLightPosition[MAXIMUM_DYNAMIC_LIGHTS];
	uint dynamicLightCount;
	float3 dynamicEyePosition;
	float PAD20;
}

cbuffer CB_Stuff3 : register(b3)
{

}