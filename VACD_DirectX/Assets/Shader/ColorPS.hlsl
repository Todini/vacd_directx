///////////////////////////////////
// TYPEDEFS
///////////////////////////////////
struct PSInputType
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float3 normal : NORMAL;
};

///////////////////////////////////
// Pixel Shader
///////////////////////////////////
float4 ColorShader(PSInputType input) : SV_TARGET
{
	//Basic directional lightning
	//Invert the light direction for calculations
	float3 lightDir = float3(0.25f, 0.5f, 0.5f);
	lightDir = normalize(lightDir);

	//float lightIntensity = saturate(dot(input.normal, lightDir));
	float lightIntensity = saturate(dot(input.normal, lightDir) * 0.9f);

	//Determine the final amount of diffuse color based on the diffuse color combined with the light intensity
	float4 color = saturate(input.color * lightIntensity);
	
	return color;
	//return input.color;
}