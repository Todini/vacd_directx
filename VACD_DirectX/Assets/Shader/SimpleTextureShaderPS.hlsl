#include "BasicSamplers.hlsl"

Texture2D texDiffuse : register(t0);

///////////////////////////////////
// TYPEDEFS
///////////////////////////////////
struct PSInputType
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 tex : TEXCOORD0;
};

///////////////////////////////////
// Pixel Shader
///////////////////////////////////
float4 SimpleTextureShader(PSInputType input) : SV_TARGET
{
	//Sample the pixel color from the texture using the sampler at this texture coordinate location
	float4 textureColor = texDiffuse.Sample(samplerMipLinearWrap, input.tex);

	return textureColor;
}