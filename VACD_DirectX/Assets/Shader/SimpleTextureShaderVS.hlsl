#include "BasicConstantBuffersVS.hlsl"

//////////////////////
// CONSTANT BUFFERS //
//////////////////////
cbuffer CB_Matrices : register(b4)
{
	matrix worldMatrix;
};

//////////////
// TYPEDEFS //
//////////////
struct VSInputType
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float2 tex : TEXCOORD0;
};

struct VSOutputType
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 tex : TEXCOORD0;
};

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
VSOutputType SimpleTextureShader(VSInputType input)
{
	VSOutputType output;

	//Change the position vector to be 4 units for proper matrix calculations.
	float4 inputPosition = float4(input.position, 1.0f);

	//Calculate the position of the vertex against world, view and projection matrices
	output.position = mul(inputPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	//Store the texture coordinates for the pixel shader.
	output.tex = input.tex;

	//Calculate the normal vector against the world matrix only.
	output.normal = mul(input.normal, (float3x3)worldMatrix);
	//Normalize the normal vector.
	output.normal = normalize(output.normal);

	return output;
}
