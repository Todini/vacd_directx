#pragma once
#include "Components\Transform.h"
#include "Rendering\Mesh.h"
#include "File\MeshNames.h"
#include "EngineIDs.h"

#include "Rendering\MaterialManager.h"
#include "Physics\RigidBody.h"

namespace ICE
{
	class World;

	template <typename VertexType>
	class Actor
	{
	public:

		Actor(EMeshName_t meshName, EMovement_t movement)
			: transform(GetWorld()->GetNewTransform())
			, meshID(GetWorld()->CreateNewMesh<VertexType>(meshName))
			, rigidBody(GetWorld()->GetNewRigidBody(transform, meshName, movement))
		{
			RENDER::Mesh<VertexType>* mesh = GetWorld()->GetMesh<VertexType>(meshID);
			mesh->LinkWorldMatrix(transform->GetWorldMatrixPtr());

			const RENDER::Material* material = RENDER::GetDefaultMaterial();
			mesh->SetMaterial(material);
		}

		Actor(EMeshName_t meshName, EMovement_t movement, float posX, float posY, float posZ)
			: transform(GetWorld()->GetNewTransform(posX, posY, posZ))
			, meshID(GetWorld()->CreateNewMesh<VertexType>(meshName))
			, rigidBody(GetWorld()->GetNewRigidBody(transform, meshName, movement))
		{
			RENDER::Mesh<VertexType>* mesh = GetWorld()->GetMesh<VertexType>(meshID);
			mesh->LinkWorldMatrix(transform->GetWorldMatrixPtr());

			const RENDER::Material* material = RENDER::GetDefaultMaterial();
			mesh->SetMaterial(material);
		}

		Actor(EMeshName_t meshName, const RENDER::MaterialInfo matInfo, EMovement_t movement)
			: transform(GetWorld()->GetNewTransform())
			, meshID(GetWorld()->CreateNewMesh<VertexType>(meshName))
			, rigidBody(GetWorld()->GetNewRigidBody(transform, meshName, movement))
		{
			RENDER::Mesh<VertexType>* mesh = GetWorld()->GetMesh<VertexType>(meshID);
			mesh->LinkWorldMatrix(transform->GetWorldMatrixPtr());

			const RENDER::Material* material = RENDER::CreateMaterial(matInfo);
			mesh->SetMaterial(material);
		}
		
		Actor(EMeshName_t meshName, RENDER::MaterialInfo matInfo, EMovement_t movement, float posX, float posY, float posZ)
			: transform(GetWorld()->GetNewTransform(posX, posY, posZ))
			, meshID(GetWorld()->CreateNewMesh<VertexType>(meshName))
			, rigidBody(GetWorld()->GetNewRigidBody(transform, meshName, movement))
		{
			RENDER::Mesh<VertexType>* mesh = GetWorld()->GetMesh<VertexType>(meshID);
			mesh->LinkWorldMatrix(transform->GetWorldMatrixPtr());

			const RENDER::Material* material = RENDER::CreateMaterial(matInfo);
			mesh->SetMaterial(material);
		}
		

	public:
		Transform* transform;
		PHYS::RigidBody* rigidBody;
		MeshID meshID;


	private:


	};
}