#include "Transform.h"
#include "..\Utility\MathUtility.h"

//#define DEGREETORAD 0.01745329251994329576923690768489f
  #define DEGREETORAD 0.01745329252f

using namespace DirectX;

namespace ICE
{
	Transform::Transform()
		//: m_position(XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f))
		//, m_scale(XMVectorSet(1.0f, 1.0f, 1.0f, 0.0f))
		//, m_rotationAngles(0.0f, 0.0f, 0.0f)
		: m_position({ 0.0f, 0.0f, 0.0f })
		, m_scale({ 1.0f, 1.0f, 1.0f })
		, m_rotationAngles({ 0.0f, 0.0f, 0.0f })
		, m_rotationQuaternion(XMQuaternionIdentity())
		, m_worldMatrix(XMMatrixIdentity())
	{
		UpdateWorldMatrix();
	}

	Transform::Transform(float posX, float posY, float posZ)
		//: m_position(XMVectorSet(posX, posY, posZ, 1.0f))
		//, m_scale(XMVectorSet(1.0f, 1.0f, 1.0f, 0.0f))
		//, m_rotationAngles(0.0f, 0.0f, 0.0f)
		: m_position({ posX, posY, posZ })
		, m_scale({ 1.0f, 1.0f, 1.0f })
		, m_rotationAngles({ 0.0f, 0.0f, 0.0f })
		, m_rotationQuaternion(XMQuaternionIdentity())
		, m_worldMatrix(XMMatrixIdentity())
	{
		UpdateWorldMatrix();
	}

	Transform::Transform(XMFLOAT3 pos, XMFLOAT3 scale, XMFLOAT3 rotAngles)
		//: m_position(XMVectorSet(pos.x, pos.y, pos.z, 1.0f))
		//, m_scale(XMVectorSet(scale.x, scale.y, scale.z, 0.0f))
		//, m_rotationAngles(rotAngles)
		: m_position({ pos.x, pos.y, pos.z })
		, m_scale({ scale.x, scale.y, scale.z })
		, m_rotationAngles({ rotAngles.x, rotAngles.y, rotAngles.z })
		, m_rotationQuaternion(XMQuaternionIdentity())
		, m_worldMatrix(XMMatrixIdentity())
	{
		UpdateWorldMatrix();
	}

	Transform::Transform(const Transform & other)
	{
		m_position = other.m_position;
		m_scale = other.m_scale;
		m_rotationAngles = other.m_rotationAngles;
		m_rotationQuaternion = other.m_rotationQuaternion;
		m_worldMatrix = other.m_worldMatrix;
	}


	Transform::~Transform()
	{
	}

	void Transform::SetPosition(float x, float y, float z)
	{
		//SetPosition(XMVectorSet(x, y, z, 1.0f));
		m_position.x = x;
		m_position.y = y;
		m_position.z = z;
		UpdateWorldMatrix();
	}

	void Transform::SetPosition(Vec3 position)
	{
		m_position = position;
		UpdateWorldMatrix();
	}

	void Transform::SetPosition(DirectX::XMFLOAT3 newPos)
	{
		//SetPosition(XMVectorSet(newPos.x, newPos.y, newPos.z, 1.0f));
		SetPosition(newPos.x, newPos.y, newPos.z);
	}

	void Transform::SetPosition(DirectX::XMFLOAT3A newPos)
	{
		//SetPosition(XMVectorSet(newPos.x, newPos.y, newPos.z, 1.0f));
		SetPosition(newPos.x, newPos.y, newPos.z);
	}

	void Transform::SetPosition(DirectX::XMVECTOR newPos)
	{
		//m_position = newPos;
		//UpdateWorldMatrix();
		SetPosition(XMVectorGetX(newPos), XMVectorGetY(newPos), XMVectorGetZ(newPos));
	}

	void Transform::AddPosition(float x, float y, float z)
	{
		//AddPosition(XMVectorSet(x, y, z, 0.0f));
		m_position.x += x;
		m_position.y += y;
		m_position.z += z;
		UpdateWorldMatrix();
	}

	void Transform::AddPosition(Vec3 translation)
	{
		m_position += translation;
		UpdateWorldMatrix();
	}

	void Transform::AddPosition(DirectX::XMFLOAT3 moveBy)
	{
		//AddPosition(XMVectorSet(moveBy.x, moveBy.y, moveBy.z, 0.0f));
		AddPosition(moveBy.x, moveBy.y, moveBy.z);
	}

	void Transform::AddPosition(DirectX::XMVECTOR moveVector)
	{
		//m_position += moveVector;
		//UpdateWorldMatrix();
		AddPosition(XMVectorGetX(moveVector), XMVectorGetY(moveVector), XMVectorGetZ(moveVector));
	}

	void Transform::SetRotationAngles(float x, float y, float z)
	{
		m_rotationAngles.x = x;
		m_rotationAngles.y = y;
		m_rotationAngles.z = z;
		m_rotationQuaternion = XMQuaternionRotationRollPitchYaw(m_rotationAngles.y * DEGREETORAD, m_rotationAngles.x * DEGREETORAD, m_rotationAngles.z * DEGREETORAD);
		UpdateWorldMatrix();
	}

	void Transform::SetRotationAngles(DirectX::XMFLOAT3 newRotation)
	{
		SetRotationAngles(newRotation.x, newRotation.y, newRotation.z);
	}

	void Transform::SetRotationAngles(DirectX::XMVECTOR rotation)
	{
		SetRotationAngles(XMVectorGetX(rotation), XMVectorGetY(rotation), XMVectorGetZ(rotation));
	}

	void Transform::AddRotationAngles(float x, float y, float z)
	{
		m_rotationAngles.x += x;
		m_rotationAngles.y += y;
		m_rotationAngles.z += z;
		m_rotationQuaternion = XMQuaternionRotationRollPitchYaw(m_rotationAngles.y * DEGREETORAD, m_rotationAngles.x * DEGREETORAD, m_rotationAngles.z * DEGREETORAD);
		UpdateWorldMatrix();
	}

	void Transform::AddRotationAngles(DirectX::XMFLOAT3 rotateBy)
	{
		AddRotationAngles(rotateBy.x, rotateBy.y, rotateBy.z);
	}

	void Transform::AddRotationAngles(DirectX::XMVECTOR rotation)
	{
		AddRotationAngles(XMVectorGetX(rotation), XMVectorGetY(rotation), XMVectorGetZ(rotation));
	}

	void Transform::SetRotation(DirectX::XMVECTOR newRotationQuaternion)
	{
		m_rotationQuaternion = newRotationQuaternion;

		//TODO: UPDATE m_rotationAngles!!!!!

		UpdateWorldMatrix();
	}

	void Transform::SetScale(float x, float y, float z)
	{
		//SetScale(XMVectorSet(x, y, z, 0.0f));
		m_scale.x = x;
		m_scale.y = y;
		m_scale.z = z;
		UpdateWorldMatrix();
	}

	void Transform::SetScale(DirectX::XMFLOAT3 newScale)
	{
		//SetScale(XMVectorSet(newScale.x, newScale.y, newScale.z, 0.0f));
		SetScale(newScale.x, newScale.y, newScale.z);
	}

	void Transform::SetScale(DirectX::XMVECTOR newScale)
	{
		//m_scale = newScale;
		//UpdateWorldMatrix();
		SetScale(XMVectorGetX(newScale), XMVectorGetY(newScale), XMVectorGetZ(newScale));
	}

	void Transform::AddScale(float x, float y, float z)
	{
		//AddScale(XMVectorSet(x, y, z, 0.0f));
		m_scale.x = x;
		m_scale.y = y;
		m_scale.z = z;
		UpdateWorldMatrix();
	}

	void Transform::AddScale(DirectX::XMFLOAT3 addScale)
	{
		//AddScale(XMVectorSet(addScale.x, addScale.y, addScale.z, 0.0f));
		AddScale(addScale.x, addScale.y, addScale.z);
	}

	void Transform::AddScale(DirectX::XMVECTOR addScale)
	{
		//m_scale += addScale;
		//UpdateWorldMatrix();
		AddScale(XMVectorGetX(addScale) , XMVectorGetY(addScale), XMVectorGetZ(addScale));
	}

	Vec3 Transform::GetDirection()
	{
		return MATH::AnglesToDirection(m_rotationAngles);
	}

	/*
	float Transform::GetMaxSphereRadius()
	{
		float x = XMVectorGetX(m_scale);
		float y = XMVectorGetY(m_scale);
		float z = XMVectorGetZ(m_scale);

		return MATH::Length(x, y, z) / 2.0f;
	}
	*/

	XMVECTOR Transform::Forward()
	{
		static XMVECTOR m_forward = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
		return m_forward;
	}

	XMVECTOR Transform::Up()
	{
		static XMVECTOR m_up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		return m_up;
	}

	XMVECTOR Transform::Right()
	{
		static XMVECTOR m_right = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);
		return m_right;
	}

	void Transform::UpdateWorldMatrix()
	{
		//m_worldMatrix = XMMatrixTransformation(m_position, m_rotationQuaternion, m_scale, m_position, m_rotationQuaternion, m_position);
		XMVECTOR position = XMVectorSet(m_position.x, m_position.y, m_position.z, 1.0f);
		XMVECTOR scale = XMVectorSet(m_scale.x, m_scale.y, m_scale.z, 0.0f);

		m_worldMatrix = XMMatrixTransformation(XMVectorZero(), XMQuaternionIdentity(), scale, XMVectorZero(), m_rotationQuaternion, position);
	}

}