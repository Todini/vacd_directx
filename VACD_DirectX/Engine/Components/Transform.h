#pragma once
#include <DirectXMath.h>
#include "..\Utility\Vec.h"

//TODO: Add forward, up and right functions with static variables that are affected by rotation

namespace ICE
{

	class Transform
	{
	public:
		Transform();
		Transform(float posX, float posY, float posZ);
		Transform(DirectX::XMFLOAT3 pos, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 rotationAngles);
		Transform(const Transform& other);
		~Transform();

		//SETS
		void SetPosition(float x, float y, float z);
		void SetPosition(Vec3 position);
		void SetPosition(DirectX::XMFLOAT3 newPos);
		void SetPosition(DirectX::XMFLOAT3A newPos);
		void SetPosition(DirectX::XMVECTOR newPos);
		void AddPosition(float x, float y, float z);
		void AddPosition(Vec3 translation);
		void AddPosition(DirectX::XMFLOAT3 moveBy);
		void AddPosition(DirectX::XMVECTOR moveVector);

		//Rotation angles will be set normally. Quaternion creation has x/y flipped (y/x/z)
		void SetRotationAngles(float x, float y, float z);
		void SetRotationAngles(DirectX::XMFLOAT3 newRotation);
		void SetRotationAngles(DirectX::XMVECTOR newRotation);
		void AddRotationAngles(float x, float y, float z);
		void AddRotationAngles(DirectX::XMFLOAT3 rotateBy);
		void AddRotationAngles(DirectX::XMVECTOR rotateBy);

		void SetRotation(DirectX::XMVECTOR newRotationQuaternion);

		void SetScale(float x, float y, float z);
		void SetScale(DirectX::XMFLOAT3 newScale);
		void SetScale(DirectX::XMVECTOR newScale);
		void AddScale(float x, float y, float z);
		void AddScale(DirectX::XMFLOAT3 addScale);
		void AddScale(DirectX::XMVECTOR addScale);

		//GETS
		//Returns the world position
		//inline DirectX::XMVECTOR GetPosition()
		inline Vec3 GetPosition()
		{
			return m_position;
		}
		inline DirectX::XMVECTOR GetPositionVector()
		{
			return DirectX::XMVectorSet(m_position.x, m_position.y, m_position.z, 1.0f);
		}
		////Returns the world position as a freaking XMFLOAT3
		//inline DirectX::XMFLOAT3 GetPositionFloat()
		//{
		//	return DirectX::XMFLOAT3(DirectX::XMVectorGetX(m_position), DirectX::XMVectorGetY(m_position), DirectX::XMVectorGetZ(m_position));
		//}
		////Returns the world position X
		//inline float GetPositionX()
		//{
		//	return DirectX::XMVectorGetX(m_position);
		//}
		////Returns the world position Y
		//inline float GetPositionY()
		//{
		//	return DirectX::XMVectorGetY(m_position);
		//}
		////Returns the world position Z
		//inline float GetPositionZ()
		//{
		//	return DirectX::XMVectorGetZ(m_position);
		//}

		//Returns the rotation Quaternion
		inline DirectX::XMVECTOR GetRotation()
		{
			return m_rotationQuaternion;
		}
		inline Vec3 GetRotationAngles()
		{
			return m_rotationAngles;
		}
		Vec3 GetDirection();
		//Returns the scale
		inline Vec3 GetScale()
		{
			return m_scale;
		}
		//Returns the position matrix of the object
		inline DirectX::XMMATRIX GetWorldMatrix()
		{
			return m_worldMatrix;
		}

		inline const DirectX::XMMATRIX* GetWorldMatrixPtr()
		{
			return &m_worldMatrix;
		}

		/*
		inline float GetBoundBoxHalfX()
		{	return DirectX::XMVectorGetX(m_scale) / 2.0f;	}
		inline float GetBoundBoxHalfY()
		{	return DirectX::XMVectorGetY(m_scale) / 2.0f;	}
		inline float GetBoundBoxHalfZ()
		{	return DirectX::XMVectorGetZ(m_scale) / 2.0f;	}
		inline DirectX::XMFLOAT3 GetBoundBoxHalf()
		{	return DirectX::XMFLOAT3(GetBoundBoxHalfX(), GetBoundBoxHalfY(), GetBoundBoxHalfZ());	}
		float GetMaxSphereRadius();
		*/

		DirectX::XMVECTOR Forward();
		DirectX::XMVECTOR Up();
		DirectX::XMVECTOR Right();

	private:
		//DirectX::XMVECTOR m_position;
		Vec3 m_position;
		Vec3 m_scale;
		Vec3 m_rotationAngles;
		float pad[3];
		DirectX::XMVECTOR m_rotationQuaternion;

		DirectX::XMMATRIX m_worldMatrix;

		void UpdateWorldMatrix();
	};
}