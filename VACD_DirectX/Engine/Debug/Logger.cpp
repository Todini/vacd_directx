#include "Logger.h"

namespace ICE
{
	namespace LOG
	{
#if WINDOWS_OUTPUT
		void log()
		{
			OutputDebugString("\r\n");
		}

		/*
		template<typename ...Rest>
		void log(uint32_t&& first, Rest&& ...rest)
		{
			std::string s = std::forward<First>(std::to_string(first));
			OutputDebugString(s.c_str());
			log(std::forward<Rest>(rest)...);
		}
		*/
#else
		void log(std::ostream& stream)
		{
			stream << std::endl;
		}
#endif
	}
}