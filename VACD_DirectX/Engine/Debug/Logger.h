
#ifndef LOGGER_H
#define LOGGER_H
#include <string>
#define WINDOWS_OUTPUT 1
#if WINDOWS_OUTPUT
	#include <Windows.h>
#else
	#include <iostream>
#endif

namespace ICE
{
	namespace LOG
	{
#if WINDOWS_OUTPUT
		void log();

		template<typename First, typename ...Rest>
		void log(First&& first, Rest&& ...rest)
		{
			std::string s = std::forward<First>(first);
			OutputDebugString(s.c_str());
			log(std::forward<Rest>(rest)...);
		}
#else
		void log(std::ostream& stream);

		template<typename First, typename ...Rest>
		void log(std::ostream& stream, First&& first, Rest&& ...rest)
		{
			stream << std::forward<First>(first);
			log(stream, std::forward<Rest>(rest)...);
		}
#endif
	}
}

#if WINDOWS_OUTPUT
	#define ICE_LOG(message, ...) {	\
		ICE::LOG::log(message, __VA_ARGS__);	}

	#define ICE_LOG_ERR(message, ...) {	\
		ICE::LOG::log(message, __VA_ARGS__);	}
#else
	#define ICE_LOG(message, ...) {	\
		ICE::LOG::log(std::cout, message, __VA_ARGS__);	}

	#define ICE_LOG_ERR(message, ...) {	\
		ICE::LOG::log(std::cerr, message, __VA_ARGS__);	}
#endif

#endif