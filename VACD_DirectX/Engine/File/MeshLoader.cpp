#include "MeshLoader.h"
#include <assert.h>

#include "..\Memory\MemorySystem.h"
#include "..\Debug\Logger.h"

#include "..\Physics\CollisionDataGenerator.h"

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "..\..\TP\tiny_obj_loader.h"

namespace ICE
{
	namespace FILE
	{
		namespace
		{
			static const char* BASE_DIR_MESHES = "Meshes//";

			static MEMORY::HeapArea m_heapArea(1024 * 1024 * 5);
			static LoaderArena m_memoryArena(m_heapArea);
			static MeshData m_meshDataArray[EMeshName_t::SIZE];

			static bool m_isInit = false;
		}

		namespace
		{
			static void LoadFile(char* file, EMeshName_t meshName)
			{
				if (m_meshDataArray[meshName].isInit == true)
				{
					//That mesh is already loaded!
					ICE_LOG_ERR("The Mesh is already loaded!");
					assert(false);
				}

				tinyobj::attrib_t attrib;
				std::vector<tinyobj::shape_t> shapes;	
				std::vector<tinyobj::material_t> materials;

				std::string err;
				//Triangulate -> Always returns faces with 3 vertices!
				tinyobj::LoadObj(&attrib, &shapes, &materials, &err, file, BASE_DIR_MESHES, true);

				if (!err.empty()) 
				{ // `err` may contain warning message.
					//OutputDebugString(err.c_str());
					ICE_LOG_ERR(err);
				}
				

				// Loop over shapes
				size_t shapeSize = shapes.size();
				for (size_t s = 0; s < shapeSize; s++) 
				{
					//Currently only supporting one shape per mesh!!!
					assert(s == 0);

					MeshData meshData;
					meshData.indexCount = static_cast<uint32_t>(shapes[s].mesh.indices.size());
					meshData.indices = ICE_NEW_ARRAY((&m_memoryArena), MEMORY::NameTag::MESH_LOADER, uint32_t, meshData.indexCount);
					meshData.vertexCount = meshData.indexCount;
					meshData.vertices = ICE_NEW_ARRAY((&m_memoryArena), MEMORY::NameTag::MESH_LOADER, VTPosNorTex, meshData.vertexCount);

					uint32_t* currentIndex = meshData.indices;
					VTPosNorTex* currentVertex = meshData.vertices;

					// Loop over faces(polygon)
					size_t numFaces = shapes[s].mesh.num_face_vertices.size();

					size_t index_offset = 0;
					for (size_t f = 0; f < numFaces; f++) 
					{
						unsigned char numVerticesOnFace = shapes[s].mesh.num_face_vertices[f];
						assert(numVerticesOnFace == 3u);

						// Loop over vertices in the face.
						for (size_t v = 0; v < numVerticesOnFace; v++)
						{
							// access to vertex
							/*
							tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
							tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
							tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
							tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
							tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
							tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
							tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];
							tinyobj::real_t tx = attrib.texcoords[2 * idx.texcoord_index + 0];
							tinyobj::real_t ty = attrib.texcoords[2 * idx.texcoord_index + 1];
							*/

							tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
							(*currentIndex) = static_cast<uint32_t>(index_offset + v);
							currentIndex++;

							currentVertex->position.x = attrib.vertices[3 * idx.vertex_index + 0];
							currentVertex->position.y = attrib.vertices[3 * idx.vertex_index + 1];
							currentVertex->position.z = attrib.vertices[3 * idx.vertex_index + 2];
							currentVertex->normal.x = attrib.normals[3 * idx.normal_index + 0];
							currentVertex->normal.y = attrib.normals[3 * idx.normal_index + 1];
							currentVertex->normal.z = attrib.normals[3 * idx.normal_index + 2];
							if (idx.texcoord_index != -1)
							{
								currentVertex->texcoords.x = attrib.texcoords[2 * idx.texcoord_index + 0];
								currentVertex->texcoords.y = attrib.texcoords[2 * idx.texcoord_index + 1];
							}
							else
							{
								currentVertex->texcoords.x = 0u;
								currentVertex->texcoords.y = 0u;
							}

							currentVertex++;
						}

						index_offset += numVerticesOnFace;

						// per-face material
						shapes[s].mesh.material_ids[f];
					}

					
					m_meshDataArray[meshName] = meshData;
					m_meshDataArray[meshName].isInit = true;
				}

				//Generate the CollisionData for the Mesh
				ICE::PHYS::CreateCollisionDataForMeshName(meshName, m_meshDataArray[meshName].vertices, m_meshDataArray[meshName].vertexCount);

			} //LoadFile

		}

		void InitMeshLoader()
		{
			assert(m_isInit == false);
			m_isInit = true;

			LoadFile("Meshes//cube.obj", EMeshName_t::CUBE);
			LoadFile("Meshes//plane.obj", EMeshName_t::PLANE);
			//LoadFile("Meshes//Orbital_Lander.obj", MeshName::TEST01);
		}

		void ShutdownMeshLoader()
		{
			assert(m_isInit == true);
			m_isInit = false;

			size_t meshMaxSize = EMeshName_t::SIZE;
			for (size_t i = 0u; i < meshMaxSize; i++)
			{
				MeshData& meshData = m_meshDataArray[i];

				if (meshData.isInit == true)
				{
					ICE_DELETE_ARRAY((&m_memoryArena), meshData.vertices);
					ICE_DELETE_ARRAY((&m_memoryArena), meshData.indices);
				}
			}
		}

		const MeshData& LoadMesh(EMeshName_t meshName)
		{
			if (m_meshDataArray[meshName].isInit == true)
			{
				return m_meshDataArray[meshName];
			}
			
			assert(false);
			return m_meshDataArray[0];
		}
		

	}
}