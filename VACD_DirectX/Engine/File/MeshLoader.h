#pragma once
#include "MeshNames.h"
#include "..\Rendering\VertexTypes.h"

namespace ICE
{
	namespace FILE
	{
		struct MeshData
		{
			bool isInit = false;

			uint32_t vertexCount;
			VTPosNorTex* vertices;

			uint32_t indexCount;
			uint32_t* indices;
		};

		void InitMeshLoader();
		void ShutdownMeshLoader();

		const MeshData& LoadMesh(EMeshName_t meshName);

	}
}