#include "MeshNames.h"

namespace ICE
{
	namespace FILE
	{
		const char* EMeshName::ToString(EMeshName::Enum e)
		{
			switch (e)
			{
			case CUBE:
			{
				return "Cube";
			} break;
			case PLANE:
			{
				return "Plane";
			} break;
			case TEST01:
			{
				return "Test01";
			} break;
			default:
			{
				return "MeshName::INVALID";
			} break;
			}
		}
	}
}