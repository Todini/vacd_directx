#pragma once
#include <stdint.h>

namespace ICE
{
	namespace FILE
	{
		struct EMeshName
		{
			enum Enum : uint8_t
			{
				CUBE,
				PLANE,
				TEST01,
				SIZE,
				INVALID
			};

			const char* ToString(EMeshName::Enum e);
		};
	}
	typedef FILE::EMeshName::Enum EMeshName_t;
}