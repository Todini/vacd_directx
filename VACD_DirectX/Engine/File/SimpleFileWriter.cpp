#include "SimpleFileWriter.h"

#include <fstream>
#include <string>
#include <iostream>

namespace ICE
{
	namespace
	{
		const std::string filename = "timer.txt";
	}

	namespace FILE
	{
		void WriteToFile(std::string text)
		{
			std::ofstream out(filename);
			out << text;
			out.close();
		}
	}
}