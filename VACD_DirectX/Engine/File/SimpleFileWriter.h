#pragma once
#include <string>

namespace ICE
{
	namespace FILE
	{
		void WriteToFile(std::string text);
	}
}