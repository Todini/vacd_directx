#include "TextureLoader.h"
#include <assert.h>

#include "..\Rendering\Graphics.h"

#include "..\..\TP\DDSTextureLoader.h"
// http://blogs.msdn.com/b/chuckw/archive/2011/10/28/directxtex.aspx
// https://github.com/Microsoft/DirectXTex

namespace ICE
{
	namespace FILE
	{
		namespace
		{
			static const char* BASE_DIR_TEXTURES = "Textures//";

			static ID3D11ShaderResourceView* m_textures[ETextureName_t::SIZE];

			static bool m_isInit = false;
		}


		namespace
		{
			void LoadDDS(const wchar_t* filename, ETextureName_t textureName)
			{
				HRESULT result = DirectX::CreateDDSTextureFromFile(RENDER::GGetDevice(), filename, nullptr, (m_textures + textureName), 0u, nullptr);
				assert(SUCCEEDED(result));
			}
		}


		void InitTextureLoader()
		{
			assert(m_isInit == false);
			m_isInit = true;

			m_textures[ETextureName_t::NONE] = nullptr;
			LoadDDS(L"Textures//leaves.dds", ETextureName_t::LEAVES);
			LoadDDS(L"Textures//stone01.dds", ETextureName_t::STONE01);
			LoadDDS(L"Textures//stone01_height.dds", ETextureName_t::STONE01_HEIGHT);
			LoadDDS(L"Textures//stone01_normal.dds", ETextureName_t::STONE01_NORMAL);
		}

		void ShutdownTextureLoader()
		{
			assert(m_isInit == true);
			m_isInit = false;

			//Skip the None-Element
			for (uint8_t i = 1u; i < ETextureName_t::SIZE; i++)
			{
				m_textures[i]->Release();
			}

		}

		ID3D11ShaderResourceView* GetTexture(ETextureName_t texName)
		{
			assert(m_isInit);
			assert(texName < ETextureName_t::SIZE);

			return m_textures[texName];
		}

	}
}