#pragma once
#include "TextureNames.h"

struct ID3D11ShaderResourceView;

namespace ICE
{
	namespace FILE
	{
		void InitTextureLoader();
		void ShutdownTextureLoader();

		ID3D11ShaderResourceView* GetTexture(ETextureName_t texName);
	}
}