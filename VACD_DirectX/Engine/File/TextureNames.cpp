#include "TextureNames.h"

namespace ICE
{
	namespace FILE
	{
		const char* ETextureName::ToString(ETextureName::Enum e)
		{
			switch (e)
			{
				case NONE:
				{
					return "None";
				} break;
				case STONE01:
				case STONE01_HEIGHT:
				case STONE01_NORMAL:
				{
					return "Stone01";
				} break;
				default:
				{
					return "TextureName::INVALID";
				} break;
			}
		}
	}
}
