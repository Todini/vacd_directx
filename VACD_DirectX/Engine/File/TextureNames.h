#pragma once
#include <stdint.h>

namespace ICE
{
	namespace FILE
	{
		struct ETextureName
		{
			enum Enum : uint8_t
			{
				NONE,
				LEAVES,
				STONE01,
				STONE01_HEIGHT,
				STONE01_NORMAL,
				SIZE,
				INVALID
			};

			const char* ToString(ETextureName::Enum e);
		};

	}
	typedef FILE::ETextureName::Enum ETextureName_t;

}