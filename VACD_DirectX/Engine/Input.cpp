#include "Input.h"
#include <windows.h>

//To satisfy the compiler and not to generate warnings that it defaults to version 8 of DirectInput
#define DIRECTINPUT_VERSION 0x0800
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#include <dinput.h>

#include <assert.h>
#include <utility>

namespace ICE
{
	namespace INPUT
	{
		Input* g_Input = nullptr;

		namespace
		{
			//.... I do not want to include the "dinput.h" in the header file. An the plan is to only use one input-instance.
			DIMOUSESTATE m_mouseState;
			DIMOUSESTATE m_mouseStateOld;
		}

		void Input::Init(HINSTANCE hInstance, HWND hwnd)
		{
			assert(g_Input == nullptr);
			g_Input = this;

			//Initialize the main direct input interface
			HRESULT result = DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_directInput, nullptr);
			if (FAILED(result)) { assert(!FAILED(result)); return; }

			// ------------ KEYBOARD -----------
			//Initialize the direct input interface for the keyboard
			result = m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, nullptr);
			if (FAILED(result)) { assert(!FAILED(result)); return; }
			//Set the data format. In this case since it is a keyboard we can use the predefined data format
			result = m_keyboard->SetDataFormat(&c_dfDIKeyboard);
			if (FAILED(result)) { assert(!FAILED(result)); return; }
			//Set the cooperative level of the keyboard to not share with other programs
			// http://www.rastertek.com/dx11tut13.html -> Setting to a different cooperative level is a bit finicky (windowed and focus-loss)
			result = m_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);
			if (FAILED(result)) { assert(!FAILED(result)); return; }
			//Now acquire the keyboard
			result = m_keyboard->Acquire();
			if (FAILED(result)) { return; }

			// ------------ MOUSE -----------
			//Initialize the direct input interface for the mouse
			result = m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, nullptr);
			if (FAILED(result)) { assert(!FAILED(result)); return; }
			//Set the data format for the mouse using the pre-defined mouse data format
			result = m_mouse->SetDataFormat(&c_dfDIMouse);
			if (FAILED(result)) { assert(!FAILED(result)); return; }
			//Set the cooperative level of the mouse to share with other programs
			result = m_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
			if (FAILED(result)) { assert(!FAILED(result)); return; }
			//Acquire the mouse 
			result = m_mouse->Acquire();
			if (FAILED(result)) { assert(!FAILED(result)); return; }

			m_sensitivity = 1.0f;


			//Set values for the player controls here (as i don't want to include dinput.h in the header-file)
			playerInputs.closeApplication = DIK_ESCAPE;

			playerInputs.moveForward = DIK_W;
			playerInputs.moveBackward = DIK_S;
			playerInputs.moveLeft = DIK_A;
			playerInputs.moveRight = DIK_D;
			playerInputs.moveUp = DIK_E;
			playerInputs.moveDown = DIK_Q;
			playerInputs.sprint = DIK_LSHIFT;

			playerInputs.raytraceCompounds = DIK_G;
			playerInputs.deleteSingleCompound = DIK_J;
			playerInputs.saveCompound = DIK_U;

			playerInputs.debugPause = DIK_P;
			playerInputs.debugNextFrame = DIK_O;


		}

		void Input::Shutdown()
		{
			assert(g_Input != nullptr);
			g_Input = nullptr;

			if (m_mouse)
			{
				m_mouse->Unacquire();
				m_mouse->Release();
			}
			if (m_keyboard)
			{
				m_keyboard->Unacquire();
				m_keyboard->Release();
			}
			if (m_directInput)
			{
				m_directInput->Release();
			}
		}

		void Input::Update()
		{
			HRESULT result;

			// ------------- Read Keyboard input -------------
			//In order to differentiate between "Key press" and "Keeping the key pressed" we need to save the old state of the keyboard
			std::swap(m_keyboardStateOld, m_keyboardState);
			/*	for (int i = 0; i < INPUT_KEY_SIZE; i++){	m_keyboardStateOld[i] = m_keyboardState[i];	}	*/

			//Read the Keyboard device
			result = m_keyboard->GetDeviceState(sizeof(m_keyboardState), (void*)&m_keyboardState);
			if (FAILED(result))
			{
				//If the keyboard lost focus or was not acquired then try to get control back
				if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
				{
					m_keyboard->Acquire();
				}
			}

			// ------------- Read Mouse input -------------
			//Read the mouse device
			std::swap(m_mouseStateOld, m_mouseState);
			result = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&m_mouseState);
			if (FAILED(result))
			{
				//If the mouse lost focus or was not acquired then try to get control back
				if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
				{
					m_mouse->Acquire();
				}
			}

			// ------------- Process input -------------
			//Save the delta-movement of the mouse0
			m_mouseDeltaX = static_cast<float>(m_mouseState.lX) * m_sensitivity;
			m_mouseDeltaY = static_cast<float>(m_mouseState.lY) * m_sensitivity;
		}

		bool Input::IsKeyPressed(uint32_t key)
		{
			return ((m_keyboardState[key] == 0x80));
		}

		bool Input::IsKeyPressedOnce(uint32_t key)
		{
			return ((m_keyboardState[key] == 0x80) && !(m_keyboardStateOld[key] == 0x80));
		}

	}
}