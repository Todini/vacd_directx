#pragma once
#include <stdint.h>

#ifndef _WINDEF_
struct HINSTANCE__;
typedef HINSTANCE__* HINSTANCE;
struct HWND__;
typedef HWND__* HWND;
#endif

struct IDirectInput8A;
typedef IDirectInput8A IDirectInput8;
struct IDirectInputDevice8A;
typedef IDirectInputDevice8A IDirectInputDevice8;

namespace ICE
{
	namespace INPUT
	{
		namespace
		{
			#define INPUT_KEY_SIZE 256
		}

		class Input;
		extern Input* g_Input;

		class Input
		{
		public:
			struct PlayerInputs
			{
				uint32_t closeApplication;
				//Movement
				uint32_t moveForward;
				uint32_t moveBackward;
				uint32_t moveRight;
				uint32_t moveLeft;
				uint32_t moveUp;
				uint32_t moveDown;
				uint32_t sprint;

				uint32_t raytraceCompounds;
				uint32_t deleteSingleCompound;
				uint32_t saveCompound;

				uint32_t debugPause;
				uint32_t debugNextFrame;

			} playerInputs;

		public:
			void Init(HINSTANCE hInstance, HWND hwnd);
			void Shutdown();

			void Update();

			bool IsKeyPressed(uint32_t key);
			bool IsKeyPressedOnce(uint32_t key);

			inline float GetMouseDeltaX()	{	return m_mouseDeltaX;	}
			inline float GetMouseDeltaY()	{	return m_mouseDeltaY;	}

		private:
			HWND m_hwnd;

			IDirectInput8* m_directInput;
			IDirectInputDevice8* m_keyboard;
			IDirectInputDevice8* m_mouse;

			unsigned char m_keyboardState[INPUT_KEY_SIZE];
			unsigned char m_keyboardStateOld[INPUT_KEY_SIZE];

			float m_sensitivity;
			float m_mouseDeltaX;
			float m_mouseDeltaY;
		};

	}
}