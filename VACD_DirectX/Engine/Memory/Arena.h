#pragma once
#include <stdint.h>
#include <assert.h>
#include "..\Utility\PointerUtility.h"
#include "..\Debug\Logger.h"

#include "SourceInfo.h"
#include "DefaultPolicies.h"
#include "BoundsChecking.h"
#include "Tracking.h"
#include "HeapArea.h"

#include "GeneralPurposeAllocator.h"
#include "LinearAllocator.h"

namespace ICE
{
	namespace MEMORY
	{
		struct SourceInfo;

		template <class AllocationPolicy, class ThreadPolicy, class BoundsCheckingPolicy, class MemoryTrackingPolicy, class MemoryTaggingPolicy>
		class Arena
		{
		public:
			template <class AreaPolicy>
			explicit Arena(const AreaPolicy& area)
				: m_allocator(area.GetStart(), area.GetEnd())
			{
			}

			void* Allocate(size_t size, size_t alignment, const SourceInfo& sourceInfo)
			{
				m_threadGuard.Enter();

				const size_t originalSize = size;
				const size_t newSize = originalSize + BoundsCheckingPolicy::SIZE_FRONT + BoundsCheckingPolicy::SIZE_BACK;
				
				void* plainMemory = m_allocator.Allocate(newSize, alignment, BoundsCheckingPolicy::SIZE_FRONT);

				const uintptr_t dataStart = PUTILITY::AddSize(plainMemory, BoundsCheckingPolicy::SIZE_FRONT);
				assert((dataStart % alignment) == 0);
				const uintptr_t dataEnd = PUTILITY::AddSize(dataStart, originalSize);

				m_boundsChecker.GuardFront(plainMemory);
				m_memoryTagger.TagAllocation(reinterpret_cast<void*>(dataStart), originalSize);
				m_boundsChecker.GuardBack(reinterpret_cast<void*>(dataEnd));

				m_memoryTracker.OnAllocation(plainMemory, newSize, alignment, sourceInfo);

				m_threadGuard.Leave();

				return reinterpret_cast<void*>(dataStart);
			}

			void Free(void* ptr)
			{
				m_threadGuard.Enter();

				uintptr_t originalMemory = PUTILITY::SubstractSize(ptr, BoundsCheckingPolicy::SIZE_FRONT);
				const size_t allocationSize = m_allocator.GetAllocationSize(reinterpret_cast<void*>(originalMemory));

				m_boundsChecker.CheckFront(reinterpret_cast<void*>(originalMemory));
				//TODO
				//m_boundsChecker.CheckBack(reinterpret_cast<void*>(PUTILITY::AddSize(originalMemory, (allocationSize - BoundsCheckingPolicy::SIZE_BACK)) ));

				m_memoryTracker.OnDeallocation(reinterpret_cast<void*>(originalMemory));

				m_memoryTagger.TagDeallocation(reinterpret_cast<void*>(originalMemory), allocationSize);

				m_allocator.Free(reinterpret_cast<void*>(originalMemory));

				m_threadGuard.Leave();
			}

			template <typename T>
			T* AllocateArray(size_t alignment, size_t arrayLength, SourceInfo info)
			{
				assert(arrayLength > 0);

				void* dataMemory = Allocate(sizeof(T) * arrayLength + ARRAY_LENGTH_FIELD, alignment, info);

				//first bytes are the length of the array (so we know how many destructors to call when freeing
				uint32_t* arrayLengthField = new(dataMemory) uint32_t(static_cast<uint32_t>(arrayLength));
				(void*)arrayLengthField;

				uintptr_t dataStart = PUTILITY::AddSize(dataMemory, ARRAY_LENGTH_FIELD);

				T* arrayStart = new(reinterpret_cast<void*>(dataStart)) T();

				for (size_t i = 1u; i < arrayLength; i++)
				{
					new(arrayStart + i) T();
				}

				return arrayStart;
			}

			template <typename T>
			void FreeArray(T* dataStart)
			{
				if (dataStart == nullptr)
				{
					ICE_LOG("Arena->FreeArray was called on a nullptr.");
					return;
				}

				uintptr_t dataMemory = PUTILITY::SubstractSize(reinterpret_cast<uintptr_t>(dataStart), ARRAY_LENGTH_FIELD);

				uint32_t* arrayLengthField = reinterpret_cast<uint32_t*>(dataMemory);
				uint32_t arrayLength = *arrayLengthField;

				for (uint32_t i = 0u; i < arrayLength; i++)
				{
					dataStart->~T();
					dataStart++;
				}
				//...
				//arrayLengthField->~uint32_t();

				Free(reinterpret_cast<void*>(dataMemory));
			}

		private:
			AllocationPolicy m_allocator;
			ThreadPolicy m_threadGuard;
			BoundsCheckingPolicy m_boundsChecker;
			MemoryTrackingPolicy m_memoryTracker;
			MemoryTaggingPolicy m_memoryTagger;
		};
	}
}

typedef ICE::MEMORY::Arena<ICE::MEMORY::LinearAllocator, ICE::MEMORY::SingleThreadPolicy, ICE::MEMORY::SimpleBoundsChecking, ICE::MEMORY::SizeWithVectorMemoryTracking, ICE::MEMORY::NoMemoryTagging> SimpleArena;
typedef ICE::MEMORY::Arena<ICE::MEMORY::LinearAllocator, ICE::MEMORY::SingleThreadPolicy, ICE::MEMORY::SimpleBoundsChecking, ICE::MEMORY::SizeWithVectorMemoryTracking, ICE::MEMORY::NoMemoryTagging> PoolArena;
typedef ICE::MEMORY::Arena<ICE::MEMORY::LinearAllocator, ICE::MEMORY::SingleThreadPolicy, ICE::MEMORY::SimpleBoundsChecking, ICE::MEMORY::SizeWithVectorMemoryTracking, ICE::MEMORY::NoMemoryTagging> LoaderArena;
typedef ICE::MEMORY::Arena<ICE::MEMORY::LinearAllocator, ICE::MEMORY::SingleThreadPolicy, ICE::MEMORY::SimpleBoundsChecking, ICE::MEMORY::SizeWithVectorMemoryTracking, ICE::MEMORY::NoMemoryTagging> PhysicsArena;


