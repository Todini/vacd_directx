#include "BoundsChecking.h"
#include <string.h>
#include <stdint.h>
#include <assert.h>

namespace ICE
{
	namespace MEMORY
	{
		//------ SimpleBoundsChecking ------

		const char SimpleBoundsChecking::BOUNDS_PATTERN_FRONT[SimpleBoundsChecking::SIZE_FRONT] = { 'M','E','M','O','R','Y' };
		const char SimpleBoundsChecking::BOUNDS_PATTERN_BACK[SimpleBoundsChecking::SIZE_BACK] = { 'D','E','T','E','C','T','E','D' };

		void SimpleBoundsChecking::GuardFront(void* ptr) const
		{
			memcpy(ptr, SimpleBoundsChecking::BOUNDS_PATTERN_FRONT, SimpleBoundsChecking::SIZE_FRONT);
		}

		void SimpleBoundsChecking::GuardBack(void* ptr) const
		{
			memcpy(ptr, SimpleBoundsChecking::BOUNDS_PATTERN_BACK, SimpleBoundsChecking::SIZE_BACK);
		}

		void SimpleBoundsChecking::CheckFront(void* ptr) const
		{
			char* guard = reinterpret_cast<char*>(ptr);

			for (uint32_t i = 0; i < SimpleBoundsChecking::SIZE_FRONT; i++)
			{
				if (SimpleBoundsChecking::BOUNDS_PATTERN_FRONT[i] != guard[i])
				{
					assert(false);
				}
			}
		}

		void SimpleBoundsChecking::CheckBack(void* ptr) const
		{
			char* guard = reinterpret_cast<char*>(ptr);

			for (uint32_t i = 0; i < SimpleBoundsChecking::SIZE_BACK; i++)
			{
				if (SimpleBoundsChecking::BOUNDS_PATTERN_BACK[i] != guard[i])
				{
					assert(false);
				}
			}
		}

		//------ SimpleBoundsChecking ------
	}
}