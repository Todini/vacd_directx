#pragma once

namespace ICE
{
	namespace MEMORY
	{
		class NoBoundsChecking
		{
		public:
			static const size_t SIZE_FRONT = 0u;
			static const size_t SIZE_BACK = 0u;

			inline void GuardFront(void* /*ptr*/) const { }
			inline void GuardBack(void* /*ptr*/) const { }

			inline void CheckFront(void* /*ptr*/) const { }
			inline void CheckBack(void* /*ptr*/) const { }
		};

		class SimpleBoundsChecking
		{
		public:
			static const size_t SIZE_FRONT = 6u;
			static const size_t SIZE_BACK = 8u;
			static const char BOUNDS_PATTERN_FRONT[SIZE_FRONT];
			static const char BOUNDS_PATTERN_BACK[SIZE_BACK];

			void GuardFront(void* ptr) const;
			void GuardBack(void* ptr) const; 

			void CheckFront(void* ptr) const;
			void CheckBack(void* ptr) const;
		};

	}
}