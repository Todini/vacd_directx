#pragma once

namespace ICE
{
	namespace MEMORY
	{
		class SimpleAllocator //AllocatorPolicy
		{
		public:
			SimpleAllocator(void* /*start*/, void* /*end*/) {}

			void* Allocate(size_t /*size*/, size_t /*alignment*/, size_t /*offset*/) {	return nullptr; }
			void Free(void* /*ptr*/) {}
			size_t GetAllocationSize(void* /*ptr*/) {}
		};

		class NoMemoryTagging
		{
		public:
			inline void TagAllocation(void* /*ptr*/, size_t /*size*/) const {}
			inline void TagDeallocation(void* /*ptr*/, size_t /*size*/) const {}
		};

		class SingleThreadPolicy
		{
		public:
			inline void Enter() const {}
			inline void Leave() const {}
		};
		/*
		template <class SynchronizationPrimitive>
		class MultiThreadPolicy
		{
		public:
			inline void Enter(void)
			{
				m_primitive.Enter();
			}

			inline void Leave(void)
			{
				m_primitive.Leave();
			}

		private:
			SynchronizationPrimitive m_primitive;
		};
		*/
	}
}