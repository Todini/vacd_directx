#include "GeneralPurposeAllocator.h"
#include <assert.h>

namespace ICE
{
	namespace MEMORY
	{

		GeneralPurposeAllocator::GeneralPurposeAllocator(void* start, void* end)
			: m_start(start)
			, m_end(end)
			, m_current(start)
		{
			assert((start != nullptr) && (end != nullptr) && (start != end));
		}

		GeneralPurposeAllocator::~GeneralPurposeAllocator()
		{

		}

		void* GeneralPurposeAllocator::Allocate(size_t size, size_t alignment, size_t offset)
		{
			assert((reinterpret_cast<uintptr_t>(m_end) - reinterpret_cast<uintptr_t>(m_current)) >= size);

			const uintptr_t uAlignment = static_cast<uintptr_t>(alignment);
			const uintptr_t uOffset = static_cast<uintptr_t>(offset);

			//Gives the current maximum bytes before the data can start (due to alignment)
			uintptr_t spaceBeforeData = uAlignment - (reinterpret_cast<uintptr_t>(m_current) % uAlignment);
			if (spaceBeforeData == uAlignment)
			{	//If it can be perfectly aligned (without counting on offset) the above calculation would still put space in there
				spaceBeforeData = 0u;
			}

			if (spaceBeforeData < uOffset)
			{	//-> Not enough space before the next alignment to place the requested offset-bytes in there
				spaceBeforeData += uAlignment;
			}

			uintptr_t dataPointer = reinterpret_cast<uintptr_t>(m_current) + spaceBeforeData - uOffset;
			assert((reinterpret_cast<uintptr_t>(m_end) - dataPointer) >= size);

			m_current = reinterpret_cast<void*>(dataPointer + static_cast<uintptr_t>(size));

			return reinterpret_cast<void*>(dataPointer);
		}

		void GeneralPurposeAllocator::Free(void* /*ptr*/)
		{

		}

		size_t GeneralPurposeAllocator::GetAllocationSize(void* /*ptr*/)
		{
			return 0u;
		}

	}
}