#pragma once

namespace ICE
{
	namespace MEMORY
	{
		class GeneralPurposeAllocator //AllocatorPolicy
		{
		public:
			GeneralPurposeAllocator(void* start, void* end);
			~GeneralPurposeAllocator();

			void* Allocate(size_t size, size_t alignment, size_t offset);
			void Free(void* ptr);
			size_t GetAllocationSize(void* ptr);

		private:
			void* m_start;
			void* m_end;

			void* m_current;
		};
	}

}