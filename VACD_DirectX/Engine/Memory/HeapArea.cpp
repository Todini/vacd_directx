#include "HeapArea.h"
#include <malloc.h>
#include <stdint.h>

namespace ICE
{
	namespace MEMORY
	{
		HeapArea::HeapArea(size_t size)
			: m_size(size)
			, m_memory(nullptr)
		{
			if (size != 0u)
			{
				m_memory = malloc(size);
			}
		}

		HeapArea::HeapArea(const HeapArea& other)
			: m_size(other.m_size)
			, m_memory(nullptr)
		{
			if (other.m_size != 0u)
			{
				m_memory = malloc(other.m_size);
			}
		}

		HeapArea::~HeapArea()
		{
			free(m_memory);
		}

		HeapArea& HeapArea::operator=(const HeapArea& other)
		{
			if (this != &other)
			{
				if (m_size < other.m_size)
				{
					free(m_memory);
					m_size = other.m_size;
					m_memory = malloc(other.m_size);
				}
			}

			return *this;
		}

		void* HeapArea::GetEnd() const
		{
			uintptr_t end = reinterpret_cast<uintptr_t>(m_memory);
			end += static_cast<uintptr_t>(m_size);

			return reinterpret_cast<void*>(end);
		}
	}
}