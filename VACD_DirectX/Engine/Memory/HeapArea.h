#pragma once

namespace ICE
{
	namespace MEMORY
	{
		class HeapArea // AreaPolicy
		{
		public:
			HeapArea(size_t size);
			HeapArea(const HeapArea& other);
			~HeapArea();

			/*	Note: If the other area is bigger, this area needs to acquire new memory!!!	*/
			HeapArea& operator=(const HeapArea& other);

			inline void* GetStart() const {	return m_memory;	}
			void* GetEnd() const;
			inline size_t GetSize() const { return m_size;	}

		private:
			size_t m_size;
			void* m_memory;
		};
	}
}