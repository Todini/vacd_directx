#pragma once
#include <map>

namespace ICE
{
	namespace MEMORY
	{
		class LinearAllocator
		{
		public:
			LinearAllocator(void* start, void* end);

			void* Allocate(size_t size, size_t alignment, size_t offset);
			void Free(void* ptr);
			inline void Reset()
			{
				m_current = m_start;
				m_allocationSizes.clear();
			}
			size_t GetAllocationSize(void* /*ptr*/);

		private:
			void* m_start;
			void* m_current;
			void* m_end;
			size_t m_totalSize;

			std::map<void*, size_t> m_allocationSizes;
		};

	}
}

