#pragma once
#include <stdint.h>

namespace ICE
{
	namespace MEMORY
	{
		struct NameTag
		{
			enum Enum : uint8_t
			{
				DEFAULT,
				GRAPHICS,
				MESH_LOADER,
				SIZE,
				INVALID
			};

			static const char* ToString(NameTag::Enum e)
			{
				switch (e)
				{
				case DEFAULT:
				{
					return "Default";
				} break;
				case GRAPHICS:
				{
					return "Graphics";
				} break;
				case MESH_LOADER:
				{
					return "Mesh Loader";
				} break;
				default:
				{
					return "NameTag::INVALID";
				} break;
				}
			}
		};
	}
}