#pragma once
#include "MemorySystem.h"

/*	
	Just there to encapsulate the requesting of memroy-space and creating an array of equal size
*/

namespace
{
	static const uint32_t SAFETY_BUFFER = 128u;
}

namespace ICE
{
	namespace MEMORY
	{
		template <typename T>
		struct MemoryHolder
		{
			MemoryHolder(uint32_t maxSize)
				: heapArea(sizeof(T) * maxSize + SAFETY_BUFFER)
				, arena(heapArea)
				, objects(ICE_NEW_ARRAY((&arena), NameTag::DEFAULT, T, maxSize))
				, count(0u)
				, capacity(maxSize)
			{

			}

			~MemoryHolder()
			{
				ICE_DELETE_ARRAY((&arena), objects);
			}

			T& operator[](size_t index)
			{
				assert(index < count)
				return objects[index];
			}
			const T& operator[](size_t index) const
			{
				assert(index < count)
				return objects[index];
			}

			HeapArea heapArea;
			PoolArena arena;
			T* objects;
			uint32_t count;
			uint32_t capacity;
		};
	}
}