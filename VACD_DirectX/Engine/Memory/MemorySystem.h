#pragma once
#include "Arena.h"
#include <new.h>

#include "..\Utility\PointerUtility.h"
#include "MemoryEnums.h"

namespace
{
	static const uint32_t ARRAY_LENGTH_FIELD = sizeof(uint32_t);
}

namespace ICE
{
	namespace MEMORY
	{
	}
}

//#define ICE_NEW_ALIGNED(arena, type, tag, alignment, ...) (new (ICE::MEMORY::g_MemorySystem->Allocate(allocator, tag, sizeof(type), 0u, alignment, __LINE__, __FILE__)) type (__VA_ARGS__))
//#define ICE_NEW_ALIGNED(arena, type, tag, alignment, ...) (new (arena->Allocate(sizeof(type), alignment, ICE::MEMORY::SourceInfo{__LINE__, __FILE__, tag} ) ) type (__VA_ARGS__))
#define ICE_NEW_ALIGNED(arena, type, tag, alignment, ...) (new (arena->Allocate(sizeof(type), alignment, ICE::MEMORY::SourceInfo() ) ) type (__VA_ARGS__))
#define ICE_NEW_TAG(arena, type, tag, ...) ICE_NEW_ALIGNED(arena, type, tag, alignof(type), __VA_ARGS__)
#define ICE_NEW(arena, type, ...) ICE_NEW_TAG(arena, type, ICE::MEMORY::NameTag::DEFAULT, __VA_ARGS__)

//#define ICE_NEW_ARRAY_ALIGNED(arena, tag, alignment, type, arrayLength) ICE::MEMORY::AllocateArray<type>(arena, tag, alignment, arrayLength, __LINE__, __FILE__)
#define ICE_NEW_ARRAY_ALIGNED(arena, tag, alignment, type, arrayLength) arena->AllocateArray<type>(alignment, arrayLength, ICE::MEMORY::SourceInfo())
#define ICE_NEW_ARRAY(arena, tag, type, arrayLength) ICE_NEW_ARRAY_ALIGNED(arena, tag, alignof(type), type, arrayLength)

#define ICE_NEW_RAW(arena, tag, alignment, size) (arena->Allocate(size, alignment, ICE::MEMORY::SourceInfo() ))


#define ICE_DELETE(arena, ptr) (arena->Free(ptr))

#define ICE_DELETE_ARRAY(arena, ptr) arena->FreeArray(ptr)