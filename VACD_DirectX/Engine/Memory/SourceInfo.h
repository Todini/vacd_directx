#pragma once
#include "MemoryEnums.h"

namespace ICE
{
	namespace MEMORY
	{
		struct SourceInfo
		{
			int32_t line;
			char* file;
			const NameTag::Enum memoryTag = NameTag::DEFAULT;
		};
	}
}