#include "StackArea.h"

#include <stdint.h>

namespace ICE
{
	namespace MEMORY
	{
		StackArea::StackArea(void* ptr, size_t size)
			: m_size(size)
			, m_memory(ptr)
		{
		}

		void* StackArea::GetEnd() const
		{
			uintptr_t end = reinterpret_cast<uintptr_t>(m_memory);
			end += static_cast<uintptr_t>(m_size);

			return reinterpret_cast<void*>(end);
		}
	}
}