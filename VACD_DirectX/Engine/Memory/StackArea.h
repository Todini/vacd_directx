#pragma once

namespace ICE
{
	namespace MEMORY
	{
		class StackArea // AreaPolicy
		{
		public:
			StackArea(void* ptr, size_t size);
			inline ~StackArea() {};

			inline void* GetStart() const { return m_memory; }
			void* GetEnd() const;

		private:
			size_t m_size;
			void* m_memory;
		};
	}
}