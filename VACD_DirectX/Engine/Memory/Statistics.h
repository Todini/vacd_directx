#pragma once

namespace ICE
{
	namespace MEMORY
	{
		struct AllocatorStatistics
		{
			AllocatorStatistics()
				: numAllocations(0)
				, totalNumAllocations(0)
				, peak(0)
				, takenMemory(0)
				, availableMemory(0)
			{
			}

			size_t numAllocations;
			size_t totalNumAllocations;
			size_t peak;
			size_t takenMemory;
			size_t availableMemory;
		};

		struct GlobalMemoryStatistics
		{
			GlobalMemoryStatistics()
				: peak(0)
				, totalNumAllocations(0)
				, totalAllocatedMemory(0)
				, allocatedMemory(0)
				, numAllocations(0)
			{
			}

			size_t peak;
			size_t totalNumAllocations;
			size_t totalAllocatedMemory;
			size_t allocatedMemory;
			size_t numAllocations;
		};
	}
}