#pragma once

#include <cstdint>
#include <mutex>
#include <assert.h>

namespace ICE
{
	namespace MEMORY
	{
		//
		// Important Note: All memory trackers must be implemented in a thread safe way
		//

		//
		// The MemoryTracker also gets the tag of the allocation, and can save it, if it saves detailed information about the allocations
		//

		/**
		* Tracks the number of allocations an deallocations
		*/
		class SimpleMemoryTracker
		{
		public:
			SimpleMemoryTracker(void);

			void OnAllocation(void* ptr, const char* tag, size_t size, size_t alignment, int line, const char* file);
			void OnFree(void* ptr, int line, const char* file);
			void OnShutdown(void);

		private:
			std::mutex trackerMutex;
			int64_t numAllocations;
		};

		inline SimpleMemoryTracker::SimpleMemoryTracker(void)
			: numAllocations(0)
		{
		}

		inline void SimpleMemoryTracker::OnAllocation(void* ptr, const char* tag, size_t size, size_t alignment, int line, const char* file)
		{
			std::lock_guard<std::mutex> lock(trackerMutex);
			++numAllocations;
		}

		inline void SimpleMemoryTracker::OnFree(void* ptr, int line, const char* file)
		{
			std::lock_guard<std::mutex> lock(trackerMutex);
			--numAllocations;
		}

		inline void SimpleMemoryTracker::OnShutdown(void)
		{
			if (numAllocations != 0)
			{
				assert(nullptr);
				// Raise an error because there were more or less allocations than deallocations
			}
		}

		/**
		* Tracks the allocations in a hash table including their stacktrace and also raises an
		* error if memory was freed, which is not registered in the table anymore
		*/
		class AdvancedMemoryTracker
		{
		public:
			void OnAllocation(void* ptr, const char* tag, size_t size, size_t alignment, int line, const char* file);
			void OnFree(void* ptr, int line, const char* file);
			void OnShutdown(void);

		private:
			std::mutex trackerMutex;
		};

		inline void AdvancedMemoryTracker::OnAllocation(void* ptr, const char* tag, size_t size, size_t alignment, int line, const char* file)
		{
			std::lock_guard<std::mutex> lock(trackerMutex);

			// Add an entry in the hash table for the given allocation
		}

		inline void AdvancedMemoryTracker::OnFree(void* ptr, int line, const char* file)
		{
			std::lock_guard<std::mutex> lock(trackerMutex);

			// Remove the entry from the hash table for the given allocation
			// If the allocation is not in the hash table, raise an error
		}

		inline void AdvancedMemoryTracker::OnShutdown(void)
		{
			// Check if the hash table is empty
			// If not, raise an error and display which allocations weren't freed
		}
	}
}