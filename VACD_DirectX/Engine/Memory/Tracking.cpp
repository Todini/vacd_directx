#include "Tracking.h"
#include <assert.h>
#include <string>
#include "..\Debug\Logger.h"

namespace ICE
{
	namespace MEMORY
	{
		SimpleMemoryTracking::SimpleMemoryTracking()
			: m_numAllocations(0)
		{

		}

		SimpleMemoryTracking::~SimpleMemoryTracking()
		{
			assert(m_numAllocations == 0);
		}

		void SimpleMemoryTracking::OnAllocation(void* /*ptr*/, size_t /*size*/, size_t /*alignment*/, const SourceInfo& /*info*/)
		{
			m_numAllocations++;
		}

		void SimpleMemoryTracking::OnDeallocation(void* /*ptr*/)
		{
			m_numAllocations--;
		}
	}
}

namespace ICE
{
	namespace MEMORY
	{
		SizeWithVectorMemoryTracking::SizeWithVectorMemoryTracking()
			: m_numAllocations(0)
			, m_sizeAllocated(0)
			, m_tracker()
		{
			m_tracker.reserve(1024u);
		}

		SizeWithVectorMemoryTracking::~SizeWithVectorMemoryTracking()
		{
			if (m_numAllocations != 0 || m_sizeAllocated != 0)
			{
				ICE_LOG("SizeMemoryTracking - numAllocations: ", std::to_string(m_numAllocations));
				ICE_LOG("SizeMemoryTracking - sizeAllocated: ", std::to_string(m_sizeAllocated));
			}

			assert(m_numAllocations == 0);
			assert(m_sizeAllocated == 0);
		}

		void SizeWithVectorMemoryTracking::OnAllocation(void* ptr, size_t size, size_t /*alignment*/, const SourceInfo& /*info*/)
		{
			m_numAllocations++;
			m_sizeAllocated += static_cast<int64_t>(size);
		
			Tracking t;
			t.ptr = ptr;
			t.size = size;
			m_tracker.push_back(t);
		}

		void SizeWithVectorMemoryTracking::OnDeallocation(void* ptr)
		{
			bool found = false;
			size_t tSize = m_tracker.size();
			for (size_t i = 0u; i < tSize; i++)
			{
				if (m_tracker[i].ptr == ptr)
				{
					m_numAllocations--;
					m_sizeAllocated -= static_cast<int64_t>(m_tracker[i].size);
					found = true;

					break;
				}
			}

			assert(found);
		}
	}
}