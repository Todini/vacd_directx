#pragma once
#include "SourceInfo.h"
#include <stdint.h>
#include <vector>

namespace ICE
{
	namespace MEMORY
	{
		class NoMemoryTracking
		{
		public:
			inline void OnAllocation(void* /*ptr*/, const size_t /*size*/, size_t /*alignment*/, const SourceInfo& /*info*/) {}
			inline void OnDeallocation(void* /*ptr*/) {}
		};
	}
}

namespace ICE
{
	namespace MEMORY
	{
		class SimpleMemoryTracking
		{
		public:
			SimpleMemoryTracking();
			~SimpleMemoryTracking();

			void OnAllocation(void* ptr, size_t size, size_t alignment, const SourceInfo& info);
			void OnDeallocation(void* ptr);

		private:
			int64_t m_numAllocations;

		};

	}
}

namespace ICE
{
	namespace MEMORY
	{
		class SizeWithVectorMemoryTracking
		{
			struct Tracking
			{
				void* ptr;
				size_t size;
			};

		public:
			SizeWithVectorMemoryTracking();
			~SizeWithVectorMemoryTracking();

			void OnAllocation(void* ptr, size_t size, size_t /*alignment*/, const SourceInfo& /*info*/);
			void OnDeallocation(void* ptr);

		private:
			int64_t m_numAllocations;
			int64_t m_sizeAllocated;

			std::vector<Tracking> m_tracker;
		};

	}
}