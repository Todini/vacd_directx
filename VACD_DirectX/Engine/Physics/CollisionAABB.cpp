#include "CollisionAABB.h"
#include "..\Utility\MathUtility.h"

namespace ICE
{
	namespace PHYS
	{
		namespace
		{
			static const float MIN = -100000.0f;
			static const float MAX = 100000.0f;
		}

		CollisionAABB::CollisionAABB()
			: min()
			, max()
		{
		}

		CollisionAABB::CollisionAABB(VTPosNorTex* vertices, uint32_t vertexCount)
			: min({ MAX, MAX, MAX })
			, max({ MIN, MIN, MIN })
		{
			Vec3 vertexPosition;
			Vec3 localMin = { MAX, MAX, MAX };
			Vec3 localMax = { MIN, MIN, MIN };
			for (uint32_t i = 0u; i < vertexCount; i++, vertices++)
			{
				vertexPosition.x = vertices->position.x;
				vertexPosition.y = vertices->position.y;
				vertexPosition.z = vertices->position.z;

				if (localMin.x > vertexPosition.x)	{ localMin.x = vertexPosition.x; }
				if (localMin.y > vertexPosition.y)	{ localMin.y = vertexPosition.y; }
				if (localMin.z > vertexPosition.z)	{ localMin.z = vertexPosition.z; }
				if (localMax.x < vertexPosition.x)	{ localMax.x = vertexPosition.x; }
				if (localMax.y < vertexPosition.y)	{ localMax.y = vertexPosition.y; }
				if (localMax.z < vertexPosition.z)	{ localMax.z = vertexPosition.z; }
			}

			min = localMin;
			max = localMax;
			unrotatedMin = localMin;
			unrotatedMax = localMax;
		}

		void CollisionAABB::Update(Vec4 rotationQuaternion)
		{
			//TODO: CHECK HOW TO ROTATE AABB
			//min = MATH::RotateWithQuaternion(unrotatedMin, rotationQuaternion);
			//max = MATH::RotateWithQuaternion(unrotatedMax, rotationQuaternion);
		}

	}
}