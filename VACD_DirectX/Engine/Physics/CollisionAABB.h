#pragma once
#include "..\Utility\Vec.h"
#include "..\Rendering\VertexTypes.h"

namespace ICE
{
	namespace PHYS
	{
		struct CollisionAABB
		{
			CollisionAABB();
			CollisionAABB(VTPosNorTex* vertices, uint32_t vertexCount);

			Vec3 min;
			Vec3 max;

			Vec3 unrotatedMin;
			Vec3 unrotatedMax;

			void Update(Vec4 rotationQuaternion);
		};
	}
}