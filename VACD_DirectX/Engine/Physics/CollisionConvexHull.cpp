#include "CollisionConvexHull.h"
#include "..\Utility\MathUtility.h"

#include "..\..\TP\quickhull\QuickHull.hpp"
#include "..\..\TP\quickhull\HalfEdgeMesh.hpp"

namespace ICE
{
	namespace PHYS
	{
		typedef quickhull::HalfEdgeMesh<float, size_t>::HalfEdge HalfEdge_t;

		CollisionConvexHull::CollisionConvexHull()
			: vertices()
			, faces()
		{
			
		}

		CollisionConvexHull::CollisionConvexHull(VTPosNorTex* inputVertices, uint32_t inputVertexCount)
			: vertices()
			, faces()
		{
			quickhull::QuickHull<float> qh;

			uint32_t sizeVerticesAsFloats = inputVertexCount * 3u;
			float* qVertices = new float[sizeVerticesAsFloats];
			uint32_t qVertIndex = 0u;
			for (uint32_t i = 0; i < inputVertexCount; i++)
			{
				qVertices[qVertIndex++] = inputVertices[i].position.x;
				qVertices[qVertIndex++] = inputVertices[i].position.y;
				qVertices[qVertIndex++] = inputVertices[i].position.z;
			}
			
			quickhull::HalfEdgeMesh<float, size_t> halfEdgeMesh = qh.getConvexHullAsMesh(qVertices, inputVertexCount, true);
			
			//TODO: Guarantee that the convex is never bigger than this!
			assert(halfEdgeMesh.m_vertices.size() <= MAX_CONVEX_SIZE);

			size_t vertexCount = halfEdgeMesh.m_vertices.size();
			size_t faceCount = halfEdgeMesh.m_faces.size();
			vertices.reserve(vertexCount);
			faces.reserve(faceCount);

			//Copy over vertices
			for(size_t i = 0u; i < vertexCount; i++)
			{
				vertices.push_back(Vec3(halfEdgeMesh.m_vertices[i].x, halfEdgeMesh.m_vertices[i].y, halfEdgeMesh.m_vertices[i].z));
			}
			//Extract faces
			for (size_t faceIndex = 0u; faceIndex < faceCount; faceIndex++)
			{
				Face face;
				face.vertexIndices.reserve(4u);
				Vec3 triangle[3];
				size_t triangleIndex = 0u;

				size_t firstHalfEdgeIndex = halfEdgeMesh.m_faces[faceIndex].m_halfEdgeIndex;
				HalfEdge_t firstHalfEdge = halfEdgeMesh.m_halfEdges[firstHalfEdgeIndex];

				face.vertexIndices.push_back(firstHalfEdge.m_endVertex);
				triangle[triangleIndex++] = vertices[firstHalfEdge.m_endVertex];
				size_t halfEdgeIndex = firstHalfEdge.m_next;
				HalfEdge_t halfEdge = halfEdgeMesh.m_halfEdges[halfEdgeIndex];

				while (halfEdgeIndex != firstHalfEdgeIndex)
				{
					face.vertexIndices.push_back(halfEdge.m_endVertex);
					triangle[triangleIndex++] = vertices[halfEdge.m_endVertex];
					halfEdgeIndex = halfEdge.m_next;
					halfEdge = halfEdgeMesh.m_halfEdges[halfEdgeIndex];
				}

				Vec3 direction0 = triangle[1] - triangle[0];
				MATH::Normalize(direction0);
				Vec3 direction1 = triangle[2] - triangle[1];
				MATH::Normalize(direction1);

				face.normal = MATH::CrossProduct(direction1, direction0);
				MATH::Normalize(face.normal);
				faces.push_back(face);
			}
		
			delete[] qVertices;
		}

		CollisionConvexHull::CollisionConvexHull(VTPosColNor* inputVertices, uint32_t inputVertexCount)
			: vertices()
			, faces()
		{
			quickhull::QuickHull<float> qh;

			uint32_t sizeVerticesAsFloats = inputVertexCount * 3u;
			float* qVertices = new float[sizeVerticesAsFloats];
			uint32_t qVertIndex = 0u;
			for (uint32_t i = 0; i < inputVertexCount; i++)
			{
				qVertices[qVertIndex++] = inputVertices[i].position.x;
				qVertices[qVertIndex++] = inputVertices[i].position.y;
				qVertices[qVertIndex++] = inputVertices[i].position.z;
			}

			quickhull::HalfEdgeMesh<float, size_t> halfEdgeMesh = qh.getConvexHullAsMesh(qVertices, inputVertexCount, true);

			//TODO: Guarantee that the convex is never bigger than this!
			//assert(halfEdgeMesh.m_vertices.size() <= MAX_CONVEX_SIZE);

			size_t vertexCount = halfEdgeMesh.m_vertices.size();
			size_t faceCount = halfEdgeMesh.m_faces.size();
			vertices.reserve(vertexCount);
			faces.reserve(faceCount);

			//Copy over vertices
			for (size_t i = 0u; i < vertexCount; i++)
			{
				vertices.push_back(Vec3(halfEdgeMesh.m_vertices[i].x, halfEdgeMesh.m_vertices[i].y, halfEdgeMesh.m_vertices[i].z));
			}
			//Extract faces
			for (size_t faceIndex = 0u; faceIndex < faceCount; faceIndex++)
			{
				Face face;
				face.vertexIndices.reserve(4u);
				Vec3 triangle[3];
				size_t triangleIndex = 0u;

				size_t firstHalfEdgeIndex = halfEdgeMesh.m_faces[faceIndex].m_halfEdgeIndex;
				HalfEdge_t firstHalfEdge = halfEdgeMesh.m_halfEdges[firstHalfEdgeIndex];

				face.vertexIndices.push_back(firstHalfEdge.m_endVertex);
				triangle[triangleIndex++] = vertices[firstHalfEdge.m_endVertex];
				size_t halfEdgeIndex = firstHalfEdge.m_next;
				HalfEdge_t halfEdge = halfEdgeMesh.m_halfEdges[halfEdgeIndex];

				while (halfEdgeIndex != firstHalfEdgeIndex)
				{
					face.vertexIndices.push_back(halfEdge.m_endVertex);
					triangle[triangleIndex++] = vertices[halfEdge.m_endVertex];
					halfEdgeIndex = halfEdge.m_next;
					halfEdge = halfEdgeMesh.m_halfEdges[halfEdgeIndex];
				}

				Vec3 direction0 = triangle[1] - triangle[0];
				MATH::Normalize(direction0);
				Vec3 direction1 = triangle[2] - triangle[1];
				MATH::Normalize(direction1);

				face.normal = MATH::CrossProduct(direction1, direction0);
				MATH::Normalize(face.normal);
				faces.push_back(face);
			}

			delete[] qVertices;
		}

		CollisionConvexHull::CollisionConvexHull(std::vector<ICE::FRAC::Vector3>& inputVertices)
			: vertices()
			, faces()
		{
			quickhull::QuickHull<PRECISION> qh;
			uint32_t inputVertexCount = static_cast<uint32_t>(inputVertices.size());

			uint32_t sizeVerticesAsFloats = inputVertexCount * 3u;
			PRECISION* qVertices = new PRECISION[sizeVerticesAsFloats];
			uint32_t qVertIndex = 0u;
			for (uint32_t i = 0; i < inputVertexCount; i++)
			{
				qVertices[qVertIndex++] = inputVertices[i].x;
				qVertices[qVertIndex++] = inputVertices[i].y;
				qVertices[qVertIndex++] = inputVertices[i].z;
			}

			quickhull::ConvexHull<PRECISION> convexHull = qh.getConvexHull(qVertices, true, true, EPSILON);


			delete[] qVertices;
		}
	}
}