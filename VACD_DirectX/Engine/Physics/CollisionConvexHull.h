#pragma once
#include "..\Utility\Vec.h"
#include "..\Rendering\VertexTypes.h"
#include <vector>

#include "..\VACD\Vectors.h"

namespace ICE
{
	namespace PHYS
	{
		namespace
		{
			static const uint8_t MAX_CONVEX_SIZE = 18u;
		}

		struct Face
		{
			Vec3 normal;
			std::vector<size_t> vertexIndices;
		};

		struct CollisionConvexHull
		{
			CollisionConvexHull();
			CollisionConvexHull(VTPosNorTex* vertices, uint32_t vertexCount);
			CollisionConvexHull(VTPosColNor* vertices, uint32_t vertexCount);
			CollisionConvexHull(std::vector<ICE::FRAC::Vector3>& vertices);

			std::vector<Vec3> vertices;
			std::vector<Face> faces;
		};
	}
}