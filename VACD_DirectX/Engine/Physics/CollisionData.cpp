#include "CollisionData.h"
#include "..\Utility\MathUtility.h"
#include "RigidBody.h"

#include <algorithm>

namespace ICE
{
	namespace PHYS
	{
		namespace
		{
			void ConvertConvexHullToLocalCoordinateSystem(CollisionConvexHull& convexHull, Vec3 convexPosition, Vec4 convexRotationQuat, Vec3 targetPosition, Vec4 targetRotationQuat)
			{
				Vec4 targetRotationQuatInverse = MATH::InvertQuaternion(targetRotationQuat);

				//Convert all positions
				size_t vertexCount = convexHull.vertices.size();
				for (size_t i = 0u; i < vertexCount; i++)
				{
					Vec3 vertexToUpdate = convexHull.vertices[i];
					//First rotate the localPoint with it's rotation
					vertexToUpdate = MATH::RotateWithQuaternion(vertexToUpdate, convexRotationQuat);
					//Then add the position of the convex (transforming it into world position)
					vertexToUpdate += convexPosition;

					//Now bring the vertex into the coordinates of the targetCoordinates (by just substracting the position)
					vertexToUpdate -= targetPosition;
					//And then rotate this point by the inverseRotation (so we only have to rotate this point, not the targeted system)
					vertexToUpdate = MATH::RotateWithQuaternion(vertexToUpdate, targetRotationQuatInverse);

					convexHull.vertices[i] = vertexToUpdate;
				}
			}

			bool CheckTriangleTriangle(Vec3* triangle0, Vec3* triangle1, Vec3& segment0, Vec3& segment1)
			{
				Vec3 normal0;
				{
					//Compute the normal of the triangle0
					Vec3 edge0 = triangle0[1] - triangle0[0];
					Vec3 edge1 = triangle0[2] - triangle0[0];
					normal0 = MATH::CrossProduct(edge0, edge1);
					MATH::Normalize(normal0);
				}

				// Test if the edges of triangle1 interesect triangle0
				float d[3];
				int8_t positive = 0, negative = 0, zero = 0;

				//Check if it intersects the PLANE of triangle0
				for (int8_t i = 0; i < 3; i++)
				{
					d[i] = MATH::Dot(normal0, triangle1[i] - triangle0[0]);
					if (d[i] > 0.0f)
					{
						positive++;
					}
					else if (d[i] < 0.0f)
					{
						negative++;
					}
					else
					{
						zero++;
					}
				}

				//Check if the triangle lies on both sides, not just one (therefore possibly intersecting
				if (positive > 0 && negative > 0)
				{
					if (positive == 2)
					{
						if (d[0] < 0.0f)
						{
							segment0 = (triangle1[0] * d[1] - triangle1[1] * d[0]) / (d[1] - d[0]);
							segment1 = (triangle1[0] * d[2] - triangle1[2] * d[0]) / (d[2] - d[0]);
						}
						else if (d[1] < 0.0f)
						{
							segment0 = (triangle1[1] * d[0] - triangle1[0] * d[1]) / (d[0] - d[1]);
							segment1 = (triangle1[1] * d[2] - triangle1[2] * d[1]) / (d[2] - d[1]);
						}
						else
						{
							segment0 = (triangle1[2] * d[0] - triangle1[0] * d[2]) / (d[0] - d[2]);
							segment1 = (triangle1[2] * d[1] - triangle1[1] * d[2]) / (d[1] - d[2]);
						}
					}
					else if (negative == 2)
					{
						if (d[0] > 0.0f)
						{
							segment0 = (triangle1[0] * d[1] - triangle1[1] * d[0]) / (d[1] - d[0]);
							segment1 = (triangle1[0] * d[2] - triangle1[2] * d[0]) / (d[2] - d[0]);
						}
						else if (d[1] > 0.0f)
						{
							segment0 = (triangle1[1] * d[0] - triangle1[0] * d[1]) / (d[0] - d[1]);
							segment1 = (triangle1[1] * d[2] - triangle1[2] * d[1]) / (d[2] - d[1]);
						}
						else
						{
							segment0 = (triangle1[2] * d[0] - triangle1[0] * d[2]) / (d[0] - d[2]);
							segment1 = (triangle1[2] * d[1] - triangle1[1] * d[2]) / (d[1] - d[2]);
						}
					}
					else
					{
						if (d[0] == 0.0f)
						{
							segment0 = triangle1[0];
							segment1 = (triangle1[1] * d[2] - triangle1[2] * d[1]) / (d[2] - d[1]);
						}
						else if (d[1] == 0.0f)
						{
							segment0 = triangle1[1];
							segment1 = (triangle1[2] * d[0] - triangle1[0] * d[2]) / (d[0] - d[2]);
						}
						else
						{
							segment0 = triangle1[2];
							segment1 = (triangle1[0] * d[1] - triangle1[1] * d[0]) / (d[1] - d[0]);
						}
					}
					return true;
				}

				return false;
			}
		}

		bool DoesIntersectSphere(Vec3 myPosition, CollisionSphere mySphere, Vec3 otherPosition, CollisionSphere otherSphere)
		{
			myPosition += mySphere.center;
			otherPosition += otherSphere.center;

			Vec3 distanceVec = otherPosition - myPosition;
			float distanceFloatSq = MATH::LengthSq(distanceVec);
			float radii = mySphere.radius + otherSphere.radius;
			radii *= radii;

			return distanceFloatSq < radii;
		}
		bool DoesIntersectPointSphere(Vec3 position, Vec3 spherePosition, CollisionSphere sphere)
		{
			spherePosition += sphere.center;

			Vec3 distanceVec = spherePosition - position;
#if 1
			float distanceFloat = MATH::Length(distanceVec);

			return distanceFloat < sphere.radius;
#else
			float distanceFloatSq = MATH::LengthSq(distanceVec);
			float radiusSq = sphere.radius * sphere.radius;

			return distanceFloatSq < radiusSq;
#endif
		}

		bool DoesIntersectAABB(Vec3 myPosition, CollisionAABB myAABB, Vec3 otherPosition, CollisionAABB otherAABB)
		{
			const Vec3 myMin = myPosition + myAABB.min;
			const Vec3 myMax = myPosition + myAABB.max;
			const Vec3 otherMin = otherPosition + otherAABB.min;
			const Vec3 otherMax = otherPosition + otherAABB.max;

			bool result = (myMin.x <= otherMax.x && myMax.x >= otherMin.x) &&
				(myMin.y <= otherMax.y && myMax.y >= otherMin.y) &&
				(myMin.z <= otherMax.z && myMax.z >= otherMin.z);

			return result;
		}
		bool DoesIntersectPointAABB(Vec3 position, Vec3 aabbPosition, CollisionAABB aabb)
		{
			//Make position relative to the aabb
			position -= aabbPosition;

			bool result =	(position.x >= aabb.min.x && position.x <= aabb.max.x) &&
							(position.y >= aabb.min.y && position.y <= aabb.max.y) &&
							(position.z >= aabb.min.z && position.z <= aabb.max.z);

			return result;
		}

		bool DoesIntersectConvexHull(RigidBody* __restrict rb0, RigidBody* __restrict rb1, std::vector<Contact>& __restrict contacts)
		{
			bool hasContacts = false;

			const Vec3 position0 = rb0->m_position;
			const Vec3 position1 = rb1->m_position;

			const Vec4 rotation0 = MATH::QuaternionFromDirectXVector(rb0->m_quatRotation);
			const Vec4 rotation1 = MATH::QuaternionFromDirectXVector(rb1->m_quatRotation);

			{
				//Testing all vertices(ConvexHull) of rb1 against the ConvexHull of rb0
				size_t hullSize1 = rb1->m_colData.convexHull.vertices.size();
				for (size_t i = 0u; i < hullSize1; i++)
				{
					//Get the localPoint of the Convex Hull
					Vec3 pointToCheck = rb1->m_colData.convexHull.vertices[i];
					//Rotate the point with it's own rotation
					pointToCheck = MATH::RotateWithQuaternion(pointToCheck, rotation1);
					//Then transform it into world coordinates
					Vec3 pointToCheckWorld = pointToCheck + position1;

					Contact possibleContact;
					if (DoesIntersectPointConvexHull(pointToCheckWorld, position0, rotation0, &rb0->m_colData.convexHull, possibleContact))
					{
						//Apparently we have a contact!
						possibleContact.rb0 = rb0;
						possibleContact.rb1 = rb1;
						contacts.push_back(possibleContact);
						hasContacts = true;
					}
				}
			}
			if (!hasContacts)
			{
				//If nothing has been found yet, then test all vertices(ConvexHull) of rb0 against the ConvexHull of rb1
				//This can easily happen, if only a point of the convex hulll intersects a face of the other
				size_t hullSize0 = rb0->m_colData.convexHull.vertices.size();
				for (size_t i = 0u; i < hullSize0; i++)
				{
					//Get the localPoint of the Convex Hull
					Vec3 pointToCheck = rb0->m_colData.convexHull.vertices[i];
					//Rotate the point with it's own rotation
					pointToCheck = MATH::RotateWithQuaternion(pointToCheck, rotation0);
					//Then transform it into world coordinates
					Vec3 pointToCheckWorld = pointToCheck + position0;

					Contact possibleContact;
					if (DoesIntersectPointConvexHull(pointToCheckWorld, position1, rotation1, &rb1->m_colData.convexHull, possibleContact))
					{
						//Apparently we have a contact!
						possibleContact.rb0 = rb1;
						possibleContact.rb1 = rb0;
						possibleContact.penetrationDepthHalf *= -1.0f;
						possibleContact.contactNormal *= -1.0f;

						contacts.push_back(possibleContact);
						hasContacts = true;
					}
				}
			}
			return hasContacts;
		}
		bool DoesIntersectPointConvexHull(Vec3 positionWorld, Vec3 convexPositionWorld, Vec4 convexRotationQuat, CollisionConvexHull* __restrict convexHull, Contact& __restrict contact)
		{
			Vec4 quaternionInverse = convexRotationQuat;
			quaternionInverse.x *= -1.0f;
			quaternionInverse.y *= -1.0f;
			quaternionInverse.z *= -1.0f;

			contact.penetrationDepthHalf = 1000.0f;
			//Bringing the position into the local Coordinate-System of the convex-hull. Including rotation!
			Vec3 positionLocal = positionWorld - convexPositionWorld;
			positionLocal = MATH::RotateWithQuaternion(positionLocal, quaternionInverse);

			size_t faceCount = convexHull->faces.size();
			for (size_t i = 0u; i < faceCount; i++)
			{
				//Get a vertex of the face and translate it to world space
				Vec3 faceVertex = convexHull->vertices[convexHull->faces[i].vertexIndices[0]];
				//faceVertex += convexPositionWorld;
				//Rotate the worldSpaceVertex with the rotation of the object
				//faceVertex = MATH::RotateWithQuaternion(faceVertex, quaternion);
				//Rotate the normal as well!
				Vec3 faceNormal = convexHull->faces[i].normal;
				//Vec3 faceNormalRotated = MATH::RotateWithQuaternion(faceNormal, quaternion);

				Vec3 direction = faceVertex - positionLocal;
				float distance = MATH::Dot(direction, faceNormal);
				//distance /= MATH::Length(direction);

				constexpr float bound = 0.0001f;
				if (distance >= bound)
				{
					return false;
				}
				distance *= -1.0f;
				//Check for the minimum penetration depth to determine contactNormal
				if (distance < contact.penetrationDepthHalf)
				{
					contact.penetrationDepthHalf = distance;
					contact.contactNormal = MATH::RotateWithQuaternion(faceNormal, convexRotationQuat);

					contact.contactPointWorld = positionWorld - contact.contactNormal * (distance * 0.5f);
				}
			}

			//Half the penetration depth
			contact.penetrationDepthHalf *= 0.5f;

			return true;
		}

		bool DoesIntersectConvexHullTriangle(RigidBody* __restrict rb0, RigidBody* __restrict rb1, std::vector<Contact>& __restrict contacts)
		{
			bool hasContacts = false;

			const Vec3 position0 = rb0->m_position;
			const Vec3 position1 = rb1->m_position;

			const Vec4 rotation0 = MATH::QuaternionFromDirectXVector(rb0->m_quatRotation);
			const Vec4 rotation1 = MATH::QuaternionFromDirectXVector(rb1->m_quatRotation);

			CollisionConvexHull convex1Local = rb1->m_colData.convexHull;
			ConvertConvexHullToLocalCoordinateSystem(convex1Local, position1, rotation1, position0, rotation0);

			{
				CollisionConvexHull* convex0 = &rb0->m_colData.convexHull;
				Vec3 triangle0[3];
				Vec3 triangle1[3];

				for (Face& face0 : convex0->faces)
				{
					//The Triangle of convex0 still need to be rotated to be accurate
					triangle0[0] = MATH::RotateWithQuaternion(convex0->vertices[face0.vertexIndices[0]], rotation0);
					triangle0[1] = MATH::RotateWithQuaternion(convex0->vertices[face0.vertexIndices[1]], rotation0);
					triangle0[2] = MATH::RotateWithQuaternion(convex0->vertices[face0.vertexIndices[2]], rotation0);

					for (Face& face1 : convex1Local.faces)
					{
						triangle1[0] = convex1Local.vertices[face1.vertexIndices[0]];
						triangle1[1] = convex1Local.vertices[face1.vertexIndices[1]];
						triangle1[2] = convex1Local.vertices[face1.vertexIndices[2]];

						Vec3 segment0, segment1;

						if (DoesIntersectTriangleTriangle(triangle0, triangle1, segment0, segment1))
						{
							hasContacts = true;

							Contact contact;
							contact.penetrationDepthHalf = 10000.0f;

							Vec3 faceNormal = face0.normal * -1.0f;
							for (uint8_t i = 0u; i < 3u; i++)
							{
								Vec3 face1Vertex = triangle1[i];

								Vec3 direction = face1Vertex - triangle0[0];
								float distance = MATH::Dot(direction, faceNormal);

								constexpr float bound = 0.01f;
								if (distance <= bound)
								{
									distance *= -1.0f;
									//Check for the minimum penetration depth to determine contactNormal
									if (distance < contact.penetrationDepthHalf)
									{
										contact.penetrationDepthHalf = distance * 0.5f;
										contact.contactNormal = MATH::RotateWithQuaternion(faceNormal, rotation0);

										contact.contactPointWorld = position0 + face1Vertex;
										contact.contactPointWorld += faceNormal * (contact.penetrationDepthHalf);
									}
								}
							}

							contact.rb0 = rb0;
							contact.rb1 = rb1;
							contacts.push_back(contact);

						}


					}
				}

			}

			return hasContacts;
		}

		//https://www.geometrictools.com/GTEngine/Samples/Geometrics/AllPairsTriangles/TriangleIntersection.cpp
		bool DoesIntersectTriangleTriangle(Vec3* triangle0, Vec3* triangle1, Vec3& segment0, Vec3& segment1)
		{
			Vec3 segment00, segment01, segment10, segment11;

			//This actually checks a Triangle against the PLANE of the other triangle -> That's why both checks are needed!
			if (CheckTriangleTriangle(triangle0, triangle1, segment00, segment01) && CheckTriangleTriangle(triangle1, triangle0, segment10, segment11))
			{
				/*	We know that both triangle are on the plane of the other triangle
				BUT this does not mean they are intersecting!
				We do know that the segments of each triangle are on the same line -> therefore simple check if these segments are intersecting!
				*/
				Vec3 normal0, normal1;
				{
					Vec3 a = triangle0[1] - triangle0[0];
					Vec3 b = triangle0[2] - triangle0[0];
					normal0 = MATH::CrossProduct(a, b);
					MATH::Normalize(normal0);
				}
				{
					Vec3 a = triangle1[1] - triangle1[0];
					Vec3 b = triangle1[2] - triangle1[0];
					normal1 = MATH::CrossProduct(a, b);
					MATH::Normalize(normal1);
				}

				Vec3 direction = MATH::CrossProduct(normal0, normal1);
				MATH::Normalize(direction);

				Vec3 average = segment00;
				average += segment01;
				average += segment10;
				average += segment11;
				average *= 0.25f;

				float t00 = MATH::Dot(direction, segment00 - average);
				float t01 = MATH::Dot(direction, segment01 - average);
				float t10 = MATH::Dot(direction, segment10 - average);
				float t11 = MATH::Dot(direction, segment11 - average);

				//This just writes min in the first value and max in the second (so just sorting)
				std::pair<float, float> I0 = std::minmax(t00, t01);
				std::pair<float, float> I1 = std::minmax(t10, t11);

				bool doesIntersect = (I0.second > I1.first && I0.first < I1.second);

				if (doesIntersect)
				{
					segment1.x = std::max(segment01.x, segment11.x);
					segment1.y = std::max(segment01.y, segment11.y);
					segment1.z = std::max(segment01.z, segment11.z);

					segment0.x = std::min(segment00.x, segment10.x);
					segment0.y = std::min(segment00.y, segment10.y);
					segment0.z = std::min(segment00.z, segment10.z);

					return true;
				}

				return false;
			}

			return false;
		}

	}
}