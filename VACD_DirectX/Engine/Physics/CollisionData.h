#pragma once
#include "CollisionSphere.h"
#include "CollisionAABB.h"
#include "CollisionConvexHull.h"

#include "Contact.h"
#include <vector>


namespace ICE
{
	namespace PHYS
	{
		struct CollisionData
		{
			CollisionSphere sphere;
			CollisionAABB aabb;
			CollisionConvexHull convexHull;
		};

		bool DoesIntersectSphere(Vec3 myPosition, CollisionSphere mySphere, Vec3 otherPosition, CollisionSphere otherSphere);
		bool DoesIntersectPointSphere(Vec3 position, Vec3 spherePosition, CollisionSphere sphere);

		bool DoesIntersectAABB(Vec3 myPosition, CollisionAABB myAABB, Vec3 otherPosition, CollisionAABB otherAABB);
		bool DoesIntersectPointAABB(Vec3 position, Vec3 aabbPosition, CollisionAABB aabb);

		bool DoesIntersectConvexHull(RigidBody* __restrict rb0, RigidBody* __restrict rb1, std::vector<Contact>& __restrict contacts);
		bool DoesIntersectPointConvexHull(Vec3 positionWorld, Vec3 convexPositionWorld, Vec4 convexRotationQuat, CollisionConvexHull* __restrict convexHull, Contact& __restrict contact);

		bool DoesIntersectConvexHullTriangle(RigidBody* __restrict rb0, RigidBody* __restrict rb1, std::vector<Contact>& __restrict contacts);
		bool DoesIntersectTriangleTriangle(Vec3* triangle0, Vec3* triangle1, Vec3& intersection0, Vec3& intersection1);


	}
}