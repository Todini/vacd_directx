#include "CollisionDataGenerator.h"
#include <assert.h>

#include "..\Memory\MemorySystem.h"


namespace ICE
{
	namespace PHYS
	{
		namespace
		{
			static MEMORY::HeapArea m_heapArea(1024 * 1024 * 5);
			static LoaderArena m_memoryArena(m_heapArea);
			static CollisionData m_collisionData[EMeshName_t::SIZE];
		}


		CollisionData CreateCollisionData(VTPosNorTex* vertices, uint32_t vertexCount)
		{
			CollisionData cData;

			cData.sphere = CollisionSphere(vertices, vertexCount);
			cData.aabb = CollisionAABB(vertices, vertexCount);
			cData.convexHull = CollisionConvexHull(vertices, vertexCount);

			return cData;
		}

		void CreateCollisionDataForMeshName(EMeshName_t meshName, VTPosNorTex* vertices, uint32_t vertexCount)
		{
			CollisionData cData = CreateCollisionData(vertices, vertexCount);
			m_collisionData[meshName] = cData;
		}

		const CollisionData& LoadCollisionData(EMeshName_t meshName)
		{
			const CollisionData& result = m_collisionData[meshName];
			return result;
		}
	}
}