#pragma once
#include "..\File\MeshNames.h"
#include "..\Rendering\VertexTypes.h"

#include "CollisionData.h"

namespace ICE
{
	namespace PHYS
	{


		CollisionData CreateCollisionData(VTPosNorTex* vertices, uint32_t vertexCount);

		void CreateCollisionDataForMeshName(EMeshName_t meshName, VTPosNorTex* vertices, uint32_t vertexCount);
		const CollisionData& LoadCollisionData(EMeshName_t meshName);

	}
}