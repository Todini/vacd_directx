#include "CollisionSphere.h"
#include "..\Utility\MathUtility.h"

namespace ICE
{
	namespace PHYS
	{
		CollisionSphere::CollisionSphere()
			: center(0.0f, 0.0f, 0.0f)
			, radius(0.0f)
		{
		}

		CollisionSphere::CollisionSphere(VTPosNorTex* vertices, uint32_t vertexCount)
			: center(0.0f, 0.0f, 0.0f)
			, radius(0.0f)
		{
			VTPosNorTex* vertexStart = vertices;
			for (uint32_t i = 0u; i < vertexCount; i++, vertexStart++)
			{
				center.x += vertexStart->position.x;
				center.y += vertexStart->position.y;
				center.z += vertexStart->position.z;
			}
			center /= static_cast<float>(vertexCount);

			float biggestRadiusSq = -1.0f;
			float temp;
			Vec3 direction;
			for (uint32_t i = 0u; i < vertexCount; i++, vertices++)
			{
				direction = center - Vec3(vertices->position.x, vertices->position.y, vertices->position.z);
				temp = MATH::LengthSq(direction);
				if (biggestRadiusSq < temp)
				{
					biggestRadiusSq = temp;
				}
			}
			
			radius = sqrtf(biggestRadiusSq);
		}

		CollisionSphere::CollisionSphere(VTPosColNor* vertices, uint32_t vertexCount, Vec3 centerPoint)
			: center(centerPoint)
			, radius(0.0f)
		{
			float biggestRadiusSq = -1.0f;
			float temp;
			Vec3 direction;
			for (uint32_t i = 0u; i < vertexCount; i++, vertices++)
			{
				direction = center - Vec3(vertices->position.x, vertices->position.y, vertices->position.z);
				temp = MATH::LengthSq(direction);
				if (biggestRadiusSq < temp)
				{
					biggestRadiusSq = temp;
				}
			}

			radius = sqrtf(biggestRadiusSq);
		}
	}
}