#pragma once
#include "..\Utility\Vec.h"
#include "..\Rendering\VertexTypes.h"

namespace ICE
{
	namespace PHYS
	{
		struct CollisionSphere
		{
			CollisionSphere();
			CollisionSphere(VTPosNorTex* vertices, uint32_t vertexCount);
			CollisionSphere(VTPosColNor* vertices, uint32_t vertexCount, Vec3 center);

			Vec3 center;
			float radius;
		};
	}
}