#include "Contact.h"
#include "CollisionData.h"
#include "RigidBody.h"

#include <algorithm>

namespace ICE
{
	namespace PHYS
	{
		namespace
		{
			static const float STEP_DISTANCE = 1.0f / 8.0f;

			void ExpandAABB(CollisionAABB aabb, Vec3 position, Vec3 points[8])
			{
				points[0] = aabb.min + position;
				points[1] = Vec3(aabb.min.x, aabb.min.y, aabb.max.z) + position;
				points[2] = Vec3(aabb.min.x, aabb.max.y, aabb.min.z) + position;
				points[3] = Vec3(aabb.min.x, aabb.max.y, aabb.max.z) + position;
				points[4] = Vec3(aabb.max.x, aabb.min.y, aabb.min.z) + position;
				points[5] = Vec3(aabb.max.x, aabb.min.y, aabb.max.z) + position;
				points[6] = Vec3(aabb.max.x, aabb.max.y, aabb.min.z) + position;
				points[7] = aabb.max + position;
			}
			void ExpandAABB(CollisionAABB aabb, Vec3 points[8])
			{
				points[0] = aabb.min;
				points[1] = Vec3(aabb.min.x, aabb.min.y, aabb.max.z);
				points[2] = Vec3(aabb.min.x, aabb.max.y, aabb.min.z);
				points[3] = Vec3(aabb.min.x, aabb.max.y, aabb.max.z);
				points[4] = Vec3(aabb.max.x, aabb.min.y, aabb.min.z);
				points[5] = Vec3(aabb.max.x, aabb.min.y, aabb.max.z);
				points[6] = Vec3(aabb.max.x, aabb.max.y, aabb.min.z);
				points[7] = aabb.max;
			}
		}

		void GenerateContactsAABB(RigidBody* rb0, RigidBody* rb1, std::vector<Contact>& contacts)
		{
			const Vec3 position0 = rb0->m_position;
			const Vec3 position1 = rb1->m_position;
			const CollisionAABB aabb0 = rb0->m_colData.aabb;
			const CollisionAABB aabb1 = rb1->m_colData.aabb;

			const Vec3 aabb0min = aabb0.min + position0;
			const Vec3 aabb0max = aabb0.max + position0;
			const Vec3 aabb1min = aabb1.min + position1;
			const Vec3 aabb1max = aabb1.max + position1;

			CollisionAABB aabbIntersectionWorld;
			aabbIntersectionWorld.min = Vec3(std::max(aabb0min.x, aabb1min.x), std::max(aabb0min.y, aabb1min.y), std::max(aabb0min.z, aabb1min.z));
			aabbIntersectionWorld.max = Vec3(std::min(aabb0max.x, aabb1max.x), std::min(aabb0max.y, aabb1max.y), std::min(aabb0max.z, aabb1max.z));

			/*	Calculating the contactNormal (by checking which axis is penetrated the least) -> Some corner-corner collisions might be wrong because of that!
			*/
			Vec3 normal;
			float minDepth;
			uint8_t axisToChoose = 0u;
			{
				Vec3 sizes = aabbIntersectionWorld.max - aabbIntersectionWorld.min;

				if (sizes.x <= sizes.y && sizes.x <= sizes.z)
				{
					minDepth = sizes.x;
					normal = Vec3(1.0f, 0.0f, 0.0f);
					if (aabbIntersectionWorld.min.x < position0.x)
					{
						normal *= -1.0f;
					}
				}
				else if (sizes.y <= sizes.z)
				{
					minDepth = sizes.y;
					axisToChoose = 1u;
					normal = Vec3(0.0f, 1.0f, 0.0f);
					if (aabbIntersectionWorld.min.y < position0.y)
					{
						normal *= -1.0f;
					}
				}
				else
				{
					minDepth = sizes.z;
					axisToChoose = 2u;
					normal = Vec3(0.0f, 0.0f, 1.0f);
					if (aabbIntersectionWorld.min.z < position0.z)
					{
						normal *= -1.0f;
					}
				}
			}


			//Calculating the penetration points (dependent on the normal)
			Contact contactsAABB[4];
			{
				//Halfing the penetrationDepth
				minDepth *= 0.5f;
				for (uint8_t i = 0u; i < 4u; i++)
				{
					contactsAABB[i].contactNormal = normal;
					contactsAABB[i].penetrationDepthHalf = minDepth;
					contactsAABB[i].rb0 = rb0;
					contactsAABB[i].rb1 = rb1;
				}

				Vec3 intersectionsPoints[8];
				ExpandAABB(aabbIntersectionWorld, intersectionsPoints);
				//Depending on the choosen axis (the axis of least penetration = likely where the object came from), we need to half the chosen axis.
				// => So the contact points are between the 2 intersecting objects.
				if (axisToChoose == 0u)
				{
					for (uint8_t i = 0u; i < 4u; i++)
					{
						contactsAABB[i].contactPointWorld = intersectionsPoints[i];
						contactsAABB[i].contactPointWorld.x += intersectionsPoints[i+4u].x;
						contactsAABB[i].contactPointWorld.x *= 0.5f;
					}
				}
				else if (axisToChoose == 1u)
				{
					contactsAABB[0].contactPointWorld = intersectionsPoints[0];
					contactsAABB[0].contactPointWorld.x += intersectionsPoints[2].x;
					contactsAABB[0].contactPointWorld.x *= 0.5f;
					contactsAABB[1].contactPointWorld = intersectionsPoints[1];
					contactsAABB[1].contactPointWorld.x += intersectionsPoints[3].x;
					contactsAABB[1].contactPointWorld.x *= 0.5f;
					contactsAABB[2].contactPointWorld = intersectionsPoints[4];
					contactsAABB[2].contactPointWorld.x += intersectionsPoints[6].x;
					contactsAABB[2].contactPointWorld.x *= 0.5f;
					contactsAABB[3].contactPointWorld = intersectionsPoints[5];
					contactsAABB[3].contactPointWorld.x += intersectionsPoints[7].x;
					contactsAABB[3].contactPointWorld.x *= 0.5f;
				}
				else
				{
					uint8_t otherIndex = 1u;
					for (uint8_t contactIndex = 0u; contactIndex < 4u; contactIndex++, otherIndex += 2u)
					{
						contactsAABB[contactIndex].contactPointWorld = intersectionsPoints[contactIndex * 2u];
						contactsAABB[contactIndex].contactPointWorld.x += intersectionsPoints[otherIndex].x;
						contactsAABB[contactIndex].contactPointWorld.x *= 0.5f;
					}
				}
			}

			for (uint8_t i = 0u; i < 4u; i++)
			{
				contacts.push_back(contactsAABB[i]);
			}


			/*
			//At least one point of the AABB is inside the other AABB
			int8_t indexOfFirstContact = -1;
			for (int8_t i = 0u; i < 8u; i++)
			{
				if (DoesIntersect(aabbPoints[i], position1, aabb1))
				{
					indexOfFirstContact = i;
					break;
				}
			}

			//Check if we found a point inside the aabb. If not, switch the aabb's!
			if (indexOfFirstContact == -1)
			{
				std::swap(position0, position1);
				std::swap(aabb0, aabb1);
				ExpandAABB(aabb0, position0, aabbPoints);

				//At least one point of the AABB is inside the other AABB
				int8_t indexOfFirstContact = -1;
				for (int8_t i = 0u; i < 8u; i++)
				{
					if (DoesIntersect(aabbPoints[i], position1, aabb1))
					{
						indexOfFirstContact = i;
						break;
					}
				}
			}
			assert(indexOfFirstContact != -1);

			{//BeginBlock
				//ContactQuad == A small Quad that fits into the intersection
				Vec3 contactQuad[8];
				contactQuad[0] = aabbPoints[indexOfFirstContact];
				int8_t contactIndex = 1;
				
				for (int8_t i = 0u; i < 8u; i++)
				{
					if (i == indexOfFirstContact) continue;
					Vec3 pointToTest = aabbPoints[i];
					//First check if the end-point is intersecting as well
					if (DoesIntersect(pointToTest, position1, aabb1))
					{
						contactQuad[contactIndex] = pointToTest;
						contactIndex++;
						continue;
					}
					//If not, check along the direction
					Vec3 direction = pointToTest - contactQuad[0];
					direction *= STEP_DISTANCE;

					for (float alpha = STEP_DISTANCE; alpha < 1.0f; alpha += STEP_DISTANCE)
					{
						pointToTest += direction;

						if (!DoesIntersect(pointToTest, position1, aabb1))
						{
							//If the first iteration of this fails -> pointToTest would be == contactQuad[0] -> Which is useless
							if (alpha != STEP_DISTANCE)
							{
								contactQuad[contactIndex] = pointToTest - direction;
								contactIndex++;
							}
							break;
						}
					}

				}
				//ContactQuad is filled before here
				
				//TODO: Generate Contacts out of the ContactQuad.
				//TODO: Calculate a contactNormal and penetrationDepth!

			}//EndBlock
			*/
		}

	}
}