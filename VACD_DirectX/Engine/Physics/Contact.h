#pragma once
#include "..\Utility\Vec.h"

#include <vector>

namespace ICE
{
	namespace PHYS
	{
		class RigidBody;

		struct Contact
		{
			RigidBody* rb0;
			RigidBody* rb1;

			Vec3 contactPointWorld;
			Vec3 contactNormal;
			float penetrationDepthHalf;
		};

		struct CollisionAABB;

		void GenerateContactsAABB(RigidBody* rb0, RigidBody* rb1, std::vector<Contact>& contacts);
	}
}