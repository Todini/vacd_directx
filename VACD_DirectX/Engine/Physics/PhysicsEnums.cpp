#include "PhysicsEnums.h"

namespace ICE
{
	namespace PHYS
	{
		const char* EMovement::ToString(EMovement::Enum e)
		{
			switch (e)
			{
				case STATIC:
				{
					return "Static";
				} break;
				case DYNAMIC:
				{
					return "Dynamic";
				} break;
				default:
				{
					return "EMovement::INVALID";
				} break;
			}
		}
	}
}