#pragma once
#include <stdint.h>

namespace ICE
{
	namespace PHYS
	{
		struct EMovement
		{
			enum Enum : uint8_t
			{
				STATIC,
				DYNAMIC,
				SIZE,
				INVALID
			};

			const char* ToString(EMovement::Enum e);
		};
	}
	typedef PHYS::EMovement::Enum EMovement_t;
}