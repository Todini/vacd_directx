#include "PhysicsSystem.h"
#include "..\Utility\DirectXMathUtility.h"
#include "..\Utility\MathUtility.h"
#include "..\Memory\MemorySystem.h"
#include "..\Debug\Logger.h"

#include "Contact.h"
#include <vector>

namespace
{
	static const float BOUNCE_VALUE = 0.15f;
	static const float DAMPENING = 0.75f;

	static uint32_t heapSize = 1024u * 1024u;
	static ICE::MEMORY::HeapArea heapArea = ICE::MEMORY::HeapArea(heapSize);

	static const float PHYSICS_TIMESTEP = 1.0f / 60.0f;
	static float accumulator = 0.0f;

	//Currently using a vector for that because I absolutely have no idea how many contacts will be generated
	std::vector<ICE::PHYS::Contact> contacts = std::vector<ICE::PHYS::Contact>();
	//Temporarily holds contacts for this collision
	std::vector<ICE::PHYS::Contact> contactsTemp = std::vector<ICE::PHYS::Contact>();



	struct Impulse
	{
		ICE::PHYS::RigidBody* rb;
		ICE::Vec3 linearImpulse;
	};
}

namespace ICE
{
	namespace PHYS
	{
		namespace
		{
			static Vec3 ComputeForces(float mass, Vec3 currentLinearVelocity /*, uint32_t forceKey*/)
			{
				static const Vec3 gravity = { 0.0f, -19.6f, 0.0f };

				Vec3 result = gravity / mass;

				//Drag
				{
					static const float K1 = 0.5f;
					static const float K2 = 0.5f;

					float velocityLength = MATH::Length(currentLinearVelocity);
					if (velocityLength > 0.0f)
					{

						float dragCoeff = K1 * velocityLength + K2 * velocityLength * velocityLength;
						//First normalize the current linear velocity
						Vec3 dragForce = currentLinearVelocity / velocityLength;
						//Then multiply the negative dragCoeff
						dragForce *= -dragCoeff;

						//result += currentLinearVelocity * -1.5f;
						result += dragForce;
					}
				}
				//Drag

				return result;
			}

			void ResolveImpactAtLocation(RigidBody* rigidBody, Contact& contact, float dt)
			{
				Vec3 reflectedVelocity = MATH::Reflect(rigidBody->m_linearVelocity, contact.contactNormal);
				reflectedVelocity *= std::powf(BOUNCE_VALUE,dt);

				rigidBody->m_linearImpulse -= rigidBody->m_linearVelocity;
				rigidBody->m_linearImpulse += reflectedVelocity;

				Vec3 distanceFromCenterToImpactPoint = contact.contactPointWorld - rigidBody->m_position;
				Vec3 torque = MATH::CrossProduct(distanceFromCenterToImpactPoint, rigidBody->m_linearVelocity);
				//torque *= 0.0f;

				rigidBody->m_angularImpulse += torque;
			}

			static bool DoesCollide(RigidBody* __restrict rbLeft, RigidBody* __restrict rbRight, std::vector<Contact>& __restrict newContacts)
			{
				Vec3 posLeft = rbLeft->m_position;
				Vec3 posRight = rbRight->m_position;

				if (DoesIntersectSphere(posLeft, rbLeft->m_colData.sphere, posRight, rbRight->m_colData.sphere))
				{
					//if (DoesIntersectAABB(posLeft, rbLeft->m_colData.aabb, posRight, rbRight->m_colData.aabb))
					{
						if (DoesIntersectConvexHullTriangle(rbLeft, rbRight, newContacts))
						{
							return true;
						}
					}
				}

				return false;
			}

			static void Simulate(RigidBody* rigidBodies, uint32_t bodyCount, float dt)
			{
				{
					RigidBody* rigidBody = rigidBodies;

					//Update the position of rigidBodies depending on the forces and their current velocities
					for (uint32_t i = 0u; i < bodyCount; i++, rigidBody++)
					{
						//Check if the rigid body is even allowed to move
						if (rigidBody->m_movement != EMovement_t::DYNAMIC) continue;

#if 0
						//Linear acceleration == forces / mass
						Vec3 frameLinearAcceleration = ComputeForces(rigidBody->m_mass, rigidBody->m_linearVelocity);
						frameLinearAcceleration *= dt;

						//Linear velocity
						Vec3 frameLinearVelocity = frameLinearAcceleration * 2.0f;
						frameLinearVelocity += rigidBody->m_linearVelocity;
						frameLinearVelocity += rigidBody->m_linearImpulse;
						frameLinearVelocity *= dt;

						//(Linear) Position
						rigidBody->m_linearVelocity += frameLinearAcceleration;
						rigidBody->m_position += frameLinearVelocity;
						//Save the frame data for linear acceleration and velocity
						rigidBody->m_frameLinearVelocity = frameLinearVelocity;
						rigidBody->m_frameLinearAcceleration = frameLinearAcceleration;

						rigidBody->m_linearImpulse = Vec3();
#else
						{
							//Linear acceleration == forces / mass
							Vec3 frameLinearAcceleration = ComputeForces(rigidBody->m_mass, rigidBody->m_linearVelocity);
							frameLinearAcceleration *= dt;

							rigidBody->m_linearVelocity += rigidBody->m_linearImpulse;
							rigidBody->m_linearVelocity *= std::powf(DAMPENING, dt);
							rigidBody->m_linearVelocity += frameLinearAcceleration;

							rigidBody->m_position += rigidBody->m_linearVelocity * dt;

							rigidBody->m_linearImpulse = Vec3();
						}
#endif

#if 1
						//Calculating the intertia tensor in world coordinates!
						rigidBody->UpdateInertiaTensorWorld();

						DirectX::XMMATRIX rotationMatrix = DirectX::XMMatrixRotationQuaternion(rigidBody->m_quatRotation);
						//Compute m_IInverse
						DirectX::XMMATRIX IInverse = DirectX::XMMatrixMultiply(rigidBody->m_InertiaTensorInverseWorld, rotationMatrix);
						//IInverse = DirectX::XMMatrixMultiply(DirectX::XMMatrixTranspose(rotationMatrix), IInverse);

						Vec3 torque = rigidBody->m_angularImpulse;
						Vec3 angularAcceleration = MATH::MatrixMultiplyVector(IInverse, torque);
						angularAcceleration *= dt;

						rigidBody->m_angularVelocity *= std::powf(DAMPENING, dt);
						MATH::SafetyRound(rigidBody->m_angularVelocity);
						rigidBody->m_angularVelocity += MATH::MatrixMultiplyVector(IInverse, angularAcceleration);

						rigidBody->m_quatRotation = DirectX::XMQuaternionMultiply(rigidBody->m_quatRotation, DirectX::XMQuaternionRotationRollPitchYaw(rigidBody->m_angularVelocity.x, rigidBody->m_angularVelocity.y, rigidBody->m_angularVelocity.z));
						
						rigidBody->m_quatRotation = DirectX::XMQuaternionNormalize(rigidBody->m_quatRotation);
						rigidBody->m_angularImpulse = Vec3();

#elif 0
						//rigidBodies->m_torque = DirectX::XMVector3Cross(distanceFromCenterToImpactPoint, forceAtImpactLocation);

						//Vec3 distanceFromCenterToImpactPoint = { 0.5f, -0.5f, 0.5f };
						//Vec3 distanceFromCenterToImpactPoint; // = { 0.5f, -0.5f, 0.5f };
						//Vec3 forceAtImpactLocation = { 0.0f, 20.0f, 0.0f };
						//Vec3 torque = MATH::CrossProduct(distanceFromCenterToImpactPoint, forceAtImpactLocation);
						Vec3 torque;

						//rigidBody->m_quatRotation = DirectX::XMQuaternionNormalize(rigidBody->m_quatRotation);
						DirectX::XMMATRIX rotationMatrix = DirectX::XMMatrixRotationQuaternion(rigidBody->m_quatRotation);
						//Compute m_IInverse
						DirectX::XMMATRIX IInverse = DirectX::XMMatrixMultiply(rotationMatrix, rigidBody->m_InertiaTensorInverse);
						IInverse = DirectX::XMMatrixMultiply(IInverse, DirectX::XMMatrixTranspose(rotationMatrix));

						Vec3 angularAcceleration = torque; //MATH::MatrixMultiplyVector(IInverse, angularAcceleration);
						angularAcceleration *= dt;

						Vec3 angularVelocity = angularAcceleration * 2.0f;
						angularVelocity += rigidBody->m_angularVelocity;
						angularVelocity += rigidBody->m_angularImpulse;
						angularVelocity *= dt;

						rigidBody->m_angularVelocity += MATH::MatrixMultiplyVector(IInverse, angularAcceleration); // angularAcceleration;
						//rigidBody->m_quatRotation += DirectX::XMQuagter DirectX::XMQuaternionRotationRollPitchYaw(angularVelocity.x, angularVelocity.y, angularVelocity.z);
						rigidBody->m_quatRotation = DirectX::XMQuaternionMultiply(rigidBody->m_quatRotation, DirectX::XMQuaternionRotationRollPitchYaw(angularVelocity.x, angularVelocity.y, angularVelocity.z));

						rigidBody->m_angularImpulse = Vec3();
#else
						{
							//rigidBody->m_quatRotation = DirectX::XMQuaternionNormalize(rigidBody->m_quatRotation);
							//DirectX::XMMATRIX rotationMatrix = DirectX::XMMatrixRotationQuaternion(rigidBody->m_quatRotation);
							//Compute m_IInverse
							//DirectX::XMMATRIX IInverse = DirectX::XMMatrixMultiply(rotationMatrix, rigidBody->m_InertiaTensorInverse);
							//IInverse = DirectX::XMMatrixMultiply(IInverse, DirectX::XMMatrixTranspose(rotationMatrix));

							Vec3 torque;

							Vec3 frameAngularAcceleration = torque;
							frameAngularAcceleration *= dt;

							rigidBody->m_angularVelocity += rigidBody->m_angularImpulse;
							rigidBody->m_angularVelocity *= std::powf(DAMPENING, dt);
							rigidBody->m_angularVelocity += frameAngularAcceleration;

#if 0
							DirectX::XMVECTOR angularVelocityAsQuaternion = DirectX::XMQuaternionRotationRollPitchYaw(rigidBody->m_angularVelocity.x, rigidBody->m_angularVelocity.y, rigidBody->m_angularVelocity.z);
							DirectX::XMVECTOR newRotation = DirectX::XMQuaternionMultiply(rigidBody->m_quatRotation, angularVelocityAsQuaternion);
							rigidBody->m_quatRotation = newRotation;
#else
							
							Vec4 quatAngularVelocity = Vec4(rigidBody->m_angularVelocity);
							//Vec4 quatRotation = MATH::QuaternionMultiply(MATH::QuaternionFromDirectXVector(rigidBody->m_quatRotation), quatAngularVelocity);
							Vec4 quatRotation = MATH::QuaternionAdd(MATH::QuaternionFromDirectXVector(rigidBody->m_quatRotation), quatAngularVelocity);

							rigidBody->m_quatRotation = MATH::DirectXVectorFromQuaternion(quatRotation);
#endif

							rigidBody->m_angularImpulse = Vec3();
						}

#endif
#if 0
						//Compute helper variables
						//Update velocity (... seems wrong, very wrong)
						rigidBodies->m_linearVelocity = rigidBodies->m_linearAcceleration / rigidBodies->m_mass;

						//Compute rotation-Matrix out of the quaternion
						rigidBodies->m_quatRotation = DirectX::XMQuaternionNormalize(rigidBodies->m_quatRotation);
						DirectX::XMMATRIX rotationMatrix = DirectX::XMMatrixRotationQuaternion(rigidBodies->m_quatRotation);
						//Compute m_IInverse
						DirectX::XMMATRIX tempMatrix = DirectX::XMMatrixMultiply(rotationMatrix, rigidBodies->m_IBodyInverse);
						rigidBodies->m_IInverse = DirectX::XMMatrixMultiply(tempMatrix, DirectX::XMMatrixTranspose(rotationMatrix));

						//Update angular velocity
						rigidBodies->m_angularVelocity = ICE::MATH::MatrixMultiplyVector(rigidBodies->m_IInverse, rigidBodies->m_angularAcceleration);
						//Compute helper variables
#endif

					} //End Update Position
				}
				//Reset the pointer to the start
				RigidBody* outer = rigidBodies;

				for (uint32_t count = 0u; count < bodyCount; count++, outer++)
				{
					//outer->m_colData.aabb.Update(MATH::QuaternionFromDirectXVector(outer->m_quatRotation));
				}

				uint32_t outerLimit = bodyCount - 1u;
				outer = rigidBodies;
				//Check Collisions and generate Contacts
				for (uint32_t outerCount = 0u; outerCount < outerLimit; outerCount++, outer++)
				{
					//if(outer->m_movement == EMovement_t::STATIC)
					RigidBody* inner = outer + 1;
					for (uint32_t innerCount = outerCount + 1u; innerCount < bodyCount; innerCount++, inner++)
					{
						//outer->m_movement
						if (DoesCollide(outer, inner, contactsTemp))
						{
							//These rigidBodies collide!
							
							Contact singleContact;
							singleContact.rb0 = contactsTemp[0].rb0;
							singleContact.rb1 = contactsTemp[0].rb1;
							singleContact.contactNormal = Vec3(0.0f, 0.0f, 0.0f);
							singleContact.contactPointWorld = Vec3(0.0f, 0.0f, 0.0f);
							singleContact.penetrationDepthHalf = 0.0f;

							size_t contactsTempSize = contactsTemp.size();
							for (size_t i = 0u; i < contactsTempSize; i++)
							{
								singleContact.penetrationDepthHalf += contactsTemp[i].penetrationDepthHalf;
								singleContact.contactPointWorld += contactsTemp[i].contactPointWorld;
								singleContact.contactNormal += contactsTemp[i].contactNormal;

								contacts.push_back(contactsTemp[i]);
							}

							float count = static_cast<float>(contactsTempSize);
							singleContact.penetrationDepthHalf /= count;
							singleContact.contactPointWorld /= count;
							MATH::Normalize(singleContact.contactNormal);

							contactsTemp.clear();

							if (singleContact.rb0->m_movement != EMovement_t::STATIC)
							{
								//ResolveImpactAtLocation(singleContact.rb0, singleContact, dt);
							}
							if (singleContact.rb1->m_movement != EMovement_t::STATIC)
							{
								//ResolveImpactAtLocation(singleContact.rb1, singleContact, dt);
							}

						}
					}
				}
				//Check Collisions and generate Contacts

				//Resolve contacts
				for (Contact& contact : contacts)
				{
					//First, check if the contact is still 'valid' and needs to be resolved

					if (contact.rb0->m_movement != EMovement_t::STATIC)
					{
						Vec3 position0 = contact.rb0->m_position;
						Vec4 rotation0 = MATH::QuaternionFromDirectXVector(contact.rb0->m_quatRotation);
						Vec3 pointToCheck = contact.contactPointWorld + contact.contactNormal * contact.penetrationDepthHalf * 0.95f;

						Contact dummyContact;
						bool doesIntersect = DoesIntersectPointConvexHull(pointToCheck, position0, rotation0, &contact.rb0->m_colData.convexHull, dummyContact);

						if (doesIntersect == false)
						{
							continue;
						}
					}
					if (contact.rb1->m_movement != EMovement_t::STATIC)
					{
						Vec3 position1 = contact.rb1->m_position;
						Vec4 rotation1 = MATH::QuaternionFromDirectXVector(contact.rb1->m_quatRotation);
						Vec3 pointToCheck = contact.contactPointWorld;// +contact.contactNormal * contact.penetrationDepthHalf;

						Contact dummyContact;
						bool doesIntersect = DoesIntersectPointConvexHull(pointToCheck, position1, rotation1, &contact.rb1->m_colData.convexHull, dummyContact);

						if (doesIntersect == false)
						{
							continue;
						}
					}

					float totalInertia = 0.0f;
					float angularInertia[2];
					float linearInertia[2];

					Vec3 impulsePerMove[2];
					Vec3 relativeContactPosition[2];
					relativeContactPosition[0] = Vec3();
					relativeContactPosition[1] = Vec3();

					if(contact.rb0->m_movement != EMovement_t::STATIC)
					{
						relativeContactPosition[0] = contact.contactPointWorld - contact.rb0->m_position;

						impulsePerMove[0] = MATH::CrossProduct(relativeContactPosition[0], contact.contactNormal);
						impulsePerMove[0] = MATH::MatrixMultiplyVector(contact.rb0->m_InertiaTensorInverseWorld, impulsePerMove[0]);
						Vec3 angularInertiaWorld = MATH::CrossProduct(impulsePerMove[0], relativeContactPosition[0]);

						angularInertia[0] = MATH::Dot(angularInertiaWorld, contact.contactNormal);
						linearInertia[0] = contact.rb0->m_massInverse;

						totalInertia += angularInertia[0] + linearInertia[0];
					}
					if (contact.rb1->m_movement != EMovement_t::STATIC)
					{
						//contact.contactNormal *= -1.0f;
						relativeContactPosition[1] = contact.contactPointWorld - contact.rb1->m_position;
						Vec3 contactNormal = contact.contactNormal * -1.0f;

						impulsePerMove[1] = MATH::CrossProduct(relativeContactPosition[1], contactNormal);
						impulsePerMove[1] = MATH::MatrixMultiplyVector(contact.rb1->m_InertiaTensorInverseWorld, impulsePerMove[1]);
						Vec3 angularInertiaWorld = MATH::CrossProduct(impulsePerMove[1], relativeContactPosition[1]);

						angularInertia[1] = MATH::Dot(angularInertiaWorld, contactNormal);
						linearInertia[1] = contact.rb1->m_massInverse;

						totalInertia += angularInertia[1] + linearInertia[1];
					}

					float inverseInertia = 1 / totalInertia;
					float penetration = contact.penetrationDepthHalf * 2.0f;
					float linearMove[2];
					float angularMove[2];
					linearMove[0] = penetration * linearInertia[0] * inverseInertia;
					linearMove[1] = -penetration * linearInertia[1] * inverseInertia;
					angularMove[0] = penetration  * angularInertia[0] * inverseInertia;
					angularMove[1] = -penetration * angularInertia[1] * inverseInertia;

					if (contact.rb0->m_movement != EMovement_t::STATIC)
					{
						//Update rotation
						Vec3 rotation = impulsePerMove[0] * (1 / angularInertia[0]);
						rotation *= angularMove[0];

						//Update the position
						Vec3 positionUpdate = contact.contactNormal * linearMove[0];
						contact.rb0->m_position += positionUpdate;

						//R' = R + 0.5f * dR * R
						// 0.5f * dR
						rotation *= 0.5f;
						Vec4 deltaRotationAsQuaternion = Vec4(rotation);
						Vec4 currentRotationAsQuaternion = MATH::QuaternionFromDirectXVector(contact.rb0->m_quatRotation);

						//(0.5f * dR) * R
						deltaRotationAsQuaternion = MATH::QuaternionMultiply(deltaRotationAsQuaternion, currentRotationAsQuaternion);
						//R + (0.5f * dR * R)
						currentRotationAsQuaternion = MATH::QuaternionAdd(currentRotationAsQuaternion, deltaRotationAsQuaternion);
						//R' = ....
						contact.rb0->m_quatRotation = MATH::DirectXVectorFromQuaternion(currentRotationAsQuaternion);
					}
					if(contact.rb1->m_movement != EMovement_t::STATIC)
					{
						//Update rotation
						Vec3 rotation = impulsePerMove[1] * (1 / angularInertia[1]);
						rotation *= angularMove[1];

						//Update the position
						Vec3 contactNormal = contact.contactNormal * -1.0f;
						Vec3 positionUpdate = contactNormal * linearMove[1];
						contact.rb1->m_position += positionUpdate;

						//R' = R + 0.5f * dR * R
						// 0.5f * dR
						rotation *= 0.5f;
						Vec4 deltaRotationAsQuaternion = Vec4(rotation);
						Vec4 currentRotationAsQuaternion = MATH::QuaternionFromDirectXVector(contact.rb1->m_quatRotation);

						//(0.5f * dR) * R
						deltaRotationAsQuaternion = MATH::QuaternionMultiply(deltaRotationAsQuaternion, currentRotationAsQuaternion);
						//R + (0.5f * dR * R)
						currentRotationAsQuaternion = MATH::QuaternionAdd(currentRotationAsQuaternion, deltaRotationAsQuaternion);
						//R' = ....
						contact.rb1->m_quatRotation = MATH::DirectXVectorFromQuaternion(currentRotationAsQuaternion);
					}

					//Resolve velocity changes (Game Physics Engine p335)
					{
						//Create the contact Coordinate System.
						Vec3 contactAxisX = contact.contactNormal, contactAxisY, contactAxisZ;
						if (fabsf(contactAxisX.y) > fabsf(contactAxisX.x))
						{
							//The normal points more into the direction of the World-Y-Axis -> Chose World-X-Axis instead
							contactAxisY = Vec3(1.0f, 0.0f, 0.0f);
						}
						else
						{
							contactAxisY = Vec3(0.0f, 1.0f, 0.0f);
						}
						MATH::MakeOrthonormalBasis(contactAxisX, &contactAxisY, &contactAxisZ);
						DirectX::XMMATRIX contactCS = MATH::CreateMatrixFromCoordinatSystem(contactAxisX, contactAxisY, contactAxisZ);


						Vec3 contactVelocity = Vec3();
						if (contact.rb0->m_movement != EMovement_t::STATIC)
						{
							Vec3 rotation = contact.rb0->m_angularVelocity;
							Vec3 velocity = MATH::CrossProduct(rotation, relativeContactPosition[0]);
							velocity += contact.rb0->m_linearVelocity;

							contactVelocity = MATH::MatrixTransform3X3(contactCS, velocity);
						}
						if (contact.rb1->m_movement != EMovement_t::STATIC)
						{
							Vec3 rotation = contact.rb1->m_angularVelocity;
							Vec3 velocity = MATH::CrossProduct(rotation, relativeContactPosition[1]);
							velocity += contact.rb1->m_linearVelocity;

							if (contact.rb0->m_movement == EMovement_t::STATIC)
							{
								contactVelocity = MATH::MatrixTransform3X3(contactCS, velocity);
							}
							else
							{
								//if both are dynamic -> contactVelcity = seperating velocity
								contactVelocity -= MATH::MatrixTransform3X3(contactCS, velocity);
							}
						}

						if (contact.rb0->m_movement != EMovement_t::STATIC)
						{
							contact.rb0->m_linearVelocity = contactVelocity;
						}
						if (contact.rb1->m_movement != EMovement_t::STATIC)
						{
							contact.rb1->m_linearVelocity = contactVelocity * -1.0f;
						}

					}

				}
				//Resolve contacts

				//After all contacts are resolved -> clear that vector
				contacts.clear();
			}

		} //End namespace

		//TODO: Add a PInit and PShutdown to acquire the memory with ICE_NEW_RAW once (or when physics gets bigger)
		void PUpdate(RigidBody* previousRigidBodies, uint32_t bodyCount, float dt)
		{
			contacts.reserve(bodyCount * 8u);

			uint32_t currentPhysicsMemorySize = sizeof(RigidBody) * bodyCount;
			//Check if our physics simulation has grown too much to be copied over
			if (currentPhysicsMemorySize > heapSize)
			{
				ICE_LOG("Increasing rigidbody heap-size from ", std::to_string(heapSize), " to ", std::to_string(currentPhysicsMemorySize * 2u));
				heapSize = currentPhysicsMemorySize * 2u;
			}

			//Arena is just there to access the memory for the physics simulation
			PhysicsArena arena = PhysicsArena(heapArea);
			//Copy over the data from the 'previous' rigidbodies
			RigidBody* currentRigidBodies = reinterpret_cast<RigidBody*>(ICE_NEW_RAW((&arena), ICE::MEMORY::NameTag::DEFAULT, alignof(RigidBody), currentPhysicsMemorySize));
			memcpy(currentRigidBodies, previousRigidBodies, currentPhysicsMemorySize);

			accumulator += dt;
			
			//This enforces that the physics-simulation runs at locked 60FPS. Currently still missing the last step of interpolating between the last 2 states.
			while (accumulator >= PHYSICS_TIMESTEP)
			{
				accumulator -= PHYSICS_TIMESTEP;

				Simulate(currentRigidBodies, bodyCount, PHYSICS_TIMESTEP);
			}

			//As the last step, copy over the data from the latests physics simulation. And update the transform
			memcpy(previousRigidBodies, currentRigidBodies, currentPhysicsMemorySize);
			ICE_DELETE((&arena), currentRigidBodies);
			RigidBody* rb = previousRigidBodies;
			for (uint32_t i = 0u; i < bodyCount; i++, rb++)
			{
				rb->UpdateTransform();
			}


		}
	}
}