#pragma once
#include "RigidBody.h"

//http://gafferongames.com/game-physics/fix-your-timestep/
namespace ICE
{
	namespace PHYS
	{
		void PUpdate(RigidBody* rigidBodies, uint32_t physicsCount, float dt);
	}
}