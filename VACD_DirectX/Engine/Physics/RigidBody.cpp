#include "RigidBody.h"

#include "..\Components\Transform.h"

namespace ICE
{
	namespace PHYS
	{
		namespace
		{
			static const float DEFAULT_MASS = 5.0f;
			static const float DEFAULT_INVERSE_MASS = 1.0f / DEFAULT_MASS;

			static DirectX::XMFLOAT3X3 CreateInertiaTensorBox(float mass)
			{
				/*	Calculating the Inertia Tensor (IBody) used to determine how easy it is to rotate an object
				TODO: To decently calculate the Inertia Tensor (IBody) we need more abstractions.
				Either in form of the AABB or the Convex Hull.

				For now we assume 1unit box centered at 0/0/0
				*/
				//http://www.cs.cmu.edu/~baraff/sigcourse/notesd1.pdf  S27
				//https://en.wikipedia.org/wiki/List_of_moments_of_inertia
				float massFactor = mass / 12.0f;

				DirectX::XMFLOAT3X3 inertiaTensor;
				inertiaTensor._11 = 2.0f * massFactor; //y� + z�
				inertiaTensor._22 = 2.0f * massFactor; //x� + z�
				inertiaTensor._33 = 2.0f * massFactor; //x� + y�

				inertiaTensor._12 = 0.0f;
				inertiaTensor._13 = 0.0f;
				inertiaTensor._21 = 0.0f;
				inertiaTensor._23 = 0.0f;
				inertiaTensor._31 = 0.0f;
				inertiaTensor._32 = 0.0f;

				return inertiaTensor;
			}
		}

		RigidBody::RigidBody()
			: m_movement(EMovement_t::INVALID)
			, m_mass(DEFAULT_MASS)
			, m_massInverse(DEFAULT_INVERSE_MASS)
			, m_InertiaTensor(DirectX::XMMatrixIdentity())
			, m_InertiaTensorInverse(m_InertiaTensor)
			, m_InertiaTensorInverseWorld(m_InertiaTensor)
			, m_quatRotation(DirectX::XMQuaternionIdentity()) // q(t)
			, m_position() // x(t);
			, m_linearVelocity() // v(t)
			, m_angularVelocity() // w(t), omega(t)
			, m_frameLinearVelocity()
			, m_frameLinearAcceleration()
			, m_linearImpulse()
			, m_angularImpulse()
			, m_colData()
			, m_transform(nullptr)
		{

		}

		RigidBody::RigidBody(Transform* transform, EMovement_t movement) //, VTPosNorTex* vertices, uint32_t vertexCount)
			: m_movement(movement)
			, m_mass(DEFAULT_MASS)
			, m_massInverse(DEFAULT_INVERSE_MASS)
			, m_InertiaTensor(DirectX::XMMatrixIdentity())
			, m_InertiaTensorInverse(m_InertiaTensor)
			, m_InertiaTensorInverseWorld(m_InertiaTensor)
			, m_quatRotation(transform->GetRotation()) // q(t)
			, m_position(transform->GetPosition()) // x(t);
			, m_linearVelocity() // v(t)
			, m_angularVelocity() // w(t), omega(t)
			, m_frameLinearVelocity()
			, m_frameLinearAcceleration()
			, m_linearImpulse()
			, m_angularImpulse()
			, m_colData()
			, m_transform(transform)
		{
			DirectX::XMFLOAT3X3 inertiaTensor = CreateInertiaTensorBox(m_mass);
			m_InertiaTensor = DirectX::XMLoadFloat3x3(&inertiaTensor);
			m_InertiaTensorInverse = DirectX::XMMatrixInverse(nullptr, m_InertiaTensor);

			//TODO: If no collisionData is provided -> Create the CollisionData!
		}

		RigidBody::RigidBody(Transform* transform, EMovement_t movement, const CollisionData& colData)
			: m_movement(movement)
			, m_mass(DEFAULT_MASS)
			, m_massInverse(DEFAULT_INVERSE_MASS)
			, m_InertiaTensor(DirectX::XMMatrixIdentity())
			, m_InertiaTensorInverse(m_InertiaTensor)
			, m_InertiaTensorInverseWorld(m_InertiaTensor)
			, m_quatRotation(transform->GetRotation()) // q(t)
			, m_position(transform->GetPosition()) // x(t);
			, m_linearVelocity() // v(t)
			, m_angularVelocity() // w(t), omega(t)
			, m_frameLinearVelocity()
			, m_frameLinearAcceleration()
			, m_linearImpulse()
			, m_angularImpulse()
			, m_colData(colData)
			, m_transform(transform)
		{
			DirectX::XMFLOAT3X3 inertiaTensor = CreateInertiaTensorBox(m_mass);
			m_InertiaTensor = DirectX::XMLoadFloat3x3(&inertiaTensor);
			m_InertiaTensorInverse = DirectX::XMMatrixInverse(nullptr, m_InertiaTensor);
		}

		void RigidBody::UpdateInertiaTensorWorld()
		{
			//inverseInertiaTensorFloat, rotationFloat
			DirectX::XMFLOAT3X3 iitF, rotF;
			DirectX::XMFLOAT3X3 iitWorld;
			DirectX::XMStoreFloat3x3(&iitF, m_InertiaTensorInverse);
			{
				DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationQuaternion(m_quatRotation);
				DirectX::XMStoreFloat3x3(&rotF, rotMatrix);
			}
			
			float t04 =	rotF._11 * iitF._11 +
						rotF._12 * iitF._21 +
						rotF._13 * iitF._31;
			float t09 =	rotF._11 * iitF._12 +
						rotF._12 * iitF._22 +
						rotF._13 * iitF._32;
			float t14 =	rotF._11 * iitF._13 +
						rotF._12 * iitF._23 +
						rotF._13 * iitF._33;

			float t28 =	rotF._21 * iitF._11 +
						rotF._22 * iitF._21 +
						rotF._23 * iitF._31;
			float t33 =	rotF._21 * iitF._12 +
						rotF._22 * iitF._22 +
						rotF._23 * iitF._32;
			float t38 =	rotF._21 * iitF._13 +
						rotF._22 * iitF._23 +
						rotF._23 * iitF._33;

			float t52 =	rotF._31 * iitF._11 +
						rotF._32 * iitF._21 +
						rotF._33 * iitF._31;
			float t57 =	rotF._31 * iitF._12 +
						rotF._32 * iitF._22 +
						rotF._33 * iitF._32;
			float t62 =	rotF._31 * iitF._13 +
						rotF._32 * iitF._23 +
						rotF._33 * iitF._33;

			iitWorld._11 =	t04 * rotF._11 +
							t09 * rotF._12 +
							t14 * rotF._13;
			iitWorld._12 =	t04 * rotF._21 +
							t09 * rotF._22 +
							t14 * rotF._23;
			iitWorld._13 =	t04 * rotF._31 +
							t09 * rotF._32 +
							t14 * rotF._33;
			iitWorld._21 =	t28 * rotF._11 +
							t33 * rotF._12 +
							t38 * rotF._13;
			iitWorld._22 =	t28 * rotF._21 +
							t33 * rotF._22 +
							t38 * rotF._23;
			iitWorld._23 =	t28 * rotF._31 +
							t33 * rotF._32 +
							t38 * rotF._33;
			iitWorld._31 =	t52 * rotF._11 +
							t57 * rotF._12 +
							t62 * rotF._13;
			iitWorld._32 =	t52 * rotF._21 +
							t57 * rotF._22 +
							t62 * rotF._23;
			iitWorld._33 =	t52 * rotF._31 +
							t57 * rotF._32 +
							t62 * rotF._33;

			m_InertiaTensorInverseWorld = DirectX::XMLoadFloat3x3(&iitWorld);
		}

		void RigidBody::UpdateTransform()
		{
			m_transform->SetPosition(m_position);
			m_transform->SetRotation(m_quatRotation);
		}

		void RigidBody::ForceRotation(float x, float y, float z)
		{
			m_transform->SetRotationAngles(x, y, z);
			m_quatRotation = m_transform->GetRotation();
		}
	}
}