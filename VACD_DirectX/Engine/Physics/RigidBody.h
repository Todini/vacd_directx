#pragma once
#include "PhysicsEnums.h"

#include "..\Rendering\VertexTypes.h"
#include "CollisionData.h"

namespace ICE
{
	class Transform;

	namespace PHYS
	{
		class RigidBody
		{
		public:
			RigidBody();
			RigidBody(Transform* transform, EMovement_t movement);
			RigidBody(Transform* transform, EMovement_t movement, const CollisionData& colData);

			void UpdateInertiaTensorWorld();

			void UpdateTransform();
			void ForceRotation(float x, float y, float z);

		public:

			EMovement_t m_movement;

			//Constants
			float m_mass;	// mass M
			float m_massInverse;
			DirectX::XMMATRIX m_InertiaTensor; // Ibody
			DirectX::XMMATRIX m_InertiaTensorInverse; // Ibody Inverse
			DirectX::XMMATRIX m_InertiaTensorInverseWorld; // Ibody Inverse

			DirectX::XMVECTOR m_quatRotation; // q(t)
			Vec3 m_position; // x(t);
			Vec3 m_linearVelocity; // v(t)
			Vec3 m_angularVelocity; // w(t), omega(t)

			//Frame variables
			Vec3 m_frameLinearVelocity;
			Vec3 m_frameLinearAcceleration;

			Vec3 m_linearImpulse;
			Vec3 m_angularImpulse;

			// Computed quantities
			//Vec3 m_force; // F(t)
			//Vec3 m_torque; // T(t)
			//Vec3 m_linearAcceleration; //linearMomentum // P(t)
			//Vec3 m_angularAcceleration; //angularMomentum // L(t)
			//DirectX::XMMATRIX m_IInverse; //I^(-1)(t)

			CollisionData m_colData;

			Transform* m_transform;
		};



	}
}