#include "Camera.h"
#include "Graphics.h"
#include "..\Input.h"

namespace
{
	const float MOVEMENT_DEFAULT = 1.0f;
	const float MOVEMENT_SPRINT = 5.0f;
}

namespace ICE
{
	namespace RENDER
	{
		Camera::Camera(float startX, float startY, float startZ)
			: m_input(INPUT::g_Input)
		{
			transform.SetPosition(startX, startY, startZ);
		}

		void Camera::Update(float dt)
		{
			using namespace DirectX;

			float x = 0.0f, y = 0.0f, z = 0.0f;

			if (m_input->IsKeyPressed(m_input->playerInputs.moveForward))	{ z += 1.0f; }
			if (m_input->IsKeyPressed(m_input->playerInputs.moveBackward))	{ z -= 1.0f; }
			if (m_input->IsKeyPressed(m_input->playerInputs.moveRight))		{ x += 1.0f; }
			if (m_input->IsKeyPressed(m_input->playerInputs.moveLeft))		{ x -= 1.0f; }
			if (m_input->IsKeyPressed(m_input->playerInputs.moveUp))		{ y += 1.0f; }
			if (m_input->IsKeyPressed(m_input->playerInputs.moveDown))		{ y -= 1.0f; }
			
			DirectX::XMVECTOR velocity = { x, y, z};
			//Normalizing movement-vector if user is moving diagonally
			velocity = DirectX::XMVector4Normalize(velocity);

			//Multiply by the sprint-modifier (if sprinting)
			velocity *= (m_input->IsKeyPressed(m_input->playerInputs.sprint)) ? MOVEMENT_SPRINT : MOVEMENT_DEFAULT;
			//Multiply by delta-time for consistent movement
			velocity *= dt;

			//CHECK ROTATION
			//Add the mouse-input to the absolute angles
			transform.AddRotationAngles(m_input->GetMouseDeltaX(), m_input->GetMouseDeltaY(), 0.0f);
			//Transform the velocity by our current rotation -> can move upwards and downwards
			DirectX::XMVECTOR transformedVelocity = XMVector3TransformCoord(velocity, XMMatrixRotationQuaternion(transform.GetRotation()));
			transform.AddPosition(transformedVelocity);


			//Create the rotation matrix
			XMMATRIX rotationMatrix = XMMatrixRotationQuaternion(transform.GetRotation());

			//Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin
			XMVECTOR m_upTemp = XMVector3TransformCoord(transform.Up(), rotationMatrix);
			XMVECTOR lookAt = XMVector3TransformCoord(transform.Forward(), rotationMatrix);
			//Translate the rotated camera position to the location of the viewer
			lookAt += transform.GetPositionVector();

			m_viewMatrix = XMMatrixLookAtLH(transform.GetPositionVector(), lookAt, m_upTemp);
		}
	}
}