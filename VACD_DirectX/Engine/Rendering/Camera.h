#pragma once
#include "..\Components\Transform.h"

namespace ICE
{
	namespace INPUT
	{
		class Input;
	}

	namespace RENDER
	{
		class Graphics;

		class Camera
		{
		public:
			Camera(float startX, float startY, float startZ);

			void Update(float dt);

			inline DirectX::XMMATRIX GetViewMatrix()
			{
				return m_viewMatrix;
			}


		public:
			Transform transform;

		private:
			INPUT::Input* m_input;
			Graphics* m_graphics;

			DirectX::XMMATRIX m_viewMatrix;
		};
	}
}