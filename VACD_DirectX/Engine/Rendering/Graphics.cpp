#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")

#include "Graphics.h"
#include <windows.h>
#include <assert.h>

#include <dxgi.h>
#include <d3d11.h>
#include <d3dcompiler.h>

#include <fstream>

#include "..\Memory\MemorySystem.h"

#define G_SHADER_ADDITIONAL_DEBUG_FLAGS 1
#define G_SHADER_ADD_STRICTNESS 0


namespace ICE
{
	namespace RENDER
	{
		namespace
		{
			bool m_isInitialized = false;

			bool m_vsync;
			float m_fieldOfView;
			int32_t m_screenWidth;
			int32_t m_screenHeight;
			float m_aspectRatio;
			size_t m_videoCardMemory;
			char m_videoCardDescription[128];
			HWND m_hwnd;

			DirectX::XMMATRIX m_projectionMatrix;
			DirectX::XMMATRIX m_worldMatrix;
			DirectX::XMMATRIX m_orthoMatrix;

			IDXGISwapChain* m_swapChain;
			ID3D11Device* m_device;
			ID3D11DeviceContext* m_deviceContext;
			ID3D11RenderTargetView* m_renderTargetView;

			ID3D11Texture2D* m_depthStencilBuffer;
			ID3D11DepthStencilState* m_depthStencilState;
			ID3D11DepthStencilState* m_depthDisabledStencilState;
			ID3D11DepthStencilView* m_depthStencilView;
			ID3D11RasterizerState* m_rasterState;

			ID3D11BlendState* m_alphaEnableBlendingState;
			ID3D11BlendState* m_alphaDisableBlendingState;

			void OutputShaderErrorMessage(ID3D10Blob* errorMessage, wchar_t* shaderFileName)
			{
				//If the shader failed to compile it should have writen something to the error message
				if (errorMessage)
				{
					//Get a pointer to the errrer message text buffer
					char* compileErrors = (char*)(errorMessage->GetBufferPointer());
					//Get the length of the message
					size_t bufferSize = errorMessage->GetBufferSize();

					std::ofstream fout;
					fout.open("shader-error.txt");

					//Write out the error message
					for (unsigned long i = 0; i < bufferSize; i++)
					{
						fout << compileErrors[i];
					}

					//Close the file
					fout.close();

					//Release the error message
					errorMessage->Release();

					//Pop a message up on the screen to notify the user to check the text file for compile errors
					MessageBox(m_hwnd, "Error compiling shader. Check shader-error.txt for message", (LPCSTR)shaderFileName, MB_OK);
				}
				else
				{
					MessageBox(m_hwnd, (LPCSTR)shaderFileName, "Missing Shader file!", MB_OK);
				}
				
			}
		}

		void GInit(int32_t screenWidth, int32_t screenHeight, HWND hwnd, bool vsync, bool fullScreen, float distanceNear, float distanceFar, uint32_t multisamplingCount, uint32_t multisamplingQuality)
		{
			assert(m_isInitialized == false);
			m_isInitialized = true;

			m_vsync = vsync;
			m_fieldOfView = static_cast<float>(DirectX::XM_PI) / 2.0f; 
			m_screenWidth = screenWidth;
			m_screenHeight = screenHeight;
			m_aspectRatio = static_cast<float>(screenWidth) / static_cast<float>(screenHeight);
			m_hwnd = hwnd;

			HRESULT result;

			D3D11_DSV_DIMENSION multisamplingDimension = (multisamplingCount == 1u && multisamplingQuality == 0u) ? D3D11_DSV_DIMENSION_TEXTURE2D : D3D11_DSV_DIMENSION_TEXTURE2DMS;

			//Create a DirectX graphics interface factory
			IDXGIFactory* factory;
			result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
			assert(!FAILED(result));

			//Use the factory to create an adapter for the primary graphics interfaces (video card).
			IDXGIAdapter* adapter;
			result = factory->EnumAdapters(0, &adapter);
			assert(!FAILED(result));

			//Enumerate the primary adapter output(monitor)
			IDXGIOutput* adapterOutput;
			result = adapter->EnumOutputs(0, &adapterOutput);
			assert(!FAILED(result));

			//Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor)
			uint32_t numModes = 0u;
			result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, nullptr);
			assert(!FAILED(result));

			//Create a list to hold all the possible display mopdes for this monitor/video card combination
			DXGI_MODE_DESC* displayModeList = new DXGI_MODE_DESC[numModes];
			//DXGI_MODE_DESC* displayModeList = MEMORY_NEW_ARRAY(allocator, "[TEMP]", DXGI_MODE_DESC, numModes);
			//Now fill the display mode list structures
			result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
			//Now go through all display modes and find the one that matches the screen width and height
			//When a match is found store the numerator and denominator of the refresh rate for that monitor
			uint32_t numerator = 0u, denominator = 1u;
			uint32_t screenWidthU = static_cast<uint32_t>(screenWidth), screenHeightU = static_cast<uint32_t>(screenHeight);
			for (uint32_t i = 0; i < numModes; i++)
			{
				if (displayModeList[i].Width == screenWidthU && displayModeList[i].Height == screenHeightU)
				{
					numerator = displayModeList[i].RefreshRate.Numerator;
					denominator = displayModeList[i].RefreshRate.Denominator;
				}
			}

			//Get the adapter (video card) description
			DXGI_ADAPTER_DESC adapterDesc;
			result = adapter->GetDesc(&adapterDesc);
			assert(!FAILED(result));

			//Store the dedicated video card memory in megabytes
			m_videoCardMemory = static_cast<size_t>(adapterDesc.DedicatedVideoMemory / 1024u / 1024u);

			//Convert the name of the video card to a character array and store it.
			size_t stringLength;
			wcstombs_s(&stringLength, m_videoCardDescription, 128, adapterDesc.Description, 128);

			//Release the display mode list
			delete[] displayModeList;
			//MEMORY_FREE_ARRAY(allocator, displayModeList);
			//Release the adapter output
			adapterOutput->Release();
			//Release the adapter
			adapter->Release();
			//Release the factory
			factory->Release();


			//Initialize the swap chain description
			DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
			SecureZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
			//Set to a single back buffer,
			swapChainDesc.BufferCount = 1;
			swapChainDesc.BufferDesc.Width = screenWidthU;
			swapChainDesc.BufferDesc.Height = screenHeightU;
			//Set regular 32-bit surface for the back buffer
			swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
			//Set the refresh rate of the back buffer
			if (m_vsync)
			{
				swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
				swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
			}
			else
			{
				swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
				swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
			}
			//Set the usage of the back buffer
			swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			//Set the handle for the window to render to
			swapChainDesc.OutputWindow = hwnd;
			//Anti-aliasing options
			swapChainDesc.SampleDesc.Count = multisamplingCount;
			swapChainDesc.SampleDesc.Quality = multisamplingQuality;
			//Set to fullscreen or window mode
			swapChainDesc.Windowed = !fullScreen;
			// Set the scan line ordering and scaling to unspecified
			swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			//Discard the back buffer contents
			swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			//Don't set the advanced flags
			swapChainDesc.Flags = 0;

			//Set the feature level to DirectX 11
			D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
			
			//Create the swap chain, Direct3D device and Direct3D device context
			result = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0,
				&featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc, &m_swapChain, &m_device, nullptr, &m_deviceContext);
			if (FAILED(result))
			{
				//User does not have a DX11 card!
				//Using CPU instead ...
				result = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_REFERENCE, nullptr, 0,
					&featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc, &m_swapChain, &m_device, nullptr, &m_deviceContext);

				assert(!FAILED(result));
			}


			uint32_t out = 0;
			for (uint32_t i = 1u; i <= 32u; i++)
			{
				result = m_device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM_SRGB, i, &out);
			}
			//Get the pointer to the back buffer
			ID3D11Texture2D* backBufferPtr;
			result = m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBufferPtr);
			assert(!FAILED(result));

			//Create the render target view with the back buffer pointer
			result = m_device->CreateRenderTargetView(backBufferPtr, nullptr, &m_renderTargetView);
			assert(!FAILED(result));

			//Release the pointer to the back buffer as we no longer need it
			backBufferPtr->Release();


			//Initialize the description of the depth buffer
			D3D11_TEXTURE2D_DESC depthBufferDesc;
			SecureZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

			//Set up the description of the depth buffer
			depthBufferDesc.Width = screenWidthU;
			depthBufferDesc.Height = screenHeightU;
			depthBufferDesc.MipLevels = 1;
			depthBufferDesc.ArraySize = 1;
			depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			depthBufferDesc.SampleDesc.Count = multisamplingCount;
			depthBufferDesc.SampleDesc.Quality = multisamplingQuality;
			depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
			depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			depthBufferDesc.CPUAccessFlags = 0;
			depthBufferDesc.MiscFlags = 0;

			//Create the texture for the depth buffer using the filled out description
			result = m_device->CreateTexture2D(&depthBufferDesc, nullptr, &m_depthStencilBuffer);
			assert(!FAILED(result));


			//Initialize the description of the stencil state
			D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
			SecureZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

			//Set up the description of the stencil state
			depthStencilDesc.DepthEnable = true;
			depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
			depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

			depthStencilDesc.StencilEnable = true;
			depthStencilDesc.StencilReadMask = 0xFF;
			depthStencilDesc.StencilWriteMask = 0xFF;

			//Stencil operations if pixel is front-facing
			depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
			depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
			//Stencil operations if pixel is back-facing
			depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
			depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

			//Create the depth stencil state
			result = m_device->CreateDepthStencilState(&depthStencilDesc, &m_depthStencilState);
			assert(!FAILED(result));
			//Set the depth stencil state
			m_deviceContext->OMSetDepthStencilState(m_depthStencilState, 1);


			//Initialize the depth stencil view
			D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
			SecureZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
			//Set up the depth stencil view description
			depthStencilViewDesc.Format = depthBufferDesc.Format; //DXGI_FORMAT_D24_UNORM_S8_UINT;
																  //Check between multisampling or not
			depthStencilViewDesc.ViewDimension = multisamplingDimension;
			depthStencilViewDesc.Texture2D.MipSlice = 0;
			depthStencilViewDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
			//Create the depth stencil view
			result = m_device->CreateDepthStencilView(m_depthStencilBuffer, &depthStencilViewDesc, &m_depthStencilView);
			assert(!FAILED(result));

			//Bind the render target view and depth stencil buffer to the output render pipeline
			m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);


			//Setup the raster description which will determine how and what polygons will be drawn
			D3D11_RASTERIZER_DESC rasterDesc;
			rasterDesc.AntialiasedLineEnable = true; //false;
			rasterDesc.CullMode = D3D11_CULL_BACK; //D3D11_CULL_BACK;
			rasterDesc.DepthBias = 0;
			rasterDesc.DepthBiasClamp = 0.0f;
			rasterDesc.DepthClipEnable = true;
			rasterDesc.FillMode = D3D11_FILL_SOLID; //D3D11_FILL_WIREFRAME;
			rasterDesc.FrontCounterClockwise = false;
			rasterDesc.MultisampleEnable = false; //true;
			rasterDesc.ScissorEnable = false;
			rasterDesc.SlopeScaledDepthBias = 0.0f;

			//Create the rasterizer state from the description we just filled out
			result = m_device->CreateRasterizerState(&rasterDesc, &m_rasterState);
			assert(!FAILED(result));
			//Now setthe rasterizer state
			m_deviceContext->RSSetState(m_rasterState);


			D3D11_VIEWPORT viewport;
			//Setup the viewport for rendering
			viewport.Width = static_cast<float>(screenWidth);
			viewport.Height = static_cast<float>(screenHeight);
			viewport.MinDepth = 0.0f;
			viewport.MaxDepth = 1.0f;
			viewport.TopLeftX = 0.0f;
			viewport.TopLeftY = 0.0f;
			//Create the viewport
			m_deviceContext->RSSetViewports(1, &viewport);


			// ----------- Initializing Matrices ------------------
			//Setup the projection matrix
			//Create the projection matrix for 3D Rendering
			m_projectionMatrix = DirectX::XMMatrixPerspectiveFovLH(m_fieldOfView, m_aspectRatio, distanceNear, distanceFar);
			//Initialize the world matrix to the identity matrix
			m_worldMatrix = DirectX::XMMatrixIdentity();
			//View Matrix will be generated in the camera class
			//Create an orthographic projection matrix for 2D rendering
			m_orthoMatrix = DirectX::XMMatrixOrthographicLH(static_cast<float>(screenWidth), static_cast<float>(screenHeight), distanceNear, distanceFar);


			D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
			D3D11_BLEND_DESC blendStateDescription;

			// Clear the second depth stencil state before setting the parameters.
			ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));
			// Now create a second depth stencil state which turns off the Z buffer for 2D rendering.  The only difference is 
			// that DepthEnable is set to false, all other parameters are the same as the other depth stencil state.
			depthDisabledStencilDesc.DepthEnable = false;
			depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
			depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
			depthDisabledStencilDesc.StencilEnable = true;
			depthDisabledStencilDesc.StencilReadMask = 0xFF;
			depthDisabledStencilDesc.StencilWriteMask = 0xFF;
			depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
			depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
			depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
			depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
			// Create the state using the device.
			result = m_device->CreateDepthStencilState(&depthDisabledStencilDesc, &m_depthDisabledStencilState);
			assert(SUCCEEDED(result));


			// Clear the blend state description.
			ZeroMemory(&blendStateDescription, sizeof(D3D11_BLEND_DESC));
			// Create an alpha enabled blend state description.
			blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
			blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
			blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
			blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
			blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
			blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;
			// Create the blend state using the description.
			result = m_device->CreateBlendState(&blendStateDescription, &m_alphaEnableBlendingState);
			assert(SUCCEEDED(result));

			// Modify the description to create an alpha disabled blend state description.
			blendStateDescription.RenderTarget[0].BlendEnable = FALSE;
			// Create the blend state using the description.
			result = m_device->CreateBlendState(&blendStateDescription, &m_alphaDisableBlendingState);
			assert(SUCCEEDED(result));
		}
		void GShutdown()
		{
			assert(m_isInitialized == true);
			m_isInitialized = false;

			m_vsync = false;
			m_fieldOfView = 1.0f;
			m_aspectRatio = 1.0f;

			m_swapChain = nullptr;
			m_device = nullptr;
			m_deviceContext = nullptr;
			m_renderTargetView = nullptr;
			m_depthStencilBuffer = nullptr;
			m_depthDisabledStencilState = nullptr;
			m_depthStencilState = nullptr;
			m_depthStencilView = nullptr;
			m_rasterState = nullptr;
			m_alphaEnableBlendingState = nullptr;
			m_alphaDisableBlendingState = nullptr;

			//Before shutting down set to window mode or when you release the swap chain it will throw an exception (?)
			if (m_swapChain)
			{
				m_swapChain->SetFullscreenState(false, nullptr);
			}
			if (m_rasterState)
			{
				m_rasterState->Release();
			}
			if (m_depthStencilView)
			{
				m_depthStencilView->Release();
			}
			if (m_depthStencilState)
			{
				m_depthStencilState->Release();
			}
			if (m_depthStencilBuffer)
			{
				m_depthStencilBuffer->Release();
			}
			if (m_renderTargetView)
			{
				m_renderTargetView->Release();
			}
			if (m_deviceContext)
			{
				m_deviceContext->Release();
			}
			if (m_device)
			{
				m_device->Release();
			}
			if (m_swapChain)
			{
				m_swapChain->Release();
			}
		}

		bool GCompileShaderFromFile(wchar_t* shader, const D3D_SHADER_MACRO* pDefines /* = nullptr*/, ID3DInclude* pInclude /* = nullptr*/,
			char* pEntrypoint, char* pTarget, uint32_t flags1, uint32_t flags2, ID3DBlob** shaderBuffer)
		{
			ID3D10Blob* errorMessage = nullptr;
#if G_SHADER_ADDITIONAL_DEBUG_FLAGS
			flags1 |= D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION | D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_WARNINGS_ARE_ERRORS;
#endif
#if G_SHADER_ADD_STRICTNESS
			flags1 |= D3DCOMPILE_ENABLE_STRICTNESS;
#endif

			HRESULT result = D3DCompileFromFile(shader, pDefines, pInclude, pEntrypoint, pTarget, flags1, flags2,
				shaderBuffer, &errorMessage);
			if (FAILED(result))
			{
				OutputShaderErrorMessage(errorMessage, shader);
				assert(false);
				return false;
			}

			return true;
		}

		bool GCreateVertexShader(ID3D10Blob* shaderBuffer, ID3D11VertexShader** vertexShader)
		{
			HRESULT result = m_device->CreateVertexShader(shaderBuffer->GetBufferPointer(), shaderBuffer->GetBufferSize(), nullptr, vertexShader);
			if (FAILED(result)) { return false; }
			return true;
		}
		bool GCreatePixelShader(ID3D10Blob* shaderBuffer, ID3D11PixelShader** pixelShader)
		{
			HRESULT result = m_device->CreatePixelShader(shaderBuffer->GetBufferPointer(), shaderBuffer->GetBufferSize(), nullptr, pixelShader);
			if (FAILED(result)) { return false; }
			return true;
		}

		bool GCreateInputLayout(D3D11_INPUT_ELEMENT_DESC* inputDesc, uint32_t numElements, ID3DBlob* vertexShaderBuffer, ID3D11InputLayout** inputLayout)
		{
			HRESULT result = m_device->CreateInputLayout(inputDesc, numElements, vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), inputLayout);
			if (FAILED(result)) { return false; }
			return true;
		}



		bool GCreateVertexBuffer(void* vertices, uint32_t vertexCount, uint32_t vertexStride, ID3D11Buffer** vertexBuffer, bool verticesDynamic)
		{
			D3D11_BUFFER_DESC vertexBufferDesc;
			vertexBufferDesc.ByteWidth = vertexStride * vertexCount;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.MiscFlags = 0;
			vertexBufferDesc.StructureByteStride = 0;

			//Set up the description of the static vertex buffer
			if (!verticesDynamic)
			{
				vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
				vertexBufferDesc.CPUAccessFlags = 0;
			}
			else
			{
				vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
				vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			}

			//Give the subresource structure a pointer to the vertex data
			D3D11_SUBRESOURCE_DATA vertexData;
			vertexData.pSysMem = vertices;
			vertexData.SysMemPitch = 0;
			vertexData.SysMemSlicePitch = 0;

			//Now create the vertex buffer
			bool result = GCreateBuffer(&vertexBufferDesc, &vertexData, vertexBuffer);
			return result;
		}
		bool GCreateIndexBuffer(uint32_t* indices, uint32_t indexCount, ID3D11Buffer** indexBuffer, bool indicesDynamic)
		{
			D3D11_BUFFER_DESC indexBufferDesc;
			indexBufferDesc.ByteWidth = sizeof(uint32_t) * indexCount;
			indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexBufferDesc.MiscFlags = 0;
			indexBufferDesc.StructureByteStride = 0;
			
			//Set up the description of the static vertex buffer
			if (!indicesDynamic)
			{
				indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
				indexBufferDesc.CPUAccessFlags = 0;
			}
			else
			{
				indexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
				indexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			}

			//Give the subresource structure a pointer to the vertex data
			D3D11_SUBRESOURCE_DATA indexData;
			indexData.pSysMem = indices;
			indexData.SysMemPitch = 0;
			indexData.SysMemSlicePitch = 0;

			//Now create the vertex buffer
			bool result = GCreateBuffer(&indexBufferDesc, &indexData, indexBuffer);
			return result;
		}
		bool GCreateBuffer(D3D11_BUFFER_DESC* bufferDesc, D3D11_SUBRESOURCE_DATA* bufferData, ID3D11Buffer** buffer)
		{
			//Now create the vertex buffer
			HRESULT result = m_device->CreateBuffer(bufferDesc, bufferData, buffer);

			if (FAILED(result))
			{
				assert(!FAILED(result));
				return false;
			}
			return true;
		}

		bool GCreateTextureSampler(D3D11_SAMPLER_DESC* samplerDesc, ID3D11SamplerState** samplerState)
		{
			HRESULT result = m_device->CreateSamplerState(samplerDesc, samplerState);
			if (FAILED(result))
			{
				assert(false);
				return false;
			}
			return true;
		}

		void GBeginScene(float r, float g, float b, float a)
		{
			//Clear the buffer to the given color
			float color[4];
			color[0] = r;
			color[1] = g;
			color[2] = b;
			color[3] = a;

			//Clear the back buffer
			m_deviceContext->ClearRenderTargetView(m_renderTargetView, color);

			//Clear the depth buffer
			m_deviceContext->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0u);
		}

		void GEndScene()
		{
			//Present the back buffer (frame) to the screen since rendering is complete
			if (m_vsync)
			{
				//Lock to screen refresh rate
				m_swapChain->Present(1, 0);
			}
			else
			{
				//Render as fast as possible
				m_swapChain->Present(0, 0);
			}
		}

		void GSetShader(ID3D11VertexShader* vertexShader, ID3D11InputLayout* inputLayout, ID3D11PixelShader* pixelShader)
		{
			//Set the Vertex Shader
			m_deviceContext->VSSetShader(vertexShader, nullptr, 0u);
			//Set the input layout for the vertices
			m_deviceContext->IASetInputLayout(inputLayout);

			//Set the Pixel Shader
			m_deviceContext->PSSetShader(pixelShader, nullptr, 0u);
		}

		void GSetTextureSampler(uint32_t startSlot, uint32_t count, ID3D11SamplerState *const * textureSampler)
		{
			m_deviceContext->PSSetSamplers(startSlot, count, textureSampler);
		}

		float GGetFOV()
		{
			return m_fieldOfView;
		}
		ID3D11Device* GGetDevice()
		{
			return m_device;
		}
		ID3D11DeviceContext* GGetDeviceContext()
		{
			return m_deviceContext;
		}
		DirectX::XMMATRIX GGetProjectionMatrix()
		{
			return m_projectionMatrix;
		}
	}
}