#include <stdint.h>
#include <DirectXMath.h>

#ifndef _WINDEF_
struct HWND__;
typedef HWND__* HWND;
#endif

struct ID3D11Device;
struct ID3D11DeviceContext;

struct D3D11_BUFFER_DESC;
struct D3D11_SUBRESOURCE_DATA;
struct ID3D11Buffer;

struct ID3DInclude;
struct _D3D_SHADER_MACRO;
typedef _D3D_SHADER_MACRO D3D_SHADER_MACRO;
struct ID3D10Blob;
typedef ID3D10Blob ID3DBlob;

struct ID3D11VertexShader;
struct ID3D11PixelShader;

struct D3D11_INPUT_ELEMENT_DESC;
struct ID3D11InputLayout;

struct D3D11_SAMPLER_DESC;
struct ID3D11SamplerState;

namespace ICE
{
	namespace RENDER
	{
		void GInit(int32_t screenWidth, int32_t screenHeight, HWND hwnd, bool vsync, bool fullScreen, float distanceNear, float distanceFar, uint32_t multisamplingCount, uint32_t multisamplingQuality);
		void GShutdown();

		/*	shader: Shader-Location (L"Shader/Color.vsh)"	
			pEntryPoint: Shader-Function (SimpleColorShader)
			pTarget: Shader-Version (vs_5_0)
		*/
		bool GCompileShaderFromFile(wchar_t* shader, const D3D_SHADER_MACRO* pDefines /* = nullptr*/, ID3DInclude* pInclude /* = nullptr*/, 
									char* pEntrypoint, char* pTarget, uint32_t flags1, uint32_t flags2, ID3DBlob** shaderBuffer);
		bool GCreateVertexShader(ID3D10Blob* shaderBuffer, ID3D11VertexShader** vertexShader);
		bool GCreateInputLayout(D3D11_INPUT_ELEMENT_DESC* inputDesc, uint32_t numElements, ID3DBlob* vertexShaderBuffer, ID3D11InputLayout** inputLayout);
		bool GCreatePixelShader(ID3D10Blob* shaderBuffer, ID3D11PixelShader** pixelShader);

		bool GCreateVertexBuffer(void* vertices, uint32_t vertexCount, uint32_t vertexStride, ID3D11Buffer** vertexBuffer, bool verticesDynamic);
		bool GCreateIndexBuffer(uint32_t* indices, uint32_t indexCount, ID3D11Buffer** indexBuffer, bool indicesDynamic);
		bool GCreateBuffer(D3D11_BUFFER_DESC* bufferDesc, D3D11_SUBRESOURCE_DATA* bufferData, ID3D11Buffer** buffer);

		bool GCreateTextureSampler(D3D11_SAMPLER_DESC* samplerDesc, ID3D11SamplerState** samplerState);

		void GBeginScene(float r, float g, float b, float a);
		void GEndScene();

		void GSetShader(ID3D11VertexShader* vertexShader, ID3D11InputLayout* inputLayout, ID3D11PixelShader* pixelShader);
		void GSetTextureSampler(uint32_t startSlot, uint32_t count, ID3D11SamplerState *const * textureSampler);

		ID3D11Device* GGetDevice();
		ID3D11DeviceContext* GGetDeviceContext();
		float GGetFOV();
		DirectX::XMMATRIX GGetProjectionMatrix();
	}
}