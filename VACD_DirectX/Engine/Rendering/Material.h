#pragma once

struct ID3D11ShaderResourceView;

namespace ICE
{
	namespace RENDER
	{
		struct Material
		{
			ID3D11ShaderResourceView* tex_diffuse;
			ID3D11ShaderResourceView* tex_height;
			ID3D11ShaderResourceView* tex_normal;
		};
	}
}