#pragma once
#include "..\\File\TextureNames.h"
#include<assert.h>


namespace ICE
{
	namespace RENDER
	{
		struct MaterialInfo
		{
			ETextureName_t texName_diffuse = ETextureName_t::NONE;
			ETextureName_t texName_height = ETextureName_t::NONE;
			ETextureName_t texName_normal = ETextureName_t::NONE;
		};
	}
}