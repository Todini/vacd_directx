#include "MaterialManager.h"
#include "Material.h"
#include <assert.h>
#include "..\File\TextureLoader.h"

namespace ICE
{
	namespace RENDER
	{
		namespace
		{
			union MaterialKey
			{
				uint64_t key;
				struct
				{
					uint8_t texName_diffuse;
					uint8_t texName_height;
					uint8_t texName_normal;
					uint8_t pad01;
					uint32_t pad02;
				} data;
			};

			static const uint8_t MAX_DEFAULT_MATERIALS = 32u;

			static MaterialKey m_materialKeys[MAX_DEFAULT_MATERIALS];
			static Material m_materials[MAX_DEFAULT_MATERIALS];
			static uint8_t m_materialCount = 0u;

			static bool m_isInit = false;

			
		}

		namespace
		{
			static MaterialKey MakeKey(const MaterialInfo matInfo)
			{
				MaterialKey matKey;
				matKey.data.texName_diffuse = matInfo.texName_diffuse;
				matKey.data.texName_height = matInfo.texName_height;
				matKey.data.texName_normal = matInfo.texName_normal;
				return matKey;
			}

			static void AddMaterial(const MaterialInfo matInfo)
			{
				assert(m_materialCount != MAX_DEFAULT_MATERIALS);
				//Copy over the MaterialInfo for easier checking if it exists
				m_materialKeys[m_materialCount] = MakeKey(matInfo);
				//Get the next Material-Slot
				Material& material = m_materials[m_materialCount];
				m_materialCount++;

				//Get the actual Texture-Pointers
				material.tex_diffuse = FILE::GetTexture(matInfo.texName_diffuse);
				material.tex_height = FILE::GetTexture(matInfo.texName_height);
				material.tex_normal = FILE::GetTexture(matInfo.texName_normal);
			}

		}

		void InitMaterialManager()
		{
			assert(m_isInit == false);
			m_isInit = true;

			//Adding a default material
			MaterialInfo matInfo;
			matInfo.texName_diffuse = ETextureName_t::LEAVES;
			AddMaterial(matInfo);
		}

		void ShutdownMaterialManager()
		{
			assert(m_isInit == true);
			m_isInit = false;
		}

		const Material* GetDefaultMaterial()
		{
			return m_materials;
		}

		const Material* CreateMaterial(const MaterialInfo matInfo)
		{
			MaterialKey newKey = MakeKey(matInfo);
			MaterialKey* curMaterialKey = m_materialKeys;

			uint8_t matSize = m_materialCount;
			for (uint8_t i = 0u; i < matSize; i++)
			{
				//If both keys are the same then it wants the same Material!
				if (newKey.key == curMaterialKey->key)
				{
					return (m_materials + i);
				}

				curMaterialKey++;
			}

			//MatInfo has not found any matching Materials -> Create new Material

			AddMaterial(matInfo);
			Material* mat = m_materials + (m_materialCount - 1);

			return mat;
		}
	}
}