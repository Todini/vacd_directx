#pragma once
#include "MaterialInfo.h"

namespace ICE
{
	namespace RENDER
	{
		struct Material;

		void InitMaterialManager();
		void ShutdownMaterialManager();

		const Material* GetDefaultMaterial();
		const Material* CreateMaterial(MaterialInfo matInfo);
	}
}