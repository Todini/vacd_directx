#pragma once
#include <stdint.h>
#include <d3d11.h>

#include "..\Memory\MemorySystem.h"
#include "Graphics.h"
#include "Material.h"

namespace ICE
{
	namespace RENDER
	{
		namespace
		{
			static const uint8_t EXTRA_BUFFER = 128u;
		}

		template <typename VertexType>
		class Mesh
		{
		public:
			Mesh()
				: m_memoryArea(0u)
				, m_arena(m_memoryArea)
				, m_stride(0u)
				, m_offset(0u)
				, m_vertexCount(0u)
				, m_indexCount(0u)
				, m_vertices(nullptr)
				, m_indices(nullptr)
				, m_topology()
				, m_vertexBuffer(nullptr)
				, m_indexBuffer(nullptr)
			{
			}

			Mesh(VertexType* vertices, uint32_t vertexCount, uint32_t* indices, uint32_t indexCount, uint32_t stride, uint32_t offset)
				: m_memoryArea(stride * vertexCount + sizeof(uint32_t) * indexCount + EXTRA_BUFFER)
				, m_arena(m_memoryArea)
				, m_stride(stride)
				, m_offset(offset)
				, m_vertexCount(vertexCount)
				, m_indexCount(indexCount)
				, m_vertices(nullptr)
				, m_indices(nullptr)
				, m_topology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
				, m_vertexBuffer(nullptr)
				, m_indexBuffer(nullptr)
				, m_material(nullptr)
				, m_transformWorldMatrix(nullptr)
			{
				//Copying the vertices and indices for later manipulation
				m_vertices = ICE_NEW_ARRAY((&m_arena), ICE::MEMORY::NameTag::GRAPHICS, VertexType, vertexCount);
				memcpy(m_vertices, vertices, vertexCount * stride);
				m_indices = ICE_NEW_ARRAY((&m_arena), ICE::MEMORY::NameTag::GRAPHICS, uint32_t, indexCount);
				memcpy(m_indices, indices, indexCount * sizeof(uint32_t));

				//Creating the Vertex and Index Buffers
				UpdateVertices();
			}
			
			~Mesh()
			{
				if (m_vertexBuffer)
				{
					m_vertexBuffer->Release();
				}
				if (m_indexBuffer)
				{
					m_indexBuffer->Release();
				}

				ICE_DELETE_ARRAY((&m_arena), m_vertices);
				ICE_DELETE_ARRAY((&m_arena), m_indices);
			}

			Mesh(const Mesh& other)
				: m_memoryArea(other.m_memoryArea.GetSize())
				, m_arena(m_memoryArea)
				, m_stride(other.m_stride)
				, m_offset(other.m_offset)
				, m_vertexCount(other.m_vertexCount)
				, m_indexCount(other.m_indexCount)
				, m_vertices(nullptr)
				, m_indices(nullptr)
				, m_topology(other.m_topology)
				, m_vertexBuffer(nullptr)
				, m_indexBuffer(nullptr)
				, m_material(other.m_material)
				, m_transformWorldMatrix(other.m_transformWorldMatrix)
			{
				//Copy other stuff

				//Copying the vertices and indices for later manipulation
				m_vertices = ICE_NEW_ARRAY((&m_arena), ICE::MEMORY::NameTag::GRAPHICS, VertexType, m_vertexCount);
				memcpy(m_vertices, other.m_vertices, m_vertexCount * m_stride);
				m_indices = ICE_NEW_ARRAY((&m_arena), ICE::MEMORY::NameTag::GRAPHICS, uint32_t, m_indexCount);
				memcpy(m_indices, other.m_indices, m_indexCount * sizeof(uint32_t));

				//Creating the Vertex and Index Buffers
				UpdateVertices();
			}

			Mesh& operator=(const Mesh& other)
			{
				if (this != &other)
				{
					//Delete my stuff
					//Copy over other-stuff

					m_stride = other.m_stride;
					m_offset = other.m_offset;
					m_vertexCount = other.m_vertexCount;
					m_indexCount = other.m_indexCount;
					m_topology = other.m_topology;

					m_material = other.m_material;
					m_transformWorldMatrix = other.m_transformWorldMatrix;

					//Cleanup our data
					{
						if (m_vertexBuffer)	
						{
							m_vertexBuffer->Release();
						}
						if (m_indexBuffer)
						{
							m_indexBuffer->Release();
						}
						ICE_DELETE_ARRAY((&m_arena), m_vertices);
						ICE_DELETE_ARRAY((&m_arena), m_indices);
					}

					m_memoryArea = other.m_memoryArea;
					m_arena = SimpleArena(m_memoryArea);

					//Copying the vertices and indices for later manipulation
					m_vertices = ICE_NEW_ARRAY((&m_arena), ICE::MEMORY::NameTag::GRAPHICS, VertexType, m_vertexCount);
					memcpy(m_vertices, other.m_vertices, m_vertexCount * m_stride);
					m_indices = ICE_NEW_ARRAY((&m_arena), ICE::MEMORY::NameTag::GRAPHICS, uint32_t, m_indexCount);
					memcpy(m_indices, other.m_indices, m_indexCount * sizeof(uint32_t));

					UpdateVertices();
				}

				return *this;
			}

			inline void LinkWorldMatrix(const DirectX::XMMATRIX* worldMatrix)
			{
				m_transformWorldMatrix = worldMatrix;
			}

			inline void SetMaterial(const Material* material)
			{
				m_material = material;
			}

			/*	TODO: Make a Renderer that sets these things. Not by the model itself!
			*/
			/*
			void Render()
			{
				Render(m_topology);
			}
			void Render(D3D11_PRIMITIVE_TOPOLOGY topology)
			{
				//Set vertex buffer stride and offset
				unsigned int stride = m_stride;
				unsigned int offset = 0;


				//Set the vertex buffer to active in the input assembler so it can be rendered
				deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

				//Set the index buffer to active in the input assembler so it can be rendered
				deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, offset);

				//Set the type of primitve that should be rendered from this vertex buffer
				deviceContext->IASetPrimitiveTopology(topology);
			}
			*/

			inline ID3D11Buffer* GetVertexBuffer() const
			{
				return m_vertexBuffer;
			}
			inline ID3D11Buffer* GetIndexBuffer() const
			{
				return m_indexBuffer;
			}
			inline uint32_t GetStride() const
			{
				return m_stride;
			}
			inline uint32_t GetOffset() const
			{
				return m_offset;
			}
			inline D3D11_PRIMITIVE_TOPOLOGY GetTopology() const
			{
				return m_topology;
			}

			inline uint32_t GetIndexCount() const
			{
				return m_indexCount;
			}

			inline const Material* GetMaterial() const
			{
				return m_material;
			}

			inline DirectX::XMMATRIX GetWorldMatrix() const
			{
				return *m_transformWorldMatrix;
			}

			inline VertexType* GetVertices()
			{
				return m_vertices;
			}

			inline void UpdateVertices()
			{
				//Creating the Vertex and Index Buffers
				GCreateVertexBuffer(m_vertices, m_vertexCount, m_stride, &m_vertexBuffer, false);
				GCreateIndexBuffer(m_indices, m_indexCount, &m_indexBuffer, false);
			}
			inline uint32_t GetVertexCount() const
			{
				return m_vertexCount;
			}

		private:
			MEMORY::HeapArea m_memoryArea;
			SimpleArena m_arena;

			uint32_t m_stride;
			uint32_t m_offset;
			uint32_t m_vertexCount;
			uint32_t m_indexCount;

			VertexType* m_vertices;
			uint32_t* m_indices;

			D3D11_PRIMITIVE_TOPOLOGY m_topology;
			ID3D11Buffer* m_vertexBuffer;
			ID3D11Buffer* m_indexBuffer;

			const Material* m_material;
			const DirectX::XMMATRIX* m_transformWorldMatrix;

		};

	}
}