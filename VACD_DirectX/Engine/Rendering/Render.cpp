#include "Renderer.h"
#include "Graphics.h"
#include "..\WorldState.h"
#include "Camera.h"

#include <d3d11.h>

#include "Shader\ColorShader.h"
#include "Shader\SimpleColorShader.h"
#include "Shader\SimpleTextureShader.h"

namespace
{
	static const int MAXIMUM_STATIC_LIGHTS = 8;
	static const int MAXIMUM_DYNAMIC_LIGHTS = 4;

	bool isInitialized = false;

	struct CBBasicMatricesVSB0
	{
		DirectX::XMMATRIX view;
		DirectX::XMMATRIX projection;
	};


	/*
	struct CBBasicStaticLightsVSB1
	{
		DirectX::XMMATRIX staticLightViewMatrix[MAXIMUM_STATIC_LIGHTS];
		DirectX::XMMATRIX staticLightProjectionMatrix[MAXIMUM_STATIC_LIGHTS];
		DirectX::XMFLOAT3 staticLightPosition[MAXIMUM_STATIC_LIGHTS];
		uint32_t staticLightCount;
		DirectX::XMFLOAT3 staticEyePosition;
		float PAD01;
	};

	struct CBBasicDynamicLightsVSB2
	{
		DirectX::XMMATRIX dynamicLightViewMatrix[MAXIMUM_STATIC_LIGHTS];
		DirectX::XMMATRIX dynamicLightProjectionMatrix[MAXIMUM_STATIC_LIGHTS];
		DirectX::XMFLOAT3 dynamicLightPosition[MAXIMUM_STATIC_LIGHTS];
		uint32_t dynamicLightCount;
		DirectX::XMFLOAT3 dynamicEyePosition;
		float PAD01;
	}
	*/

	void XM_CALLCONV UploadBasicConstantBuffers(DirectX::FXMMATRIX viewMatrixTR, ID3D11Buffer* cb_vs)
	{
		ID3D11DeviceContext* deviceContext = ICE::RENDER::GGetDeviceContext();
		DirectX::XMMATRIX projectionMatrixTR = XMMatrixTranspose(ICE::RENDER::GGetProjectionMatrix());

		HRESULT result;
		{
			//Lock the constant buffer so it can be written to
			D3D11_MAPPED_SUBRESOURCE mappedResource;
			result = deviceContext->Map(cb_vs, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
			assert(SUCCEEDED(result));

			//Get a pointer to the data in the constant buffer
			CBBasicMatricesVSB0* cbMatricesVsB0 = (CBBasicMatricesVSB0*)mappedResource.pData;

			//Copy the matrices into the constant buffer
			cbMatricesVsB0->view = viewMatrixTR;
			cbMatricesVsB0->projection = projectionMatrixTR;

			//Unlock the constant buffer
			deviceContext->Unmap(cb_vs, 0);
		}

		deviceContext->VSSetConstantBuffers(0, 1, &cb_vs);
	}

}

namespace ICE
{
	namespace RENDER
	{
		void Renderer::Init()
		{
			assert(isInitialized == false);
			isInitialized = true;

			bool result = false;
			{	//CONSTANT BUFFERS
				D3D11_BUFFER_DESC bufferDesc;

				//Setup the description of the dynamic constant buffer
				//Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail
				bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
				bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
				bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
				bufferDesc.MiscFlags = 0;
				bufferDesc.StructureByteStride = 0;

				//Create the constant buffer pointer so we can access the constant bufffer from withing this class
				bufferDesc.ByteWidth = sizeof(CBBasicMatricesVSB0);
				result = GCreateBuffer(&bufferDesc, nullptr, &m_cb_basicMatrices_vs_b0);
				assert(result);
			}	//CONSTANT BUFFERS

			{	//TEXTURE SAMPLERS
			// Create a texture sampler state description
				D3D11_SAMPLER_DESC samplerDesc;

				samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR; //D3D11_FILTER_ANISOTROPIC;
				samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDesc.MipLODBias = 0.0f;
				samplerDesc.MaxAnisotropy = 1u;
				samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
				samplerDesc.BorderColor[0] = 0.0f;
				samplerDesc.BorderColor[1] = 0.0f;
				samplerDesc.BorderColor[2] = 0.0f;
				samplerDesc.BorderColor[3] = 0.0f;
				samplerDesc.MinLOD = 0.0f;
				samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

				//Create the texture sampler state
				GCreateTextureSampler(&samplerDesc, &m_samplerStateMipLinearWrap);

				// Create a texture sampler state description
				samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT; //D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT; // D3D11_FILTER_MIN_MAG_MIP_LINEAR;
				samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
				samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
				samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
				samplerDesc.MipLODBias = 0.0f;
				samplerDesc.MaxAnisotropy = 1u; // D3D11_DEFAULT_MAX_ANISOTROPY;
				samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
				samplerDesc.BorderColor[0] = 0.0f;
				samplerDesc.BorderColor[1] = 0.0f;
				samplerDesc.BorderColor[2] = 0.0f;
				samplerDesc.BorderColor[3] = 0.0f;
				samplerDesc.MinLOD = 0.0f;
				samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

				//Create the texture sampler state
				GCreateTextureSampler(&samplerDesc, &m_samplerStateShadow);


				GSetTextureSampler(0u, 1u, &m_samplerStateMipLinearWrap);
				//GSetTextureSampler(1u, 1u, &m_samplerStateMipLinearWrap);
				//GSetTextureSampler(2u, 1u, &m_samplerStateMipLinearWrap);
				GSetTextureSampler(3u, 1u, &m_samplerStateShadow);
			}	//TEXTURE SAMPLERS


			m_colorShader = new ColorShader(L"Shader/ColorVS.hlsl", L"Shader/ColorPS.hlsl");
			m_simpleColorShader = new SimpleColorShader(L"Shader/SimpleColorShaderVS.hlsl", L"Shader/SimpleColorShaderPS.hlsl");
			m_simpleTextureShader = new SimpleTextureShader(L"Shader/SimpleTextureShaderVS.hlsl", L"Shader/SimpleTextureShaderPS.hlsl");
		}

		void Renderer::Shutdown()
		{
			assert(isInitialized == true);
			isInitialized = false;
		
			if (m_cb_basicMatrices_vs_b0)
			{
				m_cb_basicMatrices_vs_b0->Release();
			}

			delete m_simpleColorShader;
		}

		void Renderer::Render(WorldState* worldState)
		{
			//Begin the Scene
			GBeginScene(0.25f, 0.25f, 0.25f, 1.0f);

			DirectX::XMMATRIX viewMatrixTR = XMMatrixTranspose(worldState->camera->GetViewMatrix());
			//Update the per-frame ConstantBuffer
			UploadBasicConstantBuffers(viewMatrixTR, m_cb_basicMatrices_vs_b0);

			//Render all objects via the shaders

			m_colorShader->Render(worldState->compounds);
			m_simpleColorShader->Render(worldState->meshesColor, worldState->meshesColorCount);
			m_simpleTextureShader->Render(worldState->meshesTexture, worldState->meshesTextureCount);

			//End the Scene
			GEndScene();
		}
	}
}