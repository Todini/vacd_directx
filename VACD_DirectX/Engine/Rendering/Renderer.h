#pragma once
#pragma comment(lib, "D3DCompiler.lib")

struct ID3D11Buffer;
struct ID3D11SamplerState;

namespace ICE
{
	struct WorldState;

	namespace RENDER
	{
		class ColorShader;
		class SimpleColorShader;
		class SimpleTextureShader;

		class Renderer
		{
		public:
			void Init();
			void Shutdown();

			void Render(WorldState* worldState);

		private:
			//Constant Buffers
			ID3D11Buffer* m_cb_basicMatrices_vs_b0;

			//Texture Samplers
			ID3D11SamplerState* m_samplerStateMipLinearWrap;
			ID3D11SamplerState* m_samplerStateShadow;

			ColorShader* m_colorShader;
			SimpleColorShader* m_simpleColorShader;
			SimpleTextureShader* m_simpleTextureShader;
		};
	}
}