#pragma once
#pragma comment(lib, "D3DCompiler.lib")

#include "..\VertexTypes.h"
#include "..\Mesh.h"

#include "..\..\VACD\Compound.h"

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;

namespace ICE
{
	namespace FRAC
	{
		class Compound;
	}

	namespace RENDER
	{
		class ColorShader
		{
		public:
			ColorShader(wchar_t* vertexShader, wchar_t* pixelShader);

			void Render(std::vector<FRAC::Compound>& compounds);

		private:
			void SetShader();
			void UpdateConstantBuffers(DirectX::FXMMATRIX worldMatrix);

		private:
			ID3D11VertexShader* m_vertexShader;
			ID3D11PixelShader* m_pixelShader;
			ID3D11InputLayout* m_layout;

			ID3D11Buffer* m_cb_Matrices_vs_b4;

		};
	}
}