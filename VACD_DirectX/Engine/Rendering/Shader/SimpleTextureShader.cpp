#include "SimpleTextureShader.h"

#include <d3d11.h>
#include <d3dcompiler.h>

#include "..\Graphics.h"

namespace
{
	struct CBMatricesVSB4
	{
		DirectX::XMMATRIX world;
	};

	void UpdateConstantBuffers(ID3D11DeviceContext* deviceContext, DirectX::FXMMATRIX worldMatrixTR, ID3D11Buffer** cb_vs_b4)
	{
		HRESULT result;
		{
			//Lock the constant buffer so it can be written to
			D3D11_MAPPED_SUBRESOURCE mappedResource;
			result = deviceContext->Map(*cb_vs_b4, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
			assert(SUCCEEDED(result));

			//Get a pointer to the data in the constant buffer
			CBMatricesVSB4* cbMatricesVsB4 = (CBMatricesVSB4*)mappedResource.pData;

			//Copy the matrices into the constant buffer
			cbMatricesVsB4->world = worldMatrixTR;

			//Unlock the constant buffer
			deviceContext->Unmap(*cb_vs_b4, 0);
		}

		deviceContext->VSSetConstantBuffers(4, 1, cb_vs_b4);
	}

	void UpdateTextures(ID3D11DeviceContext* deviceContext, const ICE::RENDER::Material* material)
	{
		deviceContext->PSSetShaderResources(0u, 1, &(material->tex_diffuse));

	}
}

namespace ICE
{
	namespace RENDER
	{
		SimpleTextureShader::SimpleTextureShader(wchar_t* vertexShader, wchar_t* pixelShader)
		{
			ID3D10Blob* vertexShaderBuffer = nullptr;

			GCompileShaderFromFile(vertexShader, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "SimpleTextureShader", "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &vertexShaderBuffer);
			GCreateVertexShader(vertexShaderBuffer, &m_vertexShader);

			ID3D10Blob* pixelShaderBuffer = nullptr;
			GCompileShaderFromFile(pixelShader, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, "SimpleTextureShader", "ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &pixelShaderBuffer);
			GCreatePixelShader(pixelShaderBuffer, &m_pixelShader);

			/*
			D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			*/
			D3D11_INPUT_ELEMENT_DESC polygonLayout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};
			//Get a count of the elements in the layout
			uint32_t numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);
			//Create the vertex input layout
			GCreateInputLayout(polygonLayout, numElements, vertexShaderBuffer, &m_layout);

			vertexShaderBuffer->Release();

			bool result = false;
			D3D11_BUFFER_DESC bufferDesc;
			//Setup the description of the dynamic constant buffer
			//Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail
			bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			bufferDesc.MiscFlags = 0;
			bufferDesc.StructureByteStride = 0;

			//Create the constant buffer pointer so we can access the constant bufffer from withing this class
			bufferDesc.ByteWidth = sizeof(CBMatricesVSB4);
			result = GCreateBuffer(&bufferDesc, nullptr, &m_cb_Matrices_vs_b4);
			assert(result);
		}

		void SimpleTextureShader::Render(const Mesh<VTPosNorTex>* meshes, uint32_t count)
		{
			GSetShader(m_vertexShader, m_layout, m_pixelShader);

			const Mesh<VTPosNorTex>* mesh = meshes;
			ID3D11DeviceContext* deviceContext = ICE::RENDER::GGetDeviceContext();
			DirectX::XMMATRIX worldMatrixTR;

			for (uint32_t i = 0u; i < count; i++)
			{
				worldMatrixTR = DirectX::XMMatrixTranspose(mesh->GetWorldMatrix());
				UpdateConstantBuffers(deviceContext, worldMatrixTR, &m_cb_Matrices_vs_b4);
				UpdateTextures(deviceContext, mesh->GetMaterial());

				{	//Render the mesh
					ID3D11Buffer* vertexBuffer = mesh->GetVertexBuffer();
					uint32_t stride = mesh->GetStride();
					uint32_t offset = mesh->GetOffset();

					//Set the vertex buffer to active in the input assembler so it can be rendered
					deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
					//Set the index buffer to active in the input assembler so it can be rendered
					deviceContext->IASetIndexBuffer(mesh->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, offset);
					//Set the type of primitve that should be rendered from this vertex buffer
					deviceContext->IASetPrimitiveTopology(mesh->GetTopology());
				}

				deviceContext->DrawIndexed(mesh->GetIndexCount(), 0, 0);

				mesh++;
			}

		}
	}
}