#pragma once
#include <DirectXMath.h>

struct VTPosCol
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT4 color;
};

struct VTPosColNor
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT4 color;
	DirectX::XMFLOAT3 normal;
};

struct VTPosNorTex
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 normal;
	DirectX::XMFLOAT2 texcoords;
};