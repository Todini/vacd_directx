#include "Timer.h"
#include <Windows.h>

namespace ICE
{

	Timer::Timer()
	{
		LARGE_INTEGER performanceFrequencyCounter;
		QueryPerformanceFrequency(&performanceFrequencyCounter);
		m_performanceFrequency = performanceFrequencyCounter.QuadPart;

		QueryPerformanceCounter(&m_previousTime);
	}

	float Timer::Tick()
	{
		LARGE_INTEGER currentTime;
		QueryPerformanceCounter(&currentTime);
		deltaTime = (static_cast<float>(currentTime.QuadPart - m_previousTime.QuadPart) / static_cast<float>(m_performanceFrequency));
		m_previousTime = currentTime;

		return deltaTime;
	}

	float Timer::GetDeltaTime()
	{
		return deltaTime;
	}
}