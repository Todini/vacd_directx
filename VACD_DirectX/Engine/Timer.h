#pragma once
#include <Windows.h>

namespace ICE
{
	class Timer
	{
	public:
		Timer();

		//Ticks the time and updates deltaTime. Also returns the deltaTime
		float Tick();
		float GetDeltaTime();

	private:
		//Frequency of the PerformanceCounter
		INT64 m_performanceFrequency;
		//Last update of the performanceCounter
		LARGE_INTEGER m_previousTime;
		//Useable deltaTime
		float deltaTime;
	};

}