#pragma once
#include <DirectXMath.h>
#include "Vec.h"

namespace ICE
{
	namespace MATH
	{
		// http://mathinsight.org/matrix_vector_multiplication
		inline Vec4 MatrixMultiplyVector(const DirectX::XMMATRIX& matrix, Vec4 vector)
		{
			DirectX::XMFLOAT4X4 matrixFloat;
			DirectX::XMStoreFloat4x4(&matrixFloat, matrix);

			//TODO:  Should be rewritten to be more compiler friendly
			Vec4 result;
			result.x = matrixFloat._11 * vector.x + matrixFloat._12 * vector.y + matrixFloat._13 * vector.z + matrixFloat._14 * vector.w;
			result.y = matrixFloat._21 * vector.x + matrixFloat._22 * vector.y + matrixFloat._23 * vector.z + matrixFloat._24 * vector.w;
			result.z = matrixFloat._31 * vector.x + matrixFloat._32 * vector.y + matrixFloat._33 * vector.z + matrixFloat._34 * vector.w;
			result.w = matrixFloat._41 * vector.x + matrixFloat._42 * vector.y + matrixFloat._43 * vector.z + matrixFloat._44 * vector.w;

			return result;
		}

		inline Vec3 MatrixMultiplyVector(const DirectX::XMMATRIX& matrix, Vec3 vector)
		{
			DirectX::XMFLOAT3X3 matrixFloat;
			DirectX::XMStoreFloat3x3(&matrixFloat, matrix);

			Vec3 result;
			result.x = matrixFloat._11 * vector.x + matrixFloat._12 * vector.y + matrixFloat._13 * vector.z;
			result.y = matrixFloat._21 * vector.x + matrixFloat._22 * vector.y + matrixFloat._23 * vector.z;
			result.z = matrixFloat._31 * vector.x + matrixFloat._32 * vector.y + matrixFloat._33 * vector.z;

			return result;
		}

		inline DirectX::XMMATRIX CreateMatrixFromCoordinatSystem(Vec3 x, Vec3 y, Vec3 z)
		{
			DirectX::XMFLOAT3X3 cs;
#if 0
			cs._11 = x.x;
			cs._12 = x.y;
			cs._13 = x.z;
			cs._21 = y.x;
			cs._22 = y.y;
			cs._23 = y.z;
			cs._31 = z.x;
			cs._32 = z.y;
			cs._33 = z.z;
#else
			cs._11 = x.x;
			cs._21 = x.y;
			cs._31 = x.z;
			cs._12 = y.x;
			cs._22 = y.y;
			cs._32 = y.z;
			cs._13 = z.x;
			cs._23 = z.y;
			cs._33 = z.z;
#endif

			DirectX::XMMATRIX result = DirectX::XMLoadFloat3x3(&cs);
			return result;
		}

		inline Vec3 MatrixTransform3X3(const DirectX::XMMATRIX& matrix, Vec3 vector)
		{
			DirectX::XMFLOAT3X3 matrixF;
			DirectX::XMStoreFloat3x3(&matrixF, matrix);

			Vec3 result = Vec3();
			result.x =  matrixF._11 * vector.x;
			result.x += matrixF._12 * vector.y;
			result.x += matrixF._13 * vector.z;

			result.y =  matrixF._21 * vector.x;
			result.y += matrixF._22 * vector.y;
			result.y += matrixF._23 * vector.z;

			result.z =  matrixF._31 * vector.x;
			result.z += matrixF._32 * vector.y;
			result.z += matrixF._33 * vector.z;

			return result;
		}
	}
}