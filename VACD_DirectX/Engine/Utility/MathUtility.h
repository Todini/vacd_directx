#pragma once
#include <DirectXMath.h>
#include "..\Components\Transform.h"
#include "Vec.h"

namespace ICE
{
	namespace MATH
	{
		const double DEGREETORAD = 0.01745329251994329576923690768489f;
		const double RADTODEGREE = 57.295779513082320876798154814105f;

		/*	This is a Cubic Hermite spline that uses Kochanek-Bartels algorithm to create the tangents.
			The Cubic Hermite spline uses the current and next points and their tangents to smoothly interpolate between them.

			To better accomodate for movement changes Kochanek-Bartels algortihm is used to create the tangets.
			The tangent of the currentPoint is created with the information of the previous, current and next points.
			The tangent of our nextPoint is created with the information of the current, next and next2 points.
		*/
		inline DirectX::XMVECTOR PositionInterpolationKochanekBartel(DirectX::XMVECTOR& previousPoint, DirectX::XMVECTOR& currentPoint, DirectX::XMVECTOR& nextPoint, DirectX::XMVECTOR& next2Point, float& time, float tension = 0.0f, float bias = 0.0f, float continuity = 0.0f)
		{
			if (time >= 1.0f) { time = 1.0f; }
			using namespace DirectX;

			XMVECTOR incomingTangent = (((1.0f - tension) * (1.0f + bias) * (1.0f + continuity)) / 2.0f) * (currentPoint - previousPoint)
				+ (((1.0f - tension) * (1.0f - bias) * (1.0f - continuity)) / 2.0f) * (nextPoint - previousPoint);
			XMVECTOR outgoingTangent = (((1.0f - tension) * (1.0f + bias) * (1.0f - continuity)) / 2.0f) * (nextPoint - currentPoint)
				+ (((1.0f - tension) * (1.0f - bias) * (1.0f + continuity)) / 2.0f) * (next2Point - nextPoint);

			float time2 = time*time;
			float time3 = time2*time;

			XMVECTOR result = (2 * time3 - 3 * time2 + 1) * currentPoint
				+ (time3 - 2 * time2 + time) * incomingTangent
				+ (-2 * time3 + 3 * time2) * nextPoint
				+ (time3 - time2) * outgoingTangent;

			return result;
		}
		inline DirectX::XMVECTOR PositionInterpolationKochanekBartel(Transform& previous, Transform& current, Transform& next, Transform& next2, float& time, float t = 0.0f, float b = 0.0f, float c = 0.0f)
		{
			if (time >= 1.0f) { time = 1.0f; }
			DirectX::XMVECTOR previousT = previous.GetPositionVector();
			DirectX::XMVECTOR currentT = current.GetPositionVector();
			DirectX::XMVECTOR nextT = next.GetPositionVector();
			DirectX::XMVECTOR next2T = next2.GetPositionVector();
			return PositionInterpolationKochanekBartel(previousT, currentT, nextT, next2T, time, t, b, c);
		}

		inline DirectX::XMVECTOR QuaternionSQUADControlPoint(DirectX::XMVECTOR& pre, DirectX::XMVECTOR& cur, DirectX::XMVECTOR next)
		{
			using namespace DirectX;

			XMVECTOR innerLeft = XMQuaternionLn(XMQuaternionMultiply(XMQuaternionExp(cur), next));
			XMVECTOR innerRight = XMQuaternionLn(XMQuaternionMultiply(XMQuaternionExp(cur), pre));

			XMVECTOR inner = -0.25f * (innerLeft + innerRight);

			XMVECTOR result = XMQuaternionMultiply(cur, XMQuaternionExp(inner));

			return result;
		}
		/* Spherical and Quadrangle Interpolation

			Understanding and theory: http://www.3dgep.com/understanding-quaternions/#SQUAD
			SquadSetup for controlpoints: https://msdn.microsoft.com/en-us/library/windows/desktop/bb281657%28v=vs.85%29.aspx
			Squad: https://msdn.microsoft.com/en-us/library/windows/desktop/bb281656%28v=vs.85%29.aspx
		*/
		inline DirectX::XMVECTOR QuaternionSQUAD(DirectX::XMVECTOR& previous, DirectX::XMVECTOR& current, DirectX::XMVECTOR& next, DirectX::XMVECTOR& next2, float& time)
		{
			using namespace DirectX;
			XMVECTOR result;

#if 1
			/*	The following lines are used to prevent a bug while interpolating.
				When the angle between current and next are to small, the SQUAD will fail.
				-> at ~50% transition the angle will instantly flip to the other side,
				because the sin(angle) will be near 0, therefore give unpredictable results

				Therefore the angle needs to be checked. If it's too small, only use a Slerp.
			*/
			static XMVECTOR forward = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
			static float margin = 5.0f;
			float angleCurrent = 0.0f;
			float angleNext = 0.0f;
			XMQuaternionToAxisAngle(&forward, &angleCurrent, current);
			XMQuaternionToAxisAngle(&forward, &angleNext, next);

			angleNext -= angleCurrent;
			if (angleNext < margin && angleNext > -margin)
			{
				result = XMQuaternionSlerp(current, next, time);
			}
			else
			{
				XMVECTOR a;
				XMVECTOR b;
				XMVECTOR c;
				XMQuaternionSquadSetup(&a, &b, &c, previous, current, next, next2);
				result = XMQuaternionSquad(current, a, b, c, time);
			}
#else
			XMVECTOR controlPointA = QuaternionSQUADControlPoint(previous, current, next);
			XMVECTOR controlPointB = QuaternionSQUADControlPoint(current, next, next2);

			XMVECTOR resultLeft = XMQuaternionSlerp(current, next, time);
			XMVECTOR resultRight = XMQuaternionSlerp(controlPointA, controlPointB, time);

			result = XMQuaternionSlerp(resultLeft, resultRight, 2 * time*(1.0f - time));
#endif

			return result;
		}
		inline DirectX::XMVECTOR QuaternionSQUAD(Transform& previous, Transform& current, Transform& next, Transform& next2, float& time)
		{
			DirectX::XMVECTOR previousRot = previous.GetRotation();
			DirectX::XMVECTOR currentRot = current.GetRotation();
			DirectX::XMVECTOR nextRot = next.GetRotation();
			DirectX::XMVECTOR next2Rot = next2.GetRotation();

			return QuaternionSQUAD(previousRot, currentRot, nextRot, next2Rot, time);
		}

		inline float LengthSq(float x, float y, float z)
		{
			x *= x;
			y *= y;
			z *= z;

			x += y;
			x += z;

			return x;
		}
		inline float LengthSq(DirectX::XMFLOAT3 vector)
		{
			return LengthSq(vector.x, vector.y, vector.z);
		}
		inline float LengthSq(DirectX::XMVECTOR vector)
		{
			return LengthSq(DirectX::XMVectorGetX(vector), DirectX::XMVectorGetY(vector), DirectX::XMVectorGetZ(vector));
		}
		inline float LengthSq(Vec3 vector)
		{
			return LengthSq(vector.x, vector.y, vector.z);
		}

		inline float Length(float x, float y, float z)
		{
			x = LengthSq(x, y, z);
			x = sqrtf(x);

			return x;
		}
		inline float Length(Vec3 vector)
		{
			return Length(vector.x, vector.y, vector.z);
		}
		inline float Length(DirectX::XMFLOAT3 vector)
		{
			return Length(vector.x, vector.y, vector.z);
		}
		inline float Length(DirectX::XMVECTOR vector)
		{
			return Length(DirectX::XMVectorGetX(vector), DirectX::XMVectorGetY(vector), DirectX::XMVectorGetZ(vector));
		}

		inline void Normalize(DirectX::XMFLOAT3& vector)
		{
			float length = Length(vector);
			vector.x /= length;
			vector.y /= length;
			vector.z /= length;
		}
		inline void Normalize(Vec3& vector)
		{
			float length = Length(vector);
			vector.x /= length;
			vector.y /= length;
			vector.z /= length;
		}
		inline void Normalize(Vec3* vector)
		{
			float length = Length(*vector);
			vector->x /= length;
			vector->y /= length;
			vector->z /= length;
		}

		inline DirectX::XMFLOAT3 Normalized(DirectX::XMFLOAT3 vector)
		{
			DirectX::XMFLOAT3 result;
			float length = Length(vector);
			result.x = vector.x / length;
			result.y = vector.y / length;
			result.z = vector.z / length;

			return result;
		}
		inline Vec3 Normalized(Vec3 vector)
		{
			float length = Length(vector);
			vector /= length;
			
			return vector;
		}

		inline bool IsInside(DirectX::XMFLOAT3& minCorner, DirectX::XMFLOAT3& maxCorner, DirectX::XMFLOAT3& point)
		{
			if (minCorner.x <= point.x && point.x <= maxCorner.x &&
				minCorner.y <= point.y && point.y <= maxCorner.y &&
				minCorner.z <= point.z && point.z <= maxCorner.z)
			{
				return true;
			}

			return false;
		}

		/*	http://stackoverflow.com/questions/30011741/3d-vector-defined-by-2-angles	*/
		inline DirectX::XMFLOAT3 AnglesToDirection(DirectX::XMFLOAT3 angles)
		{
			angles.x *= DEGREETORAD;
			angles.y *= DEGREETORAD;
			angles.z *= DEGREETORAD;

			DirectX::XMFLOAT3 result;

			result.x = (sinf(angles.x) * cosf(angles.y));
			result.y = -(sinf(angles.y));
			result.z = (cosf(angles.x) * cosf(angles.y));

			Normalize(result);
			return result;
		}
		inline Vec3 AnglesToDirection(Vec3 angles)
		{
			angles.x *= DEGREETORAD;
			angles.y *= DEGREETORAD;
			angles.z *= DEGREETORAD;

			Vec3 result;

			result.x = (sinf(angles.x) * cosf(angles.y));
			result.y = -(sinf(angles.y));
			result.z = (cosf(angles.x) * cosf(angles.y));

			Normalize(result);
			return result;
		}

		inline DirectX::XMFLOAT3 RotateVectorWithAngles(DirectX::XMFLOAT3 vector, DirectX::XMFLOAT3 anglesRad)
		{
			DirectX::XMFLOAT3 result = vector;
			DirectX::XMFLOAT3 resultX = vector;
			DirectX::XMFLOAT3 resultY = vector;
			DirectX::XMFLOAT3 resultZ = vector;
			//Note: rotationAngles -> x and y are switched

			//Rotation around x-axis
			resultX.y = vector.y * cosf(anglesRad.y) - vector.z * sinf(anglesRad.y);
			resultX.z = vector.y * sinf(anglesRad.y) + vector.z * cosf(anglesRad.y);

			//Rotation around y-axis
			resultY.x = vector.x * cosf(anglesRad.x) + vector.z * sinf(anglesRad.x);
			resultY.z = -vector.x * sinf(anglesRad.x) + vector.z * cosf(anglesRad.x);

			//Rotation around z-axis
			resultZ.x = vector.x * cosf(anglesRad.z) - vector.y * sinf(anglesRad.z);
			resultZ.y = vector.x * sinf(anglesRad.z) + vector.y * cosf(anglesRad.z);

			return result;
		}
		inline Vec3 RotateVectorWithAngles(Vec3 vector, Vec3 anglesRad)
		{
			Vec3 result = vector;
			Vec3 resultX = vector;
			Vec3 resultY = vector;
			Vec3 resultZ = vector;
			//Note: rotationAngles -> x and y are switched

			//Rotation around x-axis
			resultX.y = vector.y * cosf(anglesRad.y) - vector.z * sinf(anglesRad.y);
			resultX.z = vector.y * sinf(anglesRad.y) + vector.z * cosf(anglesRad.y);

			//Rotation around y-axis
			resultY.x = vector.x * cosf(anglesRad.x) + vector.z * sinf(anglesRad.x);
			resultY.z = -vector.x * sinf(anglesRad.x) + vector.z * cosf(anglesRad.x);

			//Rotation around z-axis
			resultZ.x = vector.x * cosf(anglesRad.z) - vector.y * sinf(anglesRad.z);
			resultZ.y = vector.x * sinf(anglesRad.z) + vector.y * cosf(anglesRad.z);

			return result;
		}
		inline DirectX::XMFLOAT3 RotateVectorWithQuaternion(DirectX::XMFLOAT3 vector, DirectX::XMVECTOR quat)
		{
			DirectX::XMVECTOR v = DirectX::XMVectorSet(vector.x, vector.y, vector.z, 0.0f);
			v = DirectX::XMVector3Rotate(v, quat);

			DirectX::XMFLOAT3 result;
			result.x = DirectX::XMVectorGetX(v);
			result.y = DirectX::XMVectorGetY(v);
			result.z = DirectX::XMVectorGetZ(v);

			return result;
		}
		inline DirectX::XMFLOAT3 RotateVectorWithQuaternionReverse(DirectX::XMFLOAT3 vector, DirectX::XMVECTOR quat)
		{
			DirectX::XMVECTOR v = DirectX::XMVectorSet(vector.x, vector.y, vector.z, 0.0f);
			v = DirectX::XMVector3InverseRotate(v, quat);

			DirectX::XMFLOAT3 result;
			result.x = DirectX::XMVectorGetX(v);
			result.y = DirectX::XMVectorGetY(v);
			result.z = DirectX::XMVectorGetZ(v);

			return result;
		}

		//http://www.programming-techniques.com/2012/03/3d-rotation-algorithm-about-arbitrary.html
		inline DirectX::XMFLOAT3 RotatePointAroundArbitaryAxis(float angleDegree, DirectX::XMFLOAT3 point, DirectX::XMFLOAT3 axis)
		{
			float rotationMatrix[4][4];
			float inputMatrix[4][1] = { point.x, point.y, point.z, 1.0f };
			float outputMatrix[4][1] = { 0.0f, 0.0f, 0.0f, 0.0f };

			/*			Setting up rotation matrix			*/
#if 0
			float lengthSq = LengthSq(axis);
			float length = sqrtf(lengthSq);
			angleDegree *= DEGREETORAD;
			float cosAngle = cosf(angleDegree);
			float sinAngle = sinf(angleDegree);

			DirectX::XMFLOAT3 axisSq = axis;
			axisSq.x *= axisSq.x;
			axisSq.y *= axisSq.y;
			axisSq.z *= axisSq.z;

			rotationMatrix[0][0] = (axisSq.x + (axisSq.y + axisSq.z) * cosAngle) / lengthSq;
			rotationMatrix[0][1] = (axis.x * axis.y * (1 - cosAngle) - axis.z * length * sinAngle) / lengthSq;
			rotationMatrix[0][2] = (axis.x * axis.z * (1 - cosAngle) + axis.y * length * sinAngle) / lengthSq;
			rotationMatrix[0][3] = 0.0f;

			rotationMatrix[1][0] = (axis.x * axis.y * (1 - cosAngle) + axis.z * length * sinAngle) / lengthSq;
			rotationMatrix[1][1] = (axisSq.y + (axisSq.x + axisSq.z) * cosAngle) / lengthSq;
			rotationMatrix[1][2] = (axis.y * axis.z * (1 - cosAngle) - axis.x * length * sinAngle) / lengthSq;
			rotationMatrix[1][3] = 0.0;

			rotationMatrix[2][0] = (axis.x * axis.z * (1 - cosAngle) - axis.y * length * sinAngle) / lengthSq;
			rotationMatrix[2][1] = (axis.y * axis.z * (1 - cosAngle) + axis.x * length * sinAngle) / lengthSq;
			rotationMatrix[2][2] = (axisSq.z + (axisSq.x + axisSq.y) * cosAngle) / lengthSq;
			rotationMatrix[2][3] = 0.0;

			rotationMatrix[3][0] = 0.0f;
			rotationMatrix[3][1] = 0.0f;
			rotationMatrix[3][2] = 0.0f;
			rotationMatrix[3][3] = 1.0f;
#else
			Normalize(axis);

			angleDegree *= DEGREETORAD;
			float cosAngle = cosf(angleDegree);
			float sinAngle = sinf(angleDegree);

			DirectX::XMFLOAT3 axisSq = axis;
			axisSq.x *= axisSq.x;
			axisSq.y *= axisSq.y;
			axisSq.z *= axisSq.z;

			rotationMatrix[0][0] = (axisSq.x + (axisSq.y + axisSq.z) * cosAngle);
			rotationMatrix[0][1] = (axis.x * axis.y * (1 - cosAngle) - axis.z * sinAngle);
			rotationMatrix[0][2] = (axis.x * axis.z * (1 - cosAngle) + axis.y * sinAngle);
			rotationMatrix[0][3] = 0.0f;

			rotationMatrix[1][0] = (axis.x * axis.y * (1 - cosAngle) + axis.z * sinAngle);
			rotationMatrix[1][1] = (axisSq.y + (axisSq.x + axisSq.z) * cosAngle);
			rotationMatrix[1][2] = (axis.y * axis.z * (1 - cosAngle) - axis.x * sinAngle);
			rotationMatrix[1][3] = 0.0;

			rotationMatrix[2][0] = (axis.x * axis.z * (1 - cosAngle) - axis.y * sinAngle);
			rotationMatrix[2][1] = (axis.y * axis.z * (1 - cosAngle) + axis.x * sinAngle);
			rotationMatrix[2][2] = (axisSq.z + (axisSq.x + axisSq.y) * cosAngle);
			rotationMatrix[2][3] = 0.0;

			rotationMatrix[3][0] = 0.0f;
			rotationMatrix[3][1] = 0.0f;
			rotationMatrix[3][2] = 0.0f;
			rotationMatrix[3][3] = 1.0f;
#endif


			/*			Multiply matrices			*/
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 1; j++) {
					outputMatrix[i][j] = 0;
					for (int k = 0; k < 4; k++) {
						outputMatrix[i][j] += rotationMatrix[i][k] * inputMatrix[k][j];
					}
				}
			}

			DirectX::XMFLOAT3 result;
			result.x = outputMatrix[0][0];
			result.y = outputMatrix[1][0];
			result.z = outputMatrix[2][0];

			return result;
		}

		inline DirectX::XMFLOAT3 RotatePointAroundArbitaryAxis(float angleDegree, DirectX::XMFLOAT3 point, DirectX::XMVECTOR axis)
		{
			DirectX::XMFLOAT3 axisFloat = DirectX::XMFLOAT3(DirectX::XMVectorGetX(axis), DirectX::XMVectorGetY(axis), DirectX::XMVectorGetZ(axis));
			return RotatePointAroundArbitaryAxis(angleDegree, point, axisFloat);
		}

		inline float Dot2D(DirectX::XMFLOAT2 a, DirectX::XMFLOAT2 b)
		{
			//a * x
			a.x *= b.x;
			a.y *= b.y;

			a.x += a.y;

			return a.x;
		}

		inline Vec3 CrossProduct(Vec3 a, Vec3 b)
		{
			Vec3 result;
			result.x = a.y * b.z - a.z * b.y;
			result.y = a.z * b.x - a.x * b.z;
			result.z = a.x * b.y - a.y * b.x;

			return result;
		}

		inline static float Dot(Vec3 v1, Vec3 v2)
		{
			v1.x *= v2.x;
			v1.y *= v2.y;
			v1.z *= v2.z;

			v1.x += v1.y;
			v1.x += v1.z;

			return v1.x;
		}

		inline Vec3 Reflect(Vec3 direction, Vec3 normal)
		{
			//v = v + (2* Dot(v,n)) * n;
			float dot = Dot(direction, normal);
			dot = fabsf(dot);
			dot *= 2.0f;

			normal *= dot;

			direction += normal;

			return direction;
		}

		//https://gamedev.stackexchange.com/questions/28395/rotating-vector3-by-a-quaternion
		/*	Quaternion: x,y,z -> complex part;  w = scalar/real part
		*/
		inline Vec3 RotateWithQuaternion(Vec3 vector, Vec4 quaternion)
		{
			Vec3 quatVector = Vec3(quaternion.x, quaternion.y, quaternion.z);
			float quatScalar = quaternion.w;

			Vec3 result;
			result = quatVector * Dot(quatVector, vector) * 2.0f;
			result += (vector * (quatScalar * quatScalar - Dot(quatVector, quatVector)));
			result += CrossProduct(quatVector, vector) * quatScalar * 2.0f;

			return result;
		}

		inline Vec4 InvertQuaternion(Vec4 quat)
		{
			quat.x *= -1.0f;
			quat.y *= -1.0f;
			quat.z *= -1.0f;

			return quat;
		}

		inline Vec4 QuaternionFromDirectXVector(DirectX::XMVECTOR vector)
		{
			Vec4 result;
			result.x = vector.m128_f32[0];
			result.y = vector.m128_f32[1];
			result.z = vector.m128_f32[2];
			result.w = vector.m128_f32[3];

			return result;
		}
		inline DirectX::XMVECTOR DirectXVectorFromQuaternion(Vec4 quaternion)
		{
			DirectX::XMVECTOR result;
			result.m128_f32[0] = quaternion.x;
			result.m128_f32[1] = quaternion.y;
			result.m128_f32[2] = quaternion.z;
			result.m128_f32[3] = quaternion.w;

			return result;
		}

		inline Vec4 QuaternionMultiply(Vec4 quaternion1, Vec4 quaternion2)
		{
			quaternion1.x *= quaternion2.x;
			quaternion1.y *= quaternion2.y;
			quaternion1.z *= quaternion2.z;
			quaternion1.w *= quaternion2.w;

			return quaternion1;
		}
		inline Vec4 QuaternionAdd(Vec4 quaternion1, Vec4 quaternion2)
		{
			quaternion1.x += quaternion2.x;
			quaternion1.y += quaternion2.y;
			quaternion1.z += quaternion2.z;
			quaternion1.w += quaternion2.w;

			return quaternion1;
		}

		/* Creates a CoordinateSystem with the given x-axis and a suggested y axis (which might change)
		*/
		inline bool MakeOrthonormalBasis(Vec3 x, Vec3* y, Vec3* z)
		{
			//NOTE: Check GamePhysicsEngineDevelopment p309 for some optimization regarding normalization

			//Calculate the z-axis with the CrossProduct of x and y
			(*z) = CrossProduct(x, (*y));

			if (MATH::LengthSq(*z) <= 0.001f) return false;

			//Recalculate the y-axis
			(*y) = CrossProduct(x, (*z));

			//Normalize axes
			Normalize(y);
			Normalize(z);

			return true;
		}

		inline void SafetyRound(Vec3& vector)
		{
			const float value = 0.00000001f;
			
			if (fabsf(vector.x) < value)
			{
				vector.x = 0.0f;
			}
			if (fabsf(vector.y) < value)
			{
				vector.y = 0.0f;
			}
			if (fabsf(vector.z) < value)
			{
				vector.z = 0.0f;
			}
		}
	}
}