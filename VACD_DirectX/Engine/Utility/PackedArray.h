#pragma once
#include <assert.h>
#include <stdint.h>
#include <limits>

// The implementation makes sure that after *each* operation its internal data structure has no holes,
// and contains Item instances which are stored contiguously in memory.

namespace ICE
{
	// our ItemID has 32 bits. you can use those bits for whatever purpose you see fit.
	// this is the only thing our user sees.
	typedef uint32_t ItemID;

	namespace
	{
		// you can assume that we never store more than 256 entries.
		static const uint32_t MAX_ID_COUNT = 256u;

		#undef max
		static ItemID INVALID_ITEM_ID = std::numeric_limits<ItemID>::max();

		// helper union to extract the single fiels from the ItemID
		union ItemIDSplit
		{
			struct data
			{
				uint16_t sparseIndex;
				uint16_t generation;
			} data;

			ItemID value;
		};

		static inline ItemID MakeItemID(uint16_t sparseIndex, uint16_t generation)
		{
			ItemIDSplit idSplit;
			idSplit.data.sparseIndex = sparseIndex;
			idSplit.data.generation = generation;

			return idSplit.value;
		}

		// entry in sparse array
		struct Mapping
		{
			uint8_t denseIndex;
			uint8_t sparseIndex;
			uint16_t generation;
		};

		static inline Mapping MakeMapping(uint8_t denseIndex, uint8_t sparseIndex, uint16_t generation)
		{
			Mapping mapping;

			mapping.denseIndex = denseIndex;
			mapping.sparseIndex = sparseIndex;
			mapping.generation = generation;

			return mapping;
		}
	}


	template<typename TypeToStore>
	class PackedArray
	{
	public:
		PackedArray(void)
			: m_freeListStart(0)
			, m_itemCount(0)
		{
			// Initialize the entries in the mapping so that they all have a generation of 0 and their
			// freelist pointer (saved in the denseIndex) points to the next element
			// The first element in the freelist is the element with index 0 (see m_freeListStart)
			for (uint16_t i = 0; i < MAX_ID_COUNT; ++i)
			{
				//That conversion to an uint8_t is wanted! Last element refers to the first element again!
				m_mappings[i] = MakeMapping((uint8_t)(i + 1), 0, 0);
			}
		}

		ItemID AddItem(void)
		{
			if (m_itemCount == MAX_ID_COUNT)
			{
				// We have reached the limit of items
				return INVALID_ITEM_ID;
			}

			// Get the first free mapping with the m_freeListStart member which points to the beginning of the free list
			Mapping& mapping = m_mappings[m_freeListStart];

			// Remember the pointer to the next free sparse array element
			uint8_t nextFreeListStart = mapping.denseIndex;

			// Increase the generation and let the denseIndex point to the item in the dense array
			++mapping.generation;
			mapping.denseIndex = static_cast<uint8_t>(m_itemCount);

			// Generate the key for the newly created entry
			ItemID newItemID = MakeItemID(m_freeListStart, mapping.generation);

			// Now update the mapping from the denseIndex to its sparseIndex
			m_mappings[m_itemCount].sparseIndex = m_freeListStart;

			// m_freeListStart must now point to the new beginning of the free list, which is the entry the denseIndex of the mapping pointed to
			m_freeListStart = nextFreeListStart;

			++m_itemCount;

			return newItemID;
		}

		ItemID AddItem(TypeToStore& item)
		{
			if (m_itemCount == MAX_ID_COUNT)
			{
				// We have reached the limit of items
				return INVALID_ITEM_ID;
			}

			// Get the first free mapping with the m_freeListStart member which points to the beginning of the free list
			Mapping& mapping = m_mappings[m_freeListStart];

			// Remember the pointer to the next free sparse array element
			uint8_t nextFreeListStart = mapping.denseIndex;

			// Increase the generation and let the denseIndex point to the item in the dense array
			++mapping.generation;
			mapping.denseIndex = m_itemCount;

			// Generate the key for the newly created entry
			ItemID newItemID = MakeItemID(m_freeListStart, mapping.generation);

			// Now update the mapping from the denseIndex to its sparseIndex
			m_mappings[m_itemCount].sparseIndex = m_freeListStart;

			// m_freeListStart must now point to the new beginning of the free list, which is the entry the denseIndex of the mapping pointed to
			m_freeListStart = nextFreeListStart;

			++m_itemCount;

			//Copy over the wanted item
			m_items[mapping.denseIndex] = item;

			return newItemID;
		}

		void RemoveItem(ItemID id)
		{
			// Split the item id into its components
			ItemIDSplit idSplit;
			idSplit.value = id;

			assert(idSplit.data.sparseIndex < MAX_ID_COUNT);

			Mapping& removedMapping = m_mappings[idSplit.data.sparseIndex];

			// Ensure ItemID is up to date
			assert(idSplit.data.generation == removedMapping.generation);

			// Remember the sparseIndex of the item to be removed
			uint8_t removedSparseIndex = static_cast<uint8_t>(idSplit.data.sparseIndex);

			const Mapping& lastMapping = m_mappings[m_itemCount - 1];

			Mapping& swapMapping = m_mappings[lastMapping.sparseIndex];

			// Copy the last entry over the element to be removed (memcpy is allowed here, because we only have to deal with PODs)
			//memcpy(m_items + removedMapping.denseIndex, m_items + swapMapping.denseIndex, sizeof(TypeToStore));
			(*(m_items + removedMapping.denseIndex)) = (*(m_items + swapMapping.denseIndex));

			// Update the mappings
			// The mapping of the last element which was swapped over the removed element needs to point to its new denseIndex
			swapMapping.denseIndex = removedMapping.denseIndex;

			// Update the generation of the removed item and let m_freeListStart point to the removed mapping
			++removedMapping.generation;
			removedMapping.denseIndex = m_freeListStart;
			m_freeListStart = removedSparseIndex;


			// Update the dense-sparse mapping for swapped element
			Mapping& denseSparseMapping = m_mappings[swapMapping.denseIndex];
			denseSparseMapping.sparseIndex = lastMapping.sparseIndex;

			--m_itemCount;
		}


		TypeToStore* Lookup(ItemID id)
		{
			// Split the item id into its components
			ItemIDSplit idSplit;
			idSplit.value = id;

			if (idSplit.data.sparseIndex >= MAX_ID_COUNT)
			{
				return nullptr;
			}

			const Mapping& mapping = m_mappings[idSplit.data.sparseIndex];

			if (idSplit.data.generation != mapping.generation)
			{
				// ItemID is not up to date (item was deleted)
				return nullptr;
			}

			return m_items + mapping.denseIndex;
		}

		inline const TypeToStore* GetItems()
		{
			return m_items;
		}
		inline uint32_t GetItemCount()
		{
			return m_itemCount;
		}

	private:
		Mapping m_mappings[MAX_ID_COUNT];
		uint8_t m_freeListStart;

		//Maybe needs a MemoryHolder?
		TypeToStore m_items[MAX_ID_COUNT];
		uint32_t m_itemCount;
	};

}