#pragma once
#include <stdint.h>

namespace PUTILITY
{
	template <typename Numeric>
	uintptr_t AddSize(const void* ptr, Numeric n)
	{
		uintptr_t value = reinterpret_cast<uintptr_t>(ptr) + static_cast<uintptr_t>(n);
		return value;
	};

	template <typename Numeric>
	uintptr_t AddSize(uintptr_t ptr, Numeric n)
	{
		ptr += static_cast<uintptr_t>(n);
		return ptr;
	};

	template <typename Numeric>
	uintptr_t SubstractSize(const void* ptr, Numeric n)
	{
		uintptr_t value = reinterpret_cast<uintptr_t>(ptr) - static_cast<uintptr_t>(n);
		return value;
	};

	template <typename Numeric>
	uintptr_t SubstractSize(uintptr_t ptr, Numeric n)
	{
		ptr -= static_cast<uintptr_t>(n);
		return ptr;
	};
}