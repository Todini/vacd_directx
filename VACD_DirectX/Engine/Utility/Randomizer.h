#pragma once
#include <random>

namespace ICE
{
	template<class T>
	static T GetRandom(T min, T max)
	{
		static std::random_device rd;
		static std::mt19937 randomGenerator = std::mt19937(rd());

		std::uniform_int_distribution<T> dist = std::uniform_int_distribution<T>(min, max);
		return dist(randomGenerator);
	}

	static float GetRandom(float min, float max)
	{
		static std::random_device rd;
		static std::mt19937 randomGenerator = std::mt19937(rd());

		std::uniform_real_distribution<float> dist = std::uniform_real_distribution<float>(min, max);
		return dist(randomGenerator);
	}
	static double GetRandom(double min, double max)
	{
		static std::random_device rd;
		static std::mt19937 randomGenerator = std::mt19937(rd());

		std::uniform_real_distribution<double> dist = std::uniform_real_distribution<double>(min, max);
		return dist(randomGenerator);
	}
}