#include "Raytrace.h"
#include "..\Utility\MathUtility.h"

namespace
{
	const float SEARCH_DISTANCE_MAX = 5.0f;
	const float SEARCH_STEP_LARGE = 0.2f;
	const float SEARCH_STEP_SMALL = 0.01f;
}

namespace ICE
{
	bool RayTraceCompounds(Transform start, std::vector<FRAC::Compound>& compounds, Vec3& outHitLocation, int& outCompoundIndex)
	{
		bool hasHit = false;

		outHitLocation = Vec3(NAN, NAN, NAN);

		Vec3 startPosition = start.GetPosition();
		Vec3 forward = MATH::AnglesToDirection(start.GetRotationAngles());
		MATH::Normalize(forward);

		float remainingSearch = SEARCH_DISTANCE_MAX;
		Vec3 forwardStep = forward * SEARCH_STEP_LARGE;
		Vec3 currentPosition = startPosition;

		while(remainingSearch > 0.0f)
		{
			size_t compoundCount = compounds.size();
			for(size_t i = 0u; i < compoundCount; i++)
			{
				FRAC::Compound& com = compounds[i];
				if (com.DoesPointIntersect(currentPosition))
				{
					hasHit = true;
					break;
				}
			}

			if (hasHit)
			{
				break;
			}

			currentPosition += forwardStep;
			remainingSearch -= SEARCH_STEP_LARGE;
		}

		//If something was hit -> make more precise step checks
#if 1
		if (hasHit)
		{
			hasHit = false;
			currentPosition -= forwardStep;
			forwardStep = forward * SEARCH_STEP_SMALL;
			currentPosition += forwardStep;
			remainingSearch = SEARCH_STEP_LARGE;

			while (remainingSearch > 0.0f)
			{
				size_t compoundCount = compounds.size();
				for (size_t i = 0u; i < compoundCount; i++)
				{
					FRAC::Compound& com = compounds[i];
					if (com.DoesPointIntersect(currentPosition))
					{
						hasHit = true;

						outCompoundIndex = static_cast<int>(i);
						outHitLocation = forwardStep * -0.5f;
						outHitLocation += currentPosition;

						return hasHit;
					}
				}

				if (hasHit) break;

				currentPosition += forwardStep;
				remainingSearch -= SEARCH_STEP_SMALL;
			}
		}
#endif

		return hasHit;
	}
}