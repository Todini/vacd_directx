#pragma once
#include "..\Components\Transform.h"
#include "..\VACD\Compound.h"

namespace ICE
{
	bool RayTraceCompounds(Transform start, std::vector<FRAC::Compound>& compounds, Vec3& outHitLocation, int& outCompoundIndex);
}