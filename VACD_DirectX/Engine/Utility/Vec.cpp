#include "Vec.h"

namespace ICE
{
	Vec3& Vec3::operator+=(const Vec3& b)
	{
		this->x += b.x;
		this->y += b.y;
		this->z += b.z;
		return *this;
	}
	Vec3 Vec3::operator+(const Vec3& b) const
	{
		Vec3 temp(*this);
		temp += b;
		return temp;
	}

	Vec3& Vec3::operator-=(const Vec3& b)
	{
		this->x -= b.x;
		this->y -= b.y;
		this->z -= b.z;
		return *this;
	}
	Vec3 Vec3::operator-(const Vec3& b) const
	{
		Vec3 temp(*this);
		temp -= b;
		return temp;
	}

	Vec3& Vec3::operator*=(float b)
	{
		this->x *= b;
		this->y *= b;
		this->z *= b;
		return *this;
	}
	Vec3 Vec3::operator*(float b) const
	{
		Vec3 temp(*this);
		temp *= b;
		return temp;
	}

	Vec3& Vec3::operator/=(float b)
	{
		this->x /= b;
		this->y /= b;
		this->z /= b;
		return *this;
	}
	Vec3 Vec3::operator/(float b) const
	{
		Vec3 temp(*this);
		temp /= b;
		return temp;
	}
}