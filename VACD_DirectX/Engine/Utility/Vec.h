#pragma once

namespace ICE
{
	struct Vec3
	{
		inline Vec3()
			: x(0.0f)
			, y(0.0f)
			, z(0.0f)
		{
		}
		inline Vec3(float x_, float y_, float z_)
			: x(x_)
			, y(y_)
			, z(z_)
		{
		}

		float x;
		float y;
		float z;

		Vec3& operator+=(const Vec3& b);
		Vec3 operator+(const Vec3& b) const;

		Vec3& operator-=(const Vec3& b);
		Vec3 operator-(const Vec3& b) const;

		Vec3& operator/=(float b);
		Vec3 operator/(float b) const;

		Vec3& operator*=(float b);
		Vec3 operator*(float b) const;

	};

	struct Vec4
	{
		inline Vec4()
			: x(0.0f)
			, y(0.0f)
			, z(0.0f)
			, w(0.0f)
		{
		}
		inline Vec4(float x_, float y_, float z_, float w_)
			: x(x_)
			, y(y_)
			, z(z_)
			, w(w_)
		{
		}
		inline Vec4(Vec3 vec3)
			: x(vec3.x)
			, y(vec3.y)
			, z(vec3.z)
			, w(0.0f)
		{
		}


		float x;
		float y;
		float z;
		float w;
	};	
}