#include "Component.h"

#undef min
#undef max
#include <OpenMesh\Core\IO\MeshIO.hh>
#include "..\Utility\MathUtility.h"

#include "Plane.h"

namespace
{
#define FRACTURE_TRIANGLE_MESH 0

	void ConvertToRenderMesh(ICE::FRAC::MeshPolygon& triangleMesh, ICE::Vec3 color,
		ICE::RENDER::Mesh<VTPosColNor>& outRenderMesh, ICE::PHYS::CollisionData& outCollisionData, ICE::Vec3& outCenter)
	{
		using namespace ICE;
		using namespace ICE::FRAC;
		triangleMesh.triangulate();

		uint32_t vertexCount = static_cast<uint32_t>(triangleMesh.n_vertices());
		uint32_t vertexCountAll = static_cast<uint32_t>(triangleMesh.n_faces() * 3u);
		uint32_t indexCount = static_cast<uint32_t>(triangleMesh.n_faces() * 3u);

		VTPosColNor* vertices = new VTPosColNor[vertexCountAll];
		uint32_t* indices = new uint32_t[indexCount];

		MeshPolygon::Normal normal;
		MeshPolygon::Point point;
		Vec3 vertexPos;
		Vec3 localMin = Vec3(100000.0f, 100000.0f, 100000.0f);
		Vec3 localMax = Vec3(-100000.0f, -100000.0f, -100000.0f);

		MeshPolygon::VertexHandle vHandle;

		for (MeshPolygon::VertexIter vIt = triangleMesh.vertices_begin(); vIt != triangleMesh.vertices_end(); vIt++)
		{
			vHandle = *vIt;

			//For CollisionData
			point = triangleMesh.point(vHandle);
			vertexPos = Vec3(point[0], point[1], point[2]);

			if (localMin.x > vertexPos.x) { localMin.x = vertexPos.x; }
			if (localMin.y > vertexPos.y) { localMin.y = vertexPos.y; }
			if (localMin.z > vertexPos.z) { localMin.z = vertexPos.z; }
			if (localMax.x < vertexPos.x) { localMax.x = vertexPos.x; }
			if (localMax.y < vertexPos.y) { localMax.y = vertexPos.y; }
			if (localMax.z < vertexPos.z) { localMax.z = vertexPos.z; }
		}

		//For sharp edges with shadows we need too duplicate some vertices!
		MeshPolygon::FaceHandle fHandle;
		VTPosColNor* currentVertex = vertices;
		uint32_t* currentIndex = indices;
		uint32_t index = 0u;
		for (MeshPolygon::FaceIter fIt = triangleMesh.faces_begin(); fIt != triangleMesh.faces_end(); fIt++)
		{
			fHandle = *fIt;
			for (MeshPolygon::FaceVertexIter fvIt = triangleMesh.fv_begin(fHandle); fvIt != triangleMesh.fv_end(fHandle); fvIt++, currentVertex++, currentIndex++, index++)
			{
				vHandle = *fvIt;
				//*currentIndex = vHandle.idx();
				*currentIndex = index;

				point = triangleMesh.point(vHandle);
				normal = triangleMesh.normal(fHandle);

				currentVertex->position = DirectX::XMFLOAT3(point[0], point[1], point[2]);
				currentVertex->color = DirectX::XMFLOAT4(color.x, color.y, color.z, 1.0f);
				currentVertex->normal = DirectX::XMFLOAT3(normal[0], normal[1], normal[2]);
			}
		}

		//vertexCurrent->color = DirectX::XMFLOAT4(color[0], color[1], color[2], color[3]);
		outRenderMesh = ICE::RENDER::Mesh<VTPosColNor>(vertices, vertexCountAll, indices, indexCount, sizeof(VTPosColNor), 0u);

		/*	Create collision data for this component
		1) Compound can create it's collision data for AABB and Sphere out of the components
		2) Needed for the fracturing process -> Criteria if a component is affected or not
		*/

		Vec3 center = localMax + localMin;
		center /= 2.0f;
		outCollisionData.sphere.center = center;
		outCollisionData.sphere.radius = MATH::Length(center - localMax);
		outCenter = center;

		outCollisionData.aabb.min = localMin;
		outCollisionData.aabb.max = localMax;
		outCollisionData.aabb.unrotatedMin = localMin;
		outCollisionData.aabb.unrotatedMax = localMax;
		//outCollisionData.convexHull = PHYS::CollisionConvexHull(vertices, vertexCount);

		delete[] vertices;
		delete[] indices;
	}

	void CopyPolyMesh(ICE::FRAC::MeshPolygon& m_source, ICE::FRAC::MeshPolygon& m_target)
	{
		using namespace ICE::FRAC;
		MeshPolygon::VertexHandle vHandle, vTargetHandle;

		for (MeshPolygon::VIter vIt = m_source.vertices_begin(); vIt !=  m_source.vertices_end(); vIt++)
		{
			vHandle = *vIt;
			vTargetHandle = m_target.add_vertex(m_source.point(vHandle));
			assert(vHandle.idx() == vTargetHandle.idx());

			m_target.set_normal(vHandle, m_source.normal(vHandle));
			m_target.set_color(vHandle, m_source.color(vHandle));
		}

		std::vector<MeshPolygon::VertexHandle> vertices = std::vector<MeshPolygon::VertexHandle>();
		MeshPolygon::FaceHandle fHandle, fTargetHandle;
		for (MeshPolygon::FaceIter fIt = m_source.faces_begin(); fIt != m_source.faces_begin(); fIt++)
		{
			fHandle = *fIt;
			vertices.clear();
			for (MeshPolygon::FaceVertexIter fvIt = m_source.fv_begin(fHandle); fvIt != m_source.fv_end(fHandle); fvIt++)
			{
				vHandle = *fvIt;
				vertices.push_back(vHandle);
			}

			fTargetHandle = m_target.add_face(vertices);
			m_target.set_normal(fTargetHandle, m_source.normal(fHandle));
		}

	}
}

namespace ICE
{
	namespace FRAC
	{
		Component::Component(std::string filename, std::string fileToLoad)
			: m_mesh(), m_localCenter()
			, m_meshRender()
			, m_colData()
			, m_filename(filename)
		{
			bool success = OpenMesh::IO::read_mesh(m_mesh, fileToLoad + ".obj");
			assert(success == true);

			m_mesh.update_normals();
#if FRACTURE_TRIANGLE_MESH
			m_mesh.triangulate();
			ConvertToRenderMesh(m_mesh, Vec3(0.8f, 0.05f, 0.05f), m_meshRender, m_colData, m_localCenter);
#else
			/* Convert the polygonal mesh to a triangulated one so it can be rendered in DirectX11 */

			MeshPolygon meshTriangulated = MeshPolygon(m_mesh);
			ConvertToRenderMesh(meshTriangulated, Vec3(0.8f, 0.05f, 0.05f), m_meshRender, m_colData, m_localCenter);
#endif
		}

		Component::Component(MeshPolygon mesh, Vec3 color)
			: m_mesh(mesh)
			, m_localCenter()
			, m_meshRender()
			, m_colData()
		{
#if FRACTURE_TRIANGLE_MESH
			ConvertToRenderMesh(m_mesh, color, m_meshRender, m_colData, m_localCenter);
#else
			/*
			static int counter = 0;
			SaveMeshToFile("CONSTRUCTOR" + std::to_string(counter) + ".obj");
			counter++;
			*/
			ConvertToRenderMesh(mesh, color, m_meshRender, m_colData, m_localCenter);
#endif
		}

		bool Component::DoesPointIntersect(Vec3 myPosition, Vec3 point)
		{
			if (PHYS::DoesIntersectPointSphere(point, myPosition, m_colData.sphere))
			{
				if (PHYS::DoesIntersectPointAABB(point, myPosition, m_colData.aabb))
				{
					return true;
				}
			}

			return false;
		}

		bool Component::IsInRange(Vec3 compoundLocation, Vec3 impactLocation, float radius)
		{
			//Component center = compoundLocation + m_colData.sphere.center
			compoundLocation += m_colData.sphere.center;
			//Both radii combined
			radius += m_colData.sphere.radius;

			impactLocation -= compoundLocation;
			float distance = MATH::Length(impactLocation);

			return distance < radius;
		}

		void Component::SaveMeshToFile(std::string filename)
		{
			OpenMesh::IO::write_mesh(m_mesh, filename + ".obj");
		}

		void Component::ScaleVertices(float scaling)
		{
			VTPosColNor* vertices = m_meshRender.GetVertices();
			uint32_t vertexCount = m_meshRender.GetVertexCount();

			Vec3 vertexCenter = Vec3();
			DirectX::XMFLOAT3 vertexPosition = DirectX::XMFLOAT3();
			VTPosColNor* currentVertex = vertices;
			for (uint32_t i = 0u; i < vertexCount; i++, currentVertex++)
			{
				vertexPosition = currentVertex->position;
				vertexCenter.x += vertexPosition.x;
				vertexCenter.y += vertexPosition.y;
				vertexCenter.z += vertexPosition.z;
			}
			vertexCenter /= static_cast<float>(vertexCount);

			currentVertex = vertices;
			for (uint32_t i = 0u; i < vertexCount; i++, currentVertex++)
			{
				vertexPosition = currentVertex->position;

				vertexPosition.x -= vertexCenter.x;
				vertexPosition.x *= scaling;
				vertexPosition.x += vertexCenter.x;
				vertexPosition.y -= vertexCenter.y;
				vertexPosition.y *= scaling;
				vertexPosition.y += vertexCenter.y;
				vertexPosition.z -= vertexCenter.z;
				vertexPosition.z *= scaling;
				vertexPosition.z += vertexCenter.z;

				currentVertex->position = vertexPosition;
			}

			m_meshRender.UpdateVertices();
		}
	}
}