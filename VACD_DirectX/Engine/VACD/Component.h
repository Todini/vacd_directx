#pragma once
#include "MeshPolygon.h"
#include "..\Components\Transform.h"
#include <string>

#include "..\Rendering\Mesh.h"
#include "..\Rendering\VertexTypes.h"
#include "..\Physics\CollisionData.h"

namespace ICE
{
	namespace FRAC
	{
		class Component
		{
		public:
			Component(std::string filename, std::string fileToLoad);
			Component(MeshPolygon mesh, Vec3 color);

			inline MeshPolygon& GetPolyMesh() { return m_mesh; }
			inline const RENDER::Mesh<VTPosColNor>* GetRenderedMesh() const { return &m_meshRender; }
			inline const PHYS::CollisionData& GetCollisionData() const { return m_colData; }
			inline std::string GetFilename() { return m_filename; }
			inline void SetFilename(std::string filename) { m_filename = filename; }

			bool DoesPointIntersect(Vec3 myPosition, Vec3 point);
			bool IsInRange(Vec3 compoundLocation, Vec3 impactLocation, float radius);

			void SaveMeshToFile(std::string filename);
			void ScaleVertices(float scaling);

			MeshPolygon m_mesh;
		private:
			//local position of the mesh
			Vec3 m_localCenter;
			RENDER::Mesh<VTPosColNor> m_meshRender;

			PHYS::CollisionData m_colData;
			std::string m_filename;
		};
	}
}