#include "Compound.h"
#include "..\Utility\MathUtility.h"

namespace
{
	const std::string FOLDER_LOCATION = "Meshes\\Destructible\\";

	void CreateCollisionData(std::vector<ICE::FRAC::Component>& components, ICE::PHYS::CollisionData& outCollisionData)
	{
		using namespace ICE;
		using namespace ICE::FRAC;

		//Get collision data
		Vec3 localMin = Vec3(100000.0f, 100000.0f, 100000.0f);
		Vec3 localMax = Vec3(-100000.0f, -100000.0f, -100000.0f);

		for (Component& com : components)
		{
			const PHYS::CollisionData colData = com.GetCollisionData();

			if (localMin.x > colData.aabb.min.x) { localMin.x = colData.aabb.min.x; }
			if (localMin.y > colData.aabb.min.y) { localMin.y = colData.aabb.min.y; }
			if (localMin.z > colData.aabb.min.z) { localMin.z = colData.aabb.min.z; }
			if (localMax.x < colData.aabb.min.x) { localMax.x = colData.aabb.min.x; }
			if (localMax.y < colData.aabb.min.y) { localMax.y = colData.aabb.min.y; }
			if (localMax.z < colData.aabb.min.z) { localMax.z = colData.aabb.min.z; }

			if (localMin.x > colData.aabb.max.x) { localMin.x = colData.aabb.max.x; }
			if (localMin.y > colData.aabb.max.y) { localMin.y = colData.aabb.max.y; }
			if (localMin.z > colData.aabb.max.z) { localMin.z = colData.aabb.max.z; }
			if (localMax.x < colData.aabb.max.x) { localMax.x = colData.aabb.max.x; }
			if (localMax.y < colData.aabb.max.y) { localMax.y = colData.aabb.max.y; }
			if (localMax.z < colData.aabb.max.z) { localMax.z = colData.aabb.max.z; }
		}

		outCollisionData.aabb.min = localMin;
		outCollisionData.aabb.max = localMax;
		outCollisionData.aabb.unrotatedMin = localMin;
		outCollisionData.aabb.unrotatedMax = localMax;

		Vec3 center = localMax + localMin;
		center /= 2.0f;
		outCollisionData.sphere.center = center;
		outCollisionData.sphere.radius = MATH::Length(center - localMax);
	}
}

namespace ICE
{
	namespace FRAC
	{
		Compound::Compound(std::string filename, size_t componentCount, Vec3 position)
			: m_transform(position.x, position.y, position.z)
			, m_components()
			, m_colData()
			, m_filename(filename)
		{
			m_components.reserve(componentCount * 2u);
			filename = FOLDER_LOCATION + filename;

			if (componentCount == 0u)
			{
				m_components.push_back(Component(m_filename, filename));
			}
			else
			{
				for (size_t i = 0u; i < componentCount; i++)
				{
					m_components.push_back(Component(m_filename, filename + std::to_string(i)));
				}
			}
			CreateCollisionData(m_components, m_colData);
		}

		Compound::Compound(Component& component, Vec3 position)
			: m_transform(position.x, position.y, position.z)
			, m_components()
			, m_colData()
			, m_filename(component.GetFilename())
		{
			m_components.push_back(component);

			CreateCollisionData(m_components, m_colData);
		}

		bool Compound::DoesPointIntersect(Vec3 point)
		{
			Vec3 myPos = m_transform.GetPosition();
			if (PHYS::DoesIntersectPointSphere(point, myPos, m_colData.sphere))
			{
				if (PHYS::DoesIntersectPointAABB(point, myPos, m_colData.aabb))
				{
					return true;
				}
			}
			return false;
		}

		std::vector<Component> Compound::ComponentsInFractureRange(Vec3 impactLocation, float impactRadius)
		{
			std::vector<Component> result = std::vector<Component>();
			result.reserve(m_components.size());

			Vec3 compoundLocation = m_transform.GetPosition();
			int32_t componentSize = static_cast<int32_t>(m_components.size());
			for (int32_t i = 0; i < componentSize; i++)
			{
				if (m_components[i].IsInRange(compoundLocation, impactLocation, impactRadius))
				{
					result.push_back(m_components[i]);
					m_components.erase(m_components.begin() + i);
					i--;
					componentSize--;
				}
			}

			return result;
		}

		void Compound::AddComponent(Component& component)
		{
			m_components.push_back(component);
		}

		void Compound::SaveMeshToFile(std::string filename)
		{
			size_t componentsCounts = m_components.size();

			for (size_t i = 0u; i < componentsCounts; i++)
			{
				m_components[i].SaveMeshToFile(filename + std::to_string(i));
			}
		}

		void Compound::UpdateCollisionData()
		{
			CreateCollisionData(m_components, m_colData);
		}

		void Compound::ScaleComponent(float scaling)
		{
			for (Component& comp : m_components)
			{
				comp.ScaleVertices(scaling);
			}
		}
	}
}