#pragma once
#include "Component.h"
#include <vector>

namespace ICE
{
	namespace FRAC
	{
		class Compound
		{
		public:
			Compound(std::string filename, size_t componentCount, Vec3 position);
			Compound(Component& component, Vec3 position);

			inline Transform GetTransform() { return m_transform; }
			inline Component* GetComponents() { return m_components.data(); }
			inline size_t GetComponentCount() { return m_components.size(); }
			inline const PHYS::CollisionData& GetCollisionData() const { return m_colData; }
			inline bool IsEmpty() { return (m_components.size() == 0u); }
			inline bool HasSingleComponent() { return (m_components.size() == 1u); }
			inline std::string GetFilename() { return m_filename; }

			bool DoesPointIntersect(Vec3 point);

			/*	Returns all components in range AND removes them from this component!
			*/
			std::vector<Component> ComponentsInFractureRange(Vec3 impactLocation, float impactRadius);

			void AddComponent(Component& component);

			void SaveMeshToFile(std::string filename);
			void UpdateCollisionData();
			void ScaleComponent(float scaling);

		private:
			Transform m_transform;
			std::vector<Component> m_components;
			PHYS::CollisionData m_colData;
			std::string m_filename;
		};
	}
}