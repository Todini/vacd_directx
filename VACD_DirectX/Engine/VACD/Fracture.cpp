#include "Fracture.h"
#include "..\TP\voro++\voro++.hh"
#include "VoronoiFace.h"

#include <unordered_map>
#include <unordered_set>
#include <stack>
#include <algorithm>
#include "..\Utility\Randomizer.h"

#include "..\Timer.h"
#include "..\File\SimpleFileWriter.h"

#undef max
#undef min
#include <limits>

namespace std
{
	template <>
	struct hash<ICE::FRAC::Vector3>
	{
		size_t operator()(const ICE::FRAC::Vector3& k) const
		{
			return static_cast<size_t>(1u);
			//return static_cast<size_t>(k.x + k.y + k.z);
		}
	};
}

namespace
{
	struct EdgePoint
	{
		int faceHandleIdx;
		uint32_t voroFaceIndex;
		uint32_t clipIndex;
	};

	struct
	{
		bool operator() (ICE::FRAC::MeshPolygon::VertexHandle& a, ICE::FRAC::MeshPolygon::VertexHandle& b)
		{
			return a.idx() < b.idx();
		}
	} vertexHandleCompare;

	template<typename T>
	bool isBetweenOrSame(T value, T limit0, T limit1)
	{
		//fabs(x - rhs.x) < EPSILON
		//if (value >= limit0 && value <= limit1) return true;
		//if (value <= limit0 && value >= limit1) return true;

		if (value >= limit0 - EPSILON && value <= limit1 + EPSILON) return true;
		if (value <= limit0 + EPSILON && value >= limit1 - EPSILON) return true;

		return false;
	}

	struct CastToPRECISION
	{
		PRECISION operator()(double value) const { return static_cast<PRECISION>(value); }
	};


	std::vector<ICE::FRAC::VoronoiFace> GetVoronoiCellFaces(voro::voronoicell_neighbor& cell, ICE::FRAC::Vector3 offset)
	{
		using namespace ICE::FRAC;

		int faceCount = cell.number_of_faces();
		std::vector<VoronoiFace> voronoiFaces = std::vector<VoronoiFace>();
		voronoiFaces.reserve(faceCount);

		std::vector<int> faceIndices = std::vector<int>();
		cell.face_vertices(faceIndices);
#if FRACTURE_USE_FLOAT
		std::vector<double> verticesDouble = std::vector<double>();
		cell.vertices(static_cast<double>(offset.x), static_cast<double>(offset.y), static_cast<double>(offset.z), verticesDouble);
		std::vector<PRECISION> vertices = std::vector<PRECISION>();
		vertices.reserve(verticesDouble.size());
		std::transform(verticesDouble.begin(), verticesDouble.end(), std::back_inserter(vertices), CastToPRECISION());

		std::vector<double> faceNormalsDouble = std::vector<double>();
		cell.normals(faceNormalsDouble);
		std::vector<PRECISION> faceNormals = std::vector<PRECISION>();
		faceNormals.reserve(faceNormalsDouble.size());
		std::transform(faceNormalsDouble.begin(), faceNormalsDouble.end(), std::back_inserter(faceNormals), CastToPRECISION());
#else
		std::vector<double> vertices = std::vector<double>();
		cell.vertices(static_cast<double>(offset.x), static_cast<double>(offset.y), static_cast<double>(offset.z), vertices);
		std::vector<double> faceNormals = std::vector<double>();
		cell.normals(faceNormals);

#endif
		//voro++ saves the indices as follows: faceOrder, index, ..., index, faceOrder, index, ...
		size_t faceIndicesSize = faceIndices.size();
		for (size_t i = 0u; i < faceIndicesSize; )
		{
			int faceOrder = faceIndices[i++];
			VoronoiFace vFace = VoronoiFace();
			vFace.vertices = std::vector<Vector3>();
			vFace.vertices.reserve(faceOrder);
			vFace.clippingVertices = std::vector<ClipVertex>();
			vFace.clippingVertices.reserve(8u);

			Vector3 vertex;

			for (size_t faceIndex = 0u; faceIndex < faceOrder; faceIndex++, i++)
			{
				size_t vertexIndex = faceIndices[i] * 3u;
				vertex.x = vertices[vertexIndex];
				vertex.y = vertices[vertexIndex + 1];
				vertex.z = vertices[vertexIndex + 2];
				vFace.vertices.push_back(vertex);
			}

			vFace.clippedFaceInsideVertices = std::vector<Vector3>();
			vFace.clippedFaceInsideVertices.reserve(2u);
			vFace.clippedFaceInsideVertexHandles = std::vector<MeshPolygon::VertexHandle>();
			vFace.clippedFaceInsideVertexHandles.reserve(2u);
			vFace.clippedFaceOutsideVertices = std::vector<Vector3>();
			vFace.clippedFaceOutsideVertices.reserve(2u);
			voronoiFaces.push_back(vFace);
		}
		//Save the normals as well
		for (size_t faceIndex = 0u, faceNormalIndex = 0u; faceIndex < faceCount; faceIndex++)
		{
			Vector3 normal = Vector3();
			normal.x = faceNormals[faceNormalIndex++];
			normal.y = faceNormals[faceNormalIndex++];
			normal.z = faceNormals[faceNormalIndex++];

			voronoiFaces[faceIndex].normal = normal.normalize();
			voronoiFaces[faceIndex].plane = Plane(normal, voronoiFaces[faceIndex].vertices[0]);
		}

		return voronoiFaces;
	}


	/*	Checks if a point is between the vertices that make up this face.
	On itself this only makes sure if the point is inside and in any direction along the normal vector! */
	bool isPointBetweenFaceVertices(const std::vector<ICE::FRAC::Vector3>& vertices, ICE::FRAC::Vector3 normal, ICE::FRAC::Vector3 point)
	{
		using namespace ICE::FRAC;

		size_t vertexCount = vertices.size();
		if (vertexCount < 2u)
		{
			return false;
		}

		int rightCounter = 0;
		int leftCounter = 0;

		Vector3 direction;
		Vector3 testDirection;
		PRECISION determinant;
		for (size_t i = 0u; i < vertexCount; i++)
		{
			size_t otherEdge = i + 1u;
			if (otherEdge == vertexCount) otherEdge = 0;

			direction = vertices[otherEdge] - vertices[i];
			direction = direction.normalize();

			testDirection = point - vertices[i];
			testDirection = testDirection.normalize();
			testDirection = testDirection.cross(direction);
			determinant = testDirection.dot(normal);

			if (determinant < -EPSILON)
			{
				rightCounter++;
			}
			else if (determinant > EPSILON)
			{
				leftCounter++;
			}
		}

		//If the vertex is always on one side then the vertex lies somewhere in this face
		if ((rightCounter > 0 && leftCounter == 0) ||
			(leftCounter > 0 && rightCounter == 0))
		{
			return true;
		}

		return false;
	}

	bool CheckLineFaceIntersection(const ICE::FRAC::Plane& plane, ICE::FRAC::Vector3 planeNormal, const std::vector<ICE::FRAC::Vector3>& planeFaceVertices, ICE::FRAC::Vector3 linePoint0, ICE::FRAC::Vector3 linePoint1, ICE::FRAC::Vector3& outIntersectionPoint)
	{
		using namespace ICE::FRAC;

		outIntersectionPoint = Vector3(NAN, NAN, NAN);
		Line line = Line(linePoint0, linePoint1);

		Vector3 intersectionPoint = plane.intersect(line);
		/*	Check if the line is not parallel to the plane and if the intersection-point is on the line-segment that we need
		We need to check every axis because the line can be along one axis
		*/
		if (intersectionPoint.x != NAN &&
			isBetweenOrSame(intersectionPoint.x, linePoint0.x, linePoint1.x) &&
			isBetweenOrSame(intersectionPoint.y, linePoint0.y, linePoint1.y) &&
			isBetweenOrSame(intersectionPoint.z, linePoint0.z, linePoint1.z))
		{
			//Since this is a edge/plane test this can still give false-positives by generating intersections-points outside of this cell
			if (!isPointBetweenFaceVertices(planeFaceVertices, planeNormal, intersectionPoint))
			{
				return false;
			}

			outIntersectionPoint = intersectionPoint;
			return true;
		}

		return false;
	}

	void SortVerticesOnFaceCCW(std::vector<ICE::FRAC::Vector3>& vertices, std::vector<ICE::FRAC::MeshPolygon::VertexHandle>& vertexHandles, ICE::FRAC::Vector3 faceNormal)
	{
		using namespace ICE::FRAC;
#if 1
		size_t vertexCount = vertices.size();
		Vector3 origin = vertices[0];
		int freakCounter = vertexCount * 10;

		for (size_t i = 1u; i < vertexCount; i++)
		{
			if (freakCounter <= 0) break;

			Vector3 direction = vertices[i] - origin;
			PRECISION dirSq = direction.lengthSq();
			direction.normalize();
			for (size_t j = i + 1u; j < vertexCount; j++)
			{
				Vector3 testDirection = vertices[j] - origin;
				PRECISION testDirSq = testDirection.lengthSq();
				testDirection.normalize();

				if (direction.dot(testDirection) == 1.0)
				{

					if (dirSq > testDirSq)
					{
						//Remove the other point because it lies on the current line-segment
						vertices[j] = vertices[vertexCount - 1];
						vertices.pop_back();

						vertexHandles[j] = vertexHandles[vertexCount - 1];
						vertexHandles.pop_back();
						vertexCount--;
						j--;
						freakCounter--;
						//Since only the current element needed to be removed, we can continue from this point
					}
					else
					{
						//Remove this point because the other point is an extension of the line-segment
						vertices[i] = vertices[vertexCount - 1];
						vertices.pop_back();

						vertexHandles[i] = vertexHandles[vertexCount - 1];
						vertexHandles.pop_back();
						vertexCount--;
						i--;
						freakCounter--;
						break;
					}
				}

				Vector3 crossproduct = direction.cross(testDirection);
				crossproduct.normalize();
				PRECISION determinant = faceNormal.dot(crossproduct);

				if (determinant < 0.0)
				{
					//Point lies to the right -> we need to fix the ordering
					Vector3 temp = vertices[i];
					vertices[i] = vertices[j];
					vertices[j] = temp;

					MeshPolygon::VertexHandle tempVH = vertexHandles[i];
					vertexHandles[i] = vertexHandles[j];
					vertexHandles[j] = tempVH;
					i--;
					freakCounter--;
					break;
				}

			}
		}

#else
		size_t vertexCount = vertices.size();
		Vector3 origin = vertices[0];

		for (size_t i = 1u; i < vertexCount; i++)
		{
			Vector3 direction = vertices[i] - origin;
			PRECISION dirSq = direction.lengthSq();
			direction.normalize();
			for (size_t j = i + 1u; j < vertexCount; j++)
			{
				Vector3 testDirection = vertices[j] - origin;
				PRECISION testDirSq = testDirection.lengthSq();
				testDirection.normalize();

				if (direction.dot(testDirection) == 1.0)
				{

					if (dirSq > testDirSq)
					{
						//Remove the other point because it lies on the current line-segment
						vertices[j] = vertices[vertexCount - 1];
						vertices.pop_back();

						vertexHandles[j] = vertexHandles[vertexCount - 1];
						vertexHandles.pop_back();
						vertexCount--;
						j--;
						//Since only the current element needed to be removed, we can continue from this point
					}
					else
					{
						//Remove this point because the other point is an extension of the line-segment
						vertices[i] = vertices[vertexCount - 1];
						vertices.pop_back();

						vertexHandles[i] = vertexHandles[vertexCount - 1];
						vertexHandles.pop_back();
						vertexCount--;
						i--;
						break;
					}
				}

				Vector3 crossproduct = direction.cross(testDirection);
				crossproduct.normalize();
				PRECISION determinant = faceNormal.dot(crossproduct);

				if (determinant < 0.0)
				{
					//Point lies to the right -> we need to fix the ordering
					Vector3 temp = vertices[i];
					vertices[i] = vertices[j];
					vertices[j] = temp;

					MeshPolygon::VertexHandle tempVH = vertexHandles[i];
					vertexHandles[i] = vertexHandles[j];
					vertexHandles[j] = tempVH;
					i--;
					break;
				}

			}
		}
#endif

	}

	ICE::FRAC::MeshPolygon::FaceHandle AddFaceToMesh(ICE::FRAC::MeshPolygon& mesh, std::vector<ICE::FRAC::MeshPolygon::VertexHandle>& vertexHandles, ICE::FRAC::Vector3 normal)
	{
		using namespace ICE::FRAC;

		size_t vertexCount = vertexHandles.size();

		std::vector<Vector3> vertices = std::vector<Vector3>();
		vertices.reserve(vertexCount);

		for (size_t i = 0u; i < vertexCount; i++)
		{
			vertices.push_back(Vector3(mesh.point(vertexHandles[i])));
		}

		SortVerticesOnFaceCCW(vertices, vertexHandles, normal);
		

		MeshPolygon::FaceHandle faceHandle = mesh.add_face(vertexHandles.data(), vertexHandles.size());
		if (faceHandle.idx() == -1)
		{
			//if it is still a complex edge -> we just added them in reverse order
			std::reverse(vertexHandles.begin(), vertexHandles.end());
			faceHandle = mesh.add_face(vertexHandles.data(), vertexHandles.size());
		}

		if (faceHandle.idx() != -1)
		{
			MeshPolygon::Normal faceNormal = MeshPolygon::Normal(normal.x, normal.y, normal.z);
			mesh.set_normal(faceHandle, faceNormal);
		}

		return faceHandle;
	}

	bool CreateClipFace(uint32_t startIndex, uint32_t previousIndex, uint32_t currentIndex, std::vector<ICE::FRAC::ClipVertex>& clipVertices, std::vector<uint32_t>& currentFace)
	{
		using namespace ICE::FRAC;

		if (startIndex == currentIndex) return true;

		if (currentFace.size() > clipVertices.size())
		{
			return false;
		}

		currentFace.push_back(currentIndex);

		ClipVertex& clipVertex = clipVertices[currentIndex];
		size_t edgeCount = clipVertex.edgeIndices.size();
		bool correctPath = true;

		//Check if 

		for (size_t e = 0u; e < edgeCount; e++)
		{
			if (clipVertex.edgeIndices[e] != previousIndex)
			{
				correctPath = CreateClipFace(startIndex, currentIndex, clipVertex.edgeIndices[e], clipVertices, currentFace);
				if (correctPath) break;
			}
		}

		if (correctPath == false)
		{
			currentFace.pop_back();
			return false;
		}

		//clipVertex.wasVisited = true;
		return true;
	}

	

	ICE::FRAC::ClipVertex& AddPointToComponent(ICE::FRAC::Vector3 intersectionPoint, ICE::FRAC::Vector3 linePoint0, ICE::FRAC::Vector3 linePoint1,
		ICE::FRAC::MeshPolygon::VertexHandle vertexInterpolation0, ICE::FRAC::MeshPolygon::VertexHandle vertexInterpolation1,
		std::unordered_map<ICE::FRAC::Vector3, ICE::FRAC::MeshPolygon::VertexHandle>& uniqueVertices, ICE::FRAC::MeshPolygon& meshOriginal, ICE::FRAC::MeshPolygon& component, std::vector<ICE::FRAC::ClipVertex>& clippingVertices)
	{
		using namespace ICE::FRAC;

		std::unordered_map<Vector3, MeshPolygon::VertexHandle>::iterator doesVertexAlreadyExist = uniqueVertices.find(intersectionPoint);
		MeshPolygon::VertexHandle vertexHandle;
		if (doesVertexAlreadyExist == uniqueVertices.end())
		{
			//This is a new intersection-point for this cell!
			MeshPolygon::Point vertexPoint = MeshPolygon::Point(intersectionPoint.x, intersectionPoint.y, intersectionPoint.z);
			vertexHandle = component.add_vertex(vertexPoint);

			//Now we need to calculate the ratio to interpolate the normal and texture-coordinates.
			//Care needs to be taken if a line is along one axis, as the other axis's calculations would result in a division by 0
			PRECISION ratio = intersectionPoint.x - linePoint0.x;
			if (ratio != 0.0)
			{
				PRECISION help = linePoint1.x - linePoint0.x;
				ratio /= help;
			}
			else
			{
				ratio = intersectionPoint.y - linePoint0.y;
				if (ratio != 0.0)
				{
					PRECISION help = linePoint1.y - linePoint0.y;
					ratio /= help;
				}
				else
				{
					ratio = intersectionPoint.z - linePoint0.z;
					PRECISION help = linePoint1.z - linePoint0.z;
					ratio /= help;
				}
			}

			if (vertexInterpolation0.idx() != -1 && vertexInterpolation1.idx() != -1)
			{
				MeshPolygon::Point normal = meshOriginal.normal(vertexInterpolation1) - meshOriginal.normal(vertexInterpolation0);
				normal *= ratio;
				component.set_normal(vertexHandle, normal);
				MeshPolygon::Color color = meshOriginal.color(vertexInterpolation1) - meshOriginal.color(vertexInterpolation0);
				color *= ratio;
				component.set_color(vertexHandle, color);
			}

			std::pair<Vector3, MeshPolygon::VertexHandle> pair = std::pair<Vector3, MeshPolygon::VertexHandle>(intersectionPoint, vertexHandle);
			uniqueVertices.insert(pair);

			//Since a vertex could be added multiple times to this vector, only add it if the vertexHandle is new -> only unique's in this vector
			//clippingVertices.push_back(vertexHandle);
		}
		else
		{
			vertexHandle = (*doesVertexAlreadyExist).second;
		}

		return AddClipVertex(vertexHandle.idx(), clippingVertices);
	}

	std::vector<ICE::FRAC::MeshPolygon::FaceHandle> AddClipFaceToMesh(ICE::FRAC::MeshPolygon& mesh, std::vector<ICE::FRAC::ClipVertex>& clipVertices, ICE::FRAC::Vector3 normal)
	{
		using namespace ICE::FRAC;

		std::vector<MeshPolygon::FaceHandle> faces = std::vector<MeshPolygon::FaceHandle>();

		uint32_t clipVertexCount = static_cast<uint32_t>(clipVertices.size());
		std::vector<uint32_t> currentFaceIndices = std::vector<uint32_t>();
		currentFaceIndices.reserve(clipVertexCount);

		uint32_t maxIndex = std::numeric_limits<uint32_t>::max();
		size_t totalVertexCount = 0u;

		uint32_t currentIndex = maxIndex;

		//Remove duplicates in the edgeIndices. Duplicates can be created for penetrating edges (e.g. voronoi-edge going through multiple fingers)
		for (uint32_t i = 0u; i < clipVertexCount; i++)
		{
			ClipVertex& clipVertex = clipVertices[i];

			std::sort(clipVertex.edgeIndices.begin(), clipVertex.edgeIndices.end());
			clipVertex.edgeIndices.erase(std::unique(clipVertex.edgeIndices.begin(), clipVertex.edgeIndices.end()), clipVertex.edgeIndices.end());

			//Get the first vertex which has only 2 edges
			if (currentIndex == maxIndex && clipVertex.edgeIndices.size() == 2u)
			{
				currentIndex = i;
			}
		}

		do
		{
			ClipVertex& currentClipVertex = clipVertices[currentIndex];
			currentFaceIndices.clear();
			currentFaceIndices.push_back(currentIndex);

			CreateClipFace(currentIndex, currentIndex, currentClipVertex.edgeIndices[0], clipVertices, currentFaceIndices);

			size_t currentFaceIndexCount = currentFaceIndices.size();
			std::vector<MeshPolygon::VertexHandle> vertexHandles = std::vector<MeshPolygon::VertexHandle>(currentFaceIndexCount);
			for (size_t vi = 0u; vi < currentFaceIndexCount; vi++)
			{
				uint32_t faceIndex = currentFaceIndices[vi];
				vertexHandles[vi] = clipVertices[faceIndex].vertexHandle;
				clipVertices[faceIndex].wasVisited = true;
			}

			if (vertexHandles.size() > 2u)
			{
				//Add these vertices as a face to the mesh
				MeshPolygon::FaceHandle faceHandle = mesh.add_face(vertexHandles);
				if (faceHandle.idx() == -1)
				{
					std::reverse(vertexHandles.begin(), vertexHandles.end());
					faceHandle = mesh.add_face(vertexHandles);
				}
				if (faceHandle.idx() != -1)
				{
					mesh.set_normal(faceHandle, MeshPolygon::Normal(normal.x, normal.y, normal.z));
					faces.push_back(faceHandle);
				}
				else
				{

				}
			}
			else
			{
				//... pray
			}

			totalVertexCount += currentFaceIndexCount;

			//Check if all vertices were used -> if not that means there are multiple faces on this cell-face and we need to find a new startVertex
			if (totalVertexCount != clipVertexCount)
			{
				for (uint32_t vi = 0u; vi < clipVertexCount; vi++)
				{
					//if (clipVertices[vi].index != maxIndex)
					if (clipVertices[vi].wasVisited == false)
					{
						currentIndex = vi;
						break;
					}
				}
			}

		} while (totalVertexCount + 2u < clipVertexCount);


		return faces;
	}

	void FixClippedFace(ICE::FRAC::MeshPolygon& component, ICE::FRAC::VoronoiFace& vf)
	{
		using namespace ICE::FRAC;

		if (vf.clippedFaceInsideVertices[0] == vf.clippedFaceInsideVertices[1]) return;

		//Check if every vertex has 2 edges. If this is the case we can just return.
		size_t clippedVertexCount = vf.clippingVertices.size();
		bool allConnected = true;
		for (size_t i = 0u; i < clippedVertexCount; i++)
		{
			if (vf.clippingVertices[i].edgeIndices.size() != 2u)
			{
				allConnected = false;
			}
		}
		if (allConnected) return;

		/*	There is a chance that the voronoi cell is cutting into multiple component-faces in different directions.
		So we need to check each pair seperately
		*/
		Vector3 faceNormal = vf.normal;

		size_t clippedFaceCount = vf.clippedFaceInsideVertices.size();
		for (size_t clippedFaceIndex0 = 0u, clippedFaceIndex1 = 1u; clippedFaceIndex0 < clippedFaceCount && clippedFaceIndex1 < clippedFaceCount; clippedFaceIndex0 += 2u, clippedFaceIndex1 += 2u)
		{
			//This is another vertex on the component not matching any cell-vertices. We need it to have a reference where the other cell-vertices are placed in 3d-space
			Vector3 otherVertex;
			if (vf.clippedFaceInsideVertices[clippedFaceIndex0] == vf.clippedFaceOutsideVertices[clippedFaceIndex0] || vf.clippedFaceInsideVertices[clippedFaceIndex1] == vf.clippedFaceOutsideVertices[clippedFaceIndex0])
			{
				otherVertex = vf.clippedFaceOutsideVertices[clippedFaceIndex1];
			}
			else
			{
				otherVertex = vf.clippedFaceOutsideVertices[clippedFaceIndex0];
			}

			Vector3 direction = vf.clippedFaceInsideVertices[clippedFaceIndex1] - vf.clippedFaceInsideVertices[clippedFaceIndex0];
			direction = direction.normalize();
			Vector3 testDirection = otherVertex - vf.clippedFaceInsideVertices[clippedFaceIndex0];
			testDirection = testDirection.normalize();
			testDirection = direction.cross(testDirection);
			PRECISION det = testDirection.dot(faceNormal);
			if (det < 0.0)
			{
				//in this case the vertices are currently in a clockwise way -> swap
				std::swap(vf.clippedFaceInsideVertices[clippedFaceIndex0], vf.clippedFaceInsideVertices[clippedFaceIndex1]);
				std::swap(vf.clippedFaceInsideVertexHandles[clippedFaceIndex0], vf.clippedFaceInsideVertexHandles[clippedFaceIndex1]);
				direction = vf.clippedFaceInsideVertices[clippedFaceIndex1] - vf.clippedFaceInsideVertices[clippedFaceIndex0];
			}

			std::vector<Vector3> newVertices = std::vector<Vector3>();
			newVertices.push_back(vf.clippedFaceInsideVertices[clippedFaceIndex1]);
			std::vector<MeshPolygon::VertexHandle> newVertexHandles = std::vector<MeshPolygon::VertexHandle>();
			newVertexHandles.push_back(vf.clippedFaceInsideVertexHandles[clippedFaceIndex1]);

			/*  We know that the vertices inside vf.vertices are in cw-order since they were added via their face-indices of voro++.
			First we need to find vf.clippedFaceInsideVertices[clippedFaceIndex0] to get their index in the vf.vertices-array
			Then we need to check every vertex starting from there to preserve the vertex-order
			*/
			size_t faceVertexCount = vf.vertices.size();
			size_t faceVertexStartingIndex = 0u;
			size_t faceVertexEndIndex = 0u;

			for (size_t v = 0u; v < faceVertexCount; v++)
			{
				Vector3 cellVertex = vf.vertices[v];
				if (cellVertex == vf.clippedFaceInsideVertices[clippedFaceIndex0])
				{
					faceVertexStartingIndex = v;
				}
				if (cellVertex == vf.clippedFaceInsideVertices[clippedFaceIndex1])
				{
					faceVertexEndIndex = v;
				}
			}

			size_t v = faceVertexStartingIndex;
			do
			{
				v++;
				if (v == faceVertexCount) v = 0;

				Vector3 cellVertex = vf.vertices[v];
				if (cellVertex == vf.clippedFaceInsideVertices[clippedFaceIndex0] || cellVertex == vf.clippedFaceInsideVertices[clippedFaceIndex1]) continue;

				testDirection = cellVertex - vf.clippedFaceInsideVertices[clippedFaceIndex0];
				testDirection = direction.cross(testDirection);
				det = testDirection.dot(faceNormal);
				if (det < 0.0)
				{
					//This is a vertex of the other half of the cell-face
					newVertices.push_back(cellVertex);
					MeshPolygon::VertexHandle vertexHandle = component.add_vertex(MeshPolygon::Point(cellVertex.x, cellVertex.y, cellVertex.z));
					newVertexHandles.push_back(vertexHandle);
				}
			} while (v != faceVertexStartingIndex);

			newVertices.push_back(vf.clippedFaceInsideVertices[clippedFaceIndex0]);
			newVertexHandles.push_back(vf.clippedFaceInsideVertexHandles[clippedFaceIndex0]);

			size_t newVertexCount = newVertices.size();
			//vf.clippVertices needs more memory beforehand, otherwise it can invalidate the pointer to the clipVertices inside
			vf.clippingVertices.reserve(vf.clippingVertices.size() + newVertexCount);

			ClipVertex* prevClipVertex = nullptr;
			for (size_t i = 0u; i < newVertexCount; i++)
			{
				ClipVertex& clipVertex = AddClipVertex(newVertexHandles[i].idx(), vf.clippingVertices);

				std::sort(clipVertex.edgeIndices.begin(), clipVertex.edgeIndices.end());
				clipVertex.edgeIndices.erase(std::unique(clipVertex.edgeIndices.begin(), clipVertex.edgeIndices.end()), clipVertex.edgeIndices.end());

				if (prevClipVertex != nullptr)
				{
					if (clipVertex.edgeIndices.size() < 2u && prevClipVertex->edgeIndices.size() < 2u)
					{
						clipVertex.edgeIndices.push_back(prevClipVertex->index);
						prevClipVertex->edgeIndices.push_back(clipVertex.index);
					}
				}

				prevClipVertex = &clipVertex;
			}
		}

		//Check if any vertex still only has 1 edge. If this is the case there should be 2 of them
		std::vector<uint32_t> vertexIndices = std::vector <uint32_t>();
		clippedVertexCount = vf.clippingVertices.size();
		for (size_t i = 0u; i < clippedVertexCount; i++)
		{
			if (vf.clippingVertices[i].edgeIndices.size() == 1u)
			{
				vertexIndices.push_back(vf.clippingVertices[i].index);
			}
		}

		if (vertexIndices.size() == 2u)
		{
			vf.clippingVertices[vertexIndices[0]].edgeIndices.push_back(vertexIndices[1]);
			vf.clippingVertices[vertexIndices[1]].edgeIndices.push_back(vertexIndices[0]);
		}

	}

	voro::container* CreateFracturePattern(ICE::Vec3 sizeMin, ICE::Vec3 sizeMax, ICE::Vec3 localImpactLocation, int& numberOfParticles)
	{

		//Number of blocks the container is divided into. Needed for voro++
		const int blockSize = 6;
		const double mu = 5.0;

		//Create a voronoi container with the data above. Allocates space for 16 random points per block (for std::vectors inside)
		voro::container* outPattern = new voro::container(static_cast<double>(sizeMin.x) * mu, static_cast<double>(sizeMax.x) * mu, static_cast<double>(sizeMin.y) * mu,
			static_cast<double>(sizeMax.y) * mu, static_cast<double>(sizeMin.z) * mu, static_cast<double>(sizeMax.z) * mu, blockSize, blockSize, blockSize, false, false, false, 16);
		int index = 0;
#if 0
		double impactX = static_cast<double>(localImpactLocation.x);
		double impactY = static_cast<double>(localImpactLocation.y);
		double impactZ = static_cast<double>(localImpactLocation.z);
		outPattern->put(index++, impactX + 0.0, impactY + 0.0, impactZ + 0.0);
		outPattern->put(index++, impactX + 0.1, impactY + 0.1, impactZ + 0.1);
		outPattern->put(index++, impactX + 0.1, impactY + 0.1, impactZ + -0.1);
		outPattern->put(index++, impactX + 0.1, impactY + -0.1, impactZ + 0.1);
		outPattern->put(index++, impactX + 0.1, impactY + -0.1, impactZ + -0.1);
		outPattern->put(index++, impactX + -0.1, impactY + 0.1, impactZ + 0.1);
		outPattern->put(index++, impactX + -0.1, impactY + 0.1, impactZ + -0.1);
		outPattern->put(index++, impactX + -0.1, impactY + -0.1, impactZ + 0.1);
		outPattern->put(index++, impactX + -0.1, impactY + -0.1, impactZ + -0.1);
#else
		double impactX = static_cast<double>(localImpactLocation.x);
		double impactY = static_cast<double>(localImpactLocation.y);
		double impactZ = static_cast<double>(localImpactLocation.z);
		if (ICE::GetRandom<int>(0, 1) == 1)
		{
			outPattern->put(index++, impactX + 0.0, impactY + 0.0, impactZ + 0.0);
		}
		outPattern->put(index++, impactX + 0.05, impactY + 0.05, impactZ + 0.05);
		outPattern->put(index++, impactX + 0.05, impactY + 0.05, impactZ + -0.05);
		outPattern->put(index++, impactX + 0.05, impactY + -0.05, impactZ + 0.05);
		outPattern->put(index++, impactX + 0.05, impactY + -0.05, impactZ + -0.05);
		outPattern->put(index++, impactX + -0.05, impactY + 0.05, impactZ + 0.05);
		outPattern->put(index++, impactX + -0.05, impactY + 0.05, impactZ + -0.05);
		outPattern->put(index++, impactX + -0.05, impactY + -0.05, impactZ + 0.05);
		outPattern->put(index++, impactX + -0.05, impactY + -0.05, impactZ + -0.05);

		for (; index < 16; index++)
		{
			outPattern->put(index, ICE::GetRandom(sizeMin.x, sizeMax.x), ICE::GetRandom(sizeMin.y, sizeMax.y), ICE::GetRandom(sizeMin.z, sizeMax.z));
		}
#endif

		numberOfParticles = index;
		outPattern->compute_all_cells();

		return outPattern;
	}

	void PrepareVoroCellFaces(voro::container* con, int maxParticles, 
		std::vector<int>& indexToPID, std::vector<voro::voronoicell_neighbor>& voroCells, std::vector<ICE::FRAC::Vector3>& voroParticleCenters,
		std::vector<std::vector<ICE::FRAC::VoronoiFace>>& voroCellFaces,
		int& outValidParticleCount)
	{
		outValidParticleCount = 0;

		indexToPID = std::vector<int>(maxParticles);
		voroCells = std::vector<voro::voronoicell_neighbor>(maxParticles);
		voroParticleCenters = std::vector<ICE::FRAC::Vector3>(maxParticles);
		voroCellFaces = std::vector<std::vector<ICE::FRAC::VoronoiFace>>(maxParticles);
		std::vector<ICE::FRAC::VoronoiFace> voroCellFace = std::vector<ICE::FRAC::VoronoiFace>();

		//Prepare the voronoi-data to be more easily accessed
		{
			voro::c_loop_all conLoop(*con);
			if (!conLoop.start())
			{
				std::cout << "The voronoi-cell-fracture was empty!" << std::endl;
			}
			voro::voronoicell_neighbor voroCell;
			int componentID = 0;
			double pCenterX = 0.0, pCenterY = 0.0, pCenterZ = 0.0;
			do
			{
				int pid = conLoop.pid();
				//Check if the current cell in the loop can be computed
				if (con->compute_cell(voroCell, conLoop))
				{
					indexToPID[componentID] = pid;
					voroCells[componentID] = voroCell;

					//The vertices that are saved in the cell have a coordinate-system around the particle. 
					//Therefore we need to translate them to where they are needed. For this pre-process step we need the coordinates around the origin (0/0/0)
					conLoop.pos(pCenterX, pCenterY, pCenterZ);
					ICE::FRAC::Vector3 particleCenter = ICE::FRAC::Vector3(static_cast<PRECISION>(pCenterX), static_cast<PRECISION>(pCenterY), static_cast<PRECISION>(pCenterZ));
					voroParticleCenters[componentID] = particleCenter;

					voroCellFace.clear();
					voroCellFace = GetVoronoiCellFaces(voroCell, particleCenter);
					voroCellFaces[componentID] = voroCellFace;

				}
				componentID++;
				outValidParticleCount++;
			} while (conLoop.inc());
		}
	}

	std::vector<ICE::FRAC::MeshPolygon> ComputeFracture(ICE::FRAC::MeshPolygon& mesh, ICE::Vec3 impLoc,
		voro::container* con, int maxParticles, int validParticles,
		std::vector<int>& indexToPID, std::vector<voro::voronoicell_neighbor>& voroCells, std::vector<ICE::FRAC::Vector3>& voroParticleCenters,
		std::vector<std::vector<ICE::FRAC::VoronoiFace>>& voroCellFacesAll)
	{
		using namespace ICE::FRAC;
		
		int maxParticleCount = maxParticles;//con->total_particles();
		int validParticleCount = validParticles;

		Vector3 impactLocation = Vector3(impLoc.x, impLoc.y, impLoc.z);
		//Voronoi variables
		double pCenterX = 0.0, pCenterY = 0.0, pCenterZ = 0.0;
		Vector3 voroParticleCenter;

		//An array that tracks if a voronoi-face has clipped a mesh-face. This will be used to create the component-face or faces on the splitting cell-face.
		std::vector<EdgePoint> edgeConnections = std::vector<EdgePoint>();
		edgeConnections.reserve(32u);

		//OpenMesh variables
		//The compound holds the component pieces of the fractured mesh
		std::vector<MeshPolygon> compound = std::vector<MeshPolygon>(maxParticleCount);
		compound.reserve(maxParticleCount * 2);
		//compound.reserve(con->total_particles());
		std::vector<Vector3> meshFaceVertexPosAll = std::vector<Vector3>();

		//std::cout << "Starting the fracture." << std::endl;

		//Iterate through all cells of the voronoi diagram
		for (int componentID = 0; componentID < validParticleCount; componentID++)
		{
			int pid = indexToPID[componentID];

			//Get the faces of the cell in global coordinates
			voro::voronoicell_neighbor& voroCell = voroCells[componentID];
			voroParticleCenter = voroParticleCenters[componentID];
			//pCenterX = static_cast<double>(voroParticleCenter.x - impactLocation.x);
			//pCenterY = static_cast<double>(voroParticleCenter.y - impactLocation.y);
			//pCenterZ = static_cast<double>(voroParticleCenter.z - impactLocation.z);
			pCenterX = static_cast<double>(voroParticleCenter.x);
			pCenterY = static_cast<double>(voroParticleCenter.y);
			pCenterZ = static_cast<double>(voroParticleCenter.z);
			std::vector<ICE::FRAC::VoronoiFace> voroCellFaces = std::vector<ICE::FRAC::VoronoiFace>(voroCellFacesAll[componentID]);
			uint32_t voroCellFaceCount = static_cast<uint32_t>(voroCellFaces.size());

			edgeConnections.clear();

			MeshPolygon component = MeshPolygon();
			//Holds unique vertices for the current cell
			std::unordered_map<Vector3, MeshPolygon::VertexHandle> uniqueVertices = std::unordered_map<Vector3, MeshPolygon::VertexHandle>();

			//Loops over all the faces of the mesh
			for (MeshPolygon::FaceIter meshFaceIt = mesh.faces_sbegin(); meshFaceIt != mesh.faces_end(); meshFaceIt++)
			{
				MeshPolygon::FaceHandle meshFace = *meshFaceIt;

				bool allInside = true;
				int searchedPID = -1;
				meshFaceVertexPosAll.clear();
				//Holds the vertices for the new face
				std::vector<MeshPolygon::VertexHandle> newFaceVertices = std::vector<MeshPolygon::VertexHandle>();
				newFaceVertices.reserve(8u);

				Vector3 faceNormal = Vector3(mesh.normal(meshFace));

				//Loop over all the vertices of this face. If all vertices are in this cell, then we can simply add it to the MeshPolygon. The reverse is not true!
				for (MeshPolygon::FaceVertexIter meshFaceVertexIt = mesh.fv_begin(meshFace); meshFaceVertexIt != mesh.fv_end(meshFace); meshFaceVertexIt++)
				{
					MeshPolygon::VertexHandle faceVertexHandle = *meshFaceVertexIt;

					Vector3 vertexPoint = Vector3(mesh.point(faceVertexHandle));
					con->find_voronoi_cell(vertexPoint.x, vertexPoint.y, vertexPoint.z, pCenterX, pCenterY, pCenterZ, searchedPID);

					if (searchedPID != pid)
					{
						allInside = false;
					}
					else if (searchedPID == pid)
					{
						//Check if that vertex has already been added to the new component
						std::unordered_map<Vector3, MeshPolygon::VertexHandle>::iterator doesVertexAlreadyExist = uniqueVertices.find(vertexPoint);

						MeshPolygon::VertexHandle vertexHandle;
						if (doesVertexAlreadyExist == uniqueVertices.end())
						{
							vertexHandle = component.add_vertex(mesh.point(faceVertexHandle));
							component.set_color(vertexHandle, mesh.color(faceVertexHandle));
							component.set_normal(vertexHandle, mesh.normal(faceVertexHandle));

							std::pair<Vector3, MeshPolygon::VertexHandle> pair = std::pair<Vector3, MeshPolygon::VertexHandle>(vertexPoint, vertexHandle);
							uniqueVertices.insert(pair);
						}
						else
						{
							vertexHandle = (*doesVertexAlreadyExist).second;
						}
						//If the face-vertex is inside they are naturally part of the face that will be added to the new component
						newFaceVertices.push_back(vertexHandle);
					}
					meshFaceVertexPosAll.push_back(vertexPoint);
				}

				//If all the vertices of the face are inside the voronoi-cell -> no cut required
				if (!allInside)
				{
					//if not all vertices are on the outside, we need to test the edges of the face againt the voronoi-cell
					Vector3 p0, p1;
					//remember the vertexHandles too for normals and texCoordinates
					MeshPolygon::VertexHandle v0, v1;
					for (MeshPolygon::FaceEdgeIter meshFaceEdgeIt = mesh.fe_begin(meshFace); meshFaceEdgeIt != mesh.fe_end(meshFace); meshFaceEdgeIt++)
					{
						MeshPolygon::EdgeHandle edgeHandle = *meshFaceEdgeIt;

						v0 = mesh.from_vertex_handle(mesh.halfedge_handle(edgeHandle, 0));
						v1 = mesh.to_vertex_handle(mesh.halfedge_handle(edgeHandle, 0));

						p0 = Vector3(mesh.point(v0));
						p1 = Vector3(mesh.point(v1));

						//Line line = Line(p0, p1);
						for (uint32_t vfi = 0u; vfi < voroCellFaceCount; vfi++)
						{
							VoronoiFace& vf = voroCellFaces[vfi];
							Vector3 intersectionPoint = Vector3(NAN, NAN, NAN);
							//Check if line intersects the face
							if (CheckLineFaceIntersection(vf.plane, vf.normal, vf.vertices, p0, p1, intersectionPoint))
							{
								//Add the intersection point to the new component and check if it's a duplicate
								ClipVertex& clipVertex = AddPointToComponent(intersectionPoint, p0, p1, v0, v1, uniqueVertices, mesh, component, vf.clippingVertices);
								newFaceVertices.push_back(clipVertex.vertexHandle);

								EdgePoint edgePoint;
								edgePoint.faceHandleIdx = meshFace.idx();
								edgePoint.voroFaceIndex = vfi;
								edgePoint.clipIndex = clipVertex.index;
								edgeConnections.push_back(edgePoint);
							}
						}//for(voronoiFaces)

					}//for(meshFaceEdgeIt)


					 //We need to check if any edge of a voronoi-face intersect the current mesh-face to generate a new intersection-Point
					Plane meshFacePlane = Plane(faceNormal, meshFaceVertexPosAll[0]);
					//TODO: Get decent vertexHandles for normal-interpolation of vertices
					MeshPolygon::VertexHandle vh0 = MeshPolygon::VertexHandle();

					for (uint32_t vfi = 0u; vfi < voroCellFaceCount; vfi++)
					{
						VoronoiFace& vf = voroCellFaces[vfi];
						size_t vertexCount = vf.vertices.size();
						for (size_t i = 0u; i < vertexCount; i++)
						{
							size_t otherEdge = i + 1u;
							if (otherEdge == vertexCount) otherEdge = 0;

							Vector3 linePoint0 = vf.vertices[i];
							Vector3 linePoint1 = vf.vertices[otherEdge];
							//Line line = Line(linePoint0, linePoint1);

							Vector3 intersectionPoint = Vector3(NAN, NAN, NAN);
							//Check if line intersects the face
							if (CheckLineFaceIntersection(meshFacePlane, faceNormal, meshFaceVertexPosAll, linePoint0, linePoint1, intersectionPoint))
							{
								vf.clippingVertices.reserve(vf.clippingVertices.size() + 2u);
								//Add the intersection point to the new component and check if it's a duplicate
								ClipVertex& clipVertexInner = AddPointToComponent(intersectionPoint, linePoint0, linePoint1, vh0, vh0, uniqueVertices, mesh, component, vf.clippingVertices);
								newFaceVertices.push_back(clipVertexInner.vertexHandle);

								EdgePoint edgePointFace;
								edgePointFace.faceHandleIdx = meshFace.idx();
								edgePointFace.voroFaceIndex = vfi;
								edgePointFace.clipIndex = clipVertexInner.index;
								edgeConnections.push_back(edgePointFace);

								{
									//Now we need to check if the line ends inside the mesh. This will generate the volume-information of the mesh.
									//Check which of the 2 linePoints is inside the mesh by checking against the normal of the face
									Vector3 lineDirection = linePoint0 - intersectionPoint;
									PRECISION determinant = faceNormal.dot(lineDirection);
									//If this is positive it means that it is currently facing outwards, which is not what we need.
									if (determinant >= 0.0)
									{
										std::swap(linePoint0, linePoint1);
										lineDirection = linePoint0 - intersectionPoint;
									}

									bool hasIntersected = false;
									MeshPolygon::FaceHandle shortestIntersectionFaceHandle = MeshPolygon::FaceHandle();
									Vector3 shortestIntersectionPoint = Vector3(NAN, NAN, NAN);
									Vector3 shortestLinePoint0 = Vector3(NAN, NAN, NAN);
									Vector3 shortestLinePoint1 = Vector3(NAN, NAN, NAN);
									//Now we need to check every other face
									for (MeshPolygon::FaceIter meshFaceVolumeIt = mesh.faces_sbegin(); meshFaceVolumeIt != mesh.faces_end(); meshFaceVolumeIt++)
									{
										//We can skip the check on the same face
										if (meshFaceVolumeIt == meshFaceIt) continue;

										MeshPolygon::FaceHandle meshFaceVolume = *meshFaceVolumeIt;
										Vector3 faceVolumeNormal = mesh.normal(meshFaceVolume);

										std::vector<Vector3> verticesVolume = std::vector<Vector3>(); verticesVolume.reserve(8u);
										for (MeshPolygon::FaceVertexIter fvi = mesh.fv_begin(meshFaceVolume); fvi != mesh.fv_end(meshFaceVolume); fvi++)
										{
											verticesVolume.push_back(Vector3(mesh.point(*fvi)));
										}

										Plane meshFaceVolumePlane = Plane(faceVolumeNormal, verticesVolume[0]);

										Vector3 volumePoint = Vector3(NAN, NAN, NAN);
										if (CheckLineFaceIntersection(meshFaceVolumePlane, faceVolumeNormal, verticesVolume, intersectionPoint, linePoint0, volumePoint))
										{
											//Check if the intersectionPoint and the new volumePoint are too close
											//This can happen when the intersectionPoint is on the edge of a face
											if (intersectionPoint == volumePoint)
											{
												continue;
											}

											hasIntersected = true;
											if (shortestIntersectionFaceHandle.idx() == -1)
											{
												shortestIntersectionFaceHandle = meshFaceVolume;
												shortestIntersectionPoint = volumePoint;
											}
											else
											{
												Vector3 shortestDistanceVector = shortestIntersectionPoint - intersectionPoint;
												PRECISION shortestDistance = shortestDistanceVector.length();

												Vector3 newDistanceVector = volumePoint - intersectionPoint;
												PRECISION newDistance = newDistanceVector.length();

												if (newDistance < shortestDistance)
												{
													shortestIntersectionFaceHandle = meshFaceVolume;
													shortestIntersectionPoint = volumePoint;
												}
											}
										}

									}

									if (!hasIntersected)
									{
										ClipVertex& clipVertex = AddPointToComponent(linePoint0, intersectionPoint, linePoint0, vh0, vh0, uniqueVertices, mesh, component, vf.clippingVertices);
										vf.clippedFaceInsideVertices.push_back(linePoint0);
										vf.clippedFaceInsideVertexHandles.push_back(clipVertex.vertexHandle);
										vf.clippedFaceOutsideVertices.push_back(linePoint1);

										clipVertex.edgeIndices.push_back(clipVertexInner.index);
										clipVertexInner.edgeIndices.push_back(clipVertex.index);
									}
									else
									{
										ClipVertex& clipVertex = AddPointToComponent(shortestIntersectionPoint, linePoint0, linePoint1, vh0, vh0, uniqueVertices, mesh, component, vf.clippingVertices);
										clipVertex.edgeIndices.push_back(clipVertexInner.index);
										clipVertexInner.edgeIndices.push_back(clipVertex.index);
									}

								} //End volume point check

							}
						}
					}

				}//if(!allInside)
				else
				{
					//If all vertices are inside -> face is inside -> can only be in this voronoi-cell -> Remove it from the mesh to reduce checks in other cells
					mesh.delete_face(meshFace, true);
				}

				if (newFaceVertices.size() > 0)
				{
					//Sort and then remove all duplicate elements.
					std::sort(newFaceVertices.begin(), newFaceVertices.end(), vertexHandleCompare);
					newFaceVertices.erase(std::unique(newFaceVertices.begin(), newFaceVertices.end()), newFaceVertices.end());

					//There is a chance that an edge shares and edge with a voronoi-cell -> no face
					if (newFaceVertices.size() > 2u)
					{
						AddFaceToMesh(component, newFaceVertices, faceNormal);
					}
				}

			} //End Loop Faces of mesh

			  //Resolve the edge connections for the cut faces of the splitting-face
			size_t edgeConnectionCount = edgeConnections.size();
			const uint32_t maxIndex = std::numeric_limits<uint32_t>::max();
			for (size_t i = 0u; i < edgeConnectionCount; i++)
			{
				EdgePoint& outerPoint = edgeConnections[i];
				uint32_t voroFaceIndex = outerPoint.voroFaceIndex;
				if (voroFaceIndex == maxIndex) continue;

				for (size_t k = i + 1u; k < edgeConnectionCount; k++)
				{
					EdgePoint& innerPoint = edgeConnections[k];

					if (outerPoint.voroFaceIndex != innerPoint.voroFaceIndex) continue;
					if (outerPoint.faceHandleIdx != innerPoint.faceHandleIdx) continue;
					if (outerPoint.clipIndex == innerPoint.clipIndex) continue;

					voroCellFaces[voroFaceIndex].clippingVertices[outerPoint.clipIndex].edgeIndices.push_back(innerPoint.clipIndex);
					voroCellFaces[voroFaceIndex].clippingVertices[innerPoint.clipIndex].edgeIndices.push_back(outerPoint.clipIndex);

					innerPoint.voroFaceIndex = maxIndex;
					outerPoint.voroFaceIndex = maxIndex;
					break;
				}
			}

			//After all the faces have been checked with this cell, check if planes have been cut
			for (VoronoiFace& vf : voroCellFaces)
			{
				//If a voronoi-face has cut mesh-faces then the VertexHandles of the generated vertices have been saved. These make up a new face to plug the whole.
				if (vf.clippingVertices.size() != 0)
				{
					if (vf.clippedFaceInsideVertices.size() >= 2)
					{
						FixClippedFace(component, vf);
					}

					if (vf.clippingVertices.size() > 2)
					{
						AddClipFaceToMesh(component, vf.clippingVertices, vf.normal);
					}
				}

				vf.clippedFaceInsideVertexHandles.clear();
				vf.clippedFaceInsideVertices.clear();
				vf.clippedFaceOutsideVertices.clear();
				vf.clippingVertices.clear();
				vf.vertices.clear();
			}

			//compound.push_back(component);
			compound[componentID] = component;
		}

		//std::cout << "Surface decomposition done." << std::endl;



		//std::cout << "Adding internal componentes." << std::endl;

		/*	The next step is to include internal componentes as well.
		This is a tricky task since there can be multiple internal componentes next to each other depending on the fracture-cell sizes

		1) Create an array, flood array, for all cells. Voronoi-cells have connectivity-information, therefore we can check if a cell is bordering the border of the complete voronoi diagramm.
		(voro++ neighbour information for this is a negative value)
		2) The flood array tracks the following states:
		0 - Possible internal componentes
		1 - Walled component - Component is bordering a wall or bordering a component that is bordering a wall, etc.
		2 - Surface Component

		3) Check the array until no state change has occured for a full check. Use the following rules:
		- If a component with a 0 has ANY neighbour with state 1 it turn into a 1
		*/
		MeshPolygon::VertexHandle vertexHandleTest = MeshPolygon::VertexHandle(0);
		std::vector<double> cellVertices = std::vector<double>();
		std::vector<int> cellFaceIndices = std::vector<int>();
		std::vector<double> cellFaceNormals = std::vector<double>();

		std::vector<int> neighbors = std::vector<int>();
		neighbors.reserve(16u);
		size_t neighborCount = 0;

		std::vector<int> internalComponentID = std::vector<int>();
		std::vector<int> surfaceComponentID = std::vector<int>();
		std::vector<uint8_t> cellStateArrayPID = std::vector<uint8_t>(maxParticleCount);
		//Creating the cellStateArray
		{
			bool stateHasChanged = false;

			//Initial Setup
			for (int componentID = 0; componentID < validParticleCount; componentID++)
			{
				int pid = indexToPID[componentID];
				cellStateArrayPID[pid] = 0;

				MeshPolygon& meshTest = compound[componentID];
				//Simple check if this component has a vertex
				if (meshTest.is_valid_handle(vertexHandleTest))
				{
					cellStateArrayPID[pid] = 2;
					surfaceComponentID.push_back(componentID);
					continue;
				}

				//If it is not a surface component it could be a walled component
				voro::voronoicell_neighbor& voroCell = voroCells[componentID];
				voroCell.neighbors(neighbors);

				neighborCount = neighbors.size();
				for (size_t neighborIndex = 0u; neighborIndex < neighborCount; neighborIndex++)
				{
					//if this id is negative -> Cell is against the outermost-bound of the fracture, therefore it cannot be an internal component
					if (neighbors[neighborIndex] < 0)
					{
						cellStateArrayPID[pid] = 1;
						break;
					}
				}

			}

			//Check until state does not change
			do
			{
				stateHasChanged = false;

				for (int componentID = 0; componentID < validParticleCount; componentID++)
				{
					int pid = indexToPID[componentID];

					//Only the possible internal componentes, 0, can change their states
					if (cellStateArrayPID[pid] != 0) continue;

					//If it is not a surface component it could be a walled component
					voro::voronoicell_neighbor& voroCell = voroCells[componentID];
					voroCell.neighbors(neighbors);
					neighborCount = neighbors.size();

					for (size_t i = 0u; i < neighborCount; i++)
					{
						//if this id is negative -> Cell is against the outermost-bound of the fracture, therefore it cannot be an internal component
						if (cellStateArrayPID[neighbors[i]] == 1)
						{
							cellStateArrayPID[pid] = 1;
							stateHasChanged = true;
							break;
						}
					}
				} //end for
			} while (stateHasChanged == true);

			for (int componentID = 0; componentID < validParticleCount; componentID++)
			{
				int pid = indexToPID[componentID];

				if (cellStateArrayPID[pid] == 0)
				{
					internalComponentID.push_back(componentID);
				}
			}
		} //end of creation of cellStateArray

		std::vector<MeshPolygon::VertexHandle> componentVertices = std::vector<MeshPolygon::VertexHandle>();
		componentVertices.reserve(8u);
		std::vector<MeshPolygon::VertexHandle> componentCurrentFace = std::vector<MeshPolygon::VertexHandle>();
		componentCurrentFace.reserve(8u);
		std::vector<int> modifiedSurfaceComponentes = std::vector<int>();
		//Now that we identified the internal componentes we can add their voronoi cells as a component
		size_t internalComponentCount = internalComponentID.size();
		for (int i = 0; i < internalComponentCount; i++)
		{
			int componentID = internalComponentID[i];

			//We now know that this component is an internal component. We can 'simply' add all it's vertices to the component
			voro::voronoicell_neighbor& voroCell = voroCells[componentID];
			voroParticleCenter = voroParticleCenters[componentID];
			pCenterX = static_cast<double>(voroParticleCenter.x);
			pCenterY = static_cast<double>(voroParticleCenter.y);
			pCenterZ = static_cast<double>(voroParticleCenter.z);

			voroCell.vertices(pCenterX, pCenterY, pCenterZ, cellVertices);
			voroCell.face_vertices(cellFaceIndices);
			voroCell.normals(cellFaceNormals);

			MeshPolygon& component = compound[componentID];
			componentVertices.clear();
			componentVertices.resize(voroCell.p);

			//voro++ saves the indices as follows: faceOrder, index, ..., index, faceOrder, index, ...
			{
				size_t faceIndicesSize = cellFaceIndices.size();
				for (size_t fi = 0u, faceNormalIndex = 0u; fi < faceIndicesSize; )
				{
					componentCurrentFace.clear();
					int faceOrder = cellFaceIndices[fi++];
					MeshPolygon::Point vertex;
					for (size_t faceIndex = 0u; faceIndex < faceOrder; faceIndex++)
					{
						size_t vertexIndex = cellFaceIndices[fi++];
						if (componentVertices[vertexIndex].idx() == -1)
						{
							size_t vertexIndexAdjusted = vertexIndex * 3u;
							vertex[0] = static_cast<PRECISION>(cellVertices[vertexIndexAdjusted]);
							vertex[1] = static_cast<PRECISION>(cellVertices[vertexIndexAdjusted + 1]);
							vertex[2] = static_cast<PRECISION>(cellVertices[vertexIndexAdjusted + 2]);

							componentVertices[vertexIndex] = component.add_vertex(vertex);
						}

						componentCurrentFace.push_back(componentVertices[vertexIndex]);
					}
					//Faces in voro++ are in cw-order -> reverse is needed
					std::reverse(componentCurrentFace.begin(), componentCurrentFace.end());
					MeshPolygon::FaceHandle faceHandle = component.add_face(componentCurrentFace);

					MeshPolygon::Normal normal = MeshPolygon::Normal();
					normal[0] = static_cast<PRECISION>(cellFaceNormals[faceNormalIndex++]);
					normal[1] = static_cast<PRECISION>(cellFaceNormals[faceNormalIndex]);
					normal[2] = static_cast<PRECISION>(cellFaceNormals[faceNormalIndex]);

					component.set_normal(faceHandle, normal);
				}
			} //End creating internal component

			  /*	One important step is to add the faces of the internal componentes to it's neighbour as well.
			  Only add those faces to other surface componentes!
			  */

			//Get the faces of the cell in global coordinates
			//voroCellFaces = GetVoronoiCellFaces(voroCell, voroParticleCenter);
			voroCell.neighbors(neighbors);
			neighborCount = neighbors.size();

			for (size_t k = 0u; k < neighborCount; k++)
			{
				//Do not add other faces to other internal componentes!
				if (cellStateArrayPID[neighbors[k]] != 2) continue;

				//Search for the componentID
				int neighborPID = neighbors[k];
				for (int componentIndex = 0; componentIndex < validParticleCount; componentIndex++)
				{
					if (neighborPID == indexToPID[componentIndex])
					{
						//Remember that this surface component has been modified
						modifiedSurfaceComponentes.push_back(componentIndex);

						break;
					}
				}


			} //End loop internal component and neighbors
		} //End loop internal componentes

		  /*	The final step is to possibly connect the face bordering the internal component with the rest of the component
		  This is because we only added the vertex that penetrates into the mesh (and the corresponding face).
		  The vertices/faces that reach farther into the mesh have not yet been added.
		  */
		//std::cout << "Connecting surface componentes to internal componentes." << std::endl;

#if 1
		//size_t modifiedSurfaceComponentCount = modifiedSurfaceComponentes.size();
		size_t surfaceComponentCount = surfaceComponentID.size();

		//for (int i = 0; i < modifiedSurfaceComponentCount; i++)
		for (int i = 0; i < surfaceComponentCount; i++)
		{
			//int componentID = modifiedSurfaceComponentes[i];
			int componentID = surfaceComponentID[i];
			voro::voronoicell_neighbor& voroCell = voroCells[componentID];
			voroParticleCenter = voroParticleCenters[componentID];
			//std::vector<ICE::FRAC::VoronoiFace>& voroCellFaces = voroCellFacesAll[componentID];
			std::vector<ICE::FRAC::VoronoiFace> voroCellFaces = std::vector<ICE::FRAC::VoronoiFace>(voroCellFacesAll[componentID]);

			size_t voroCellFaceCount = voroCellFaces.size();
			MeshPolygon& component = compound[componentID];

			std::vector<bool> vertexMatch = std::vector<bool>();
			//The ring of vertices that are on the inside of the component
			std::unordered_set<Vector3> innerVerticesUnique = std::unordered_set<Vector3>();
			//The ring of vertices that are on the outside of the component
			std::unordered_set<Vector3> outerVerticesUnique = std::unordered_set<Vector3>();

			/*	Check each voronoi face against the surface component's face.
			If their normals point in the same direction, then we can compare their vertices.
			If there are 2 or more matches then we can identify which of the other vertices are on the outside
			*/
			PRECISION det = 0.0;
			for (VoronoiFace& vf : voroCellFaces)
			{
				vertexMatch.clear();
				for (MeshPolygon::FaceIter faceIt = component.faces_begin(); faceIt != component.faces_end(); faceIt++)
				{
					MeshPolygon::FaceHandle faceHandle = *faceIt;
					Vector3 faceNormal = component.normal(faceHandle);
					det = vf.normal.dot(faceNormal);

					//Check if the normals match
					if (det >= 0.9999)
					{
						size_t voroVertexCount = vf.vertices.size();
						vertexMatch = std::vector<bool>(voroVertexCount, false);
						//This is another vertex on the component not matching any cell-vertices. We need it to have a reference where the other cell-vertices are placed in 3d-space
						int firstFaceVertexHandleIdx = -1;

						//for (MeshPolygon::FaceVertexIter faceVertexIt = component.fv_begin(faceHandle); faceVertexIt.is_valid(); ++faceVertexIt)
						for (MeshPolygon::FaceVertexIter faceVertexIt = component.fv_begin(faceHandle); faceVertexIt != component.fv_end(faceHandle); faceVertexIt++)
						{
							MeshPolygon::VertexHandle vertexHandle = *faceVertexIt;
							if (firstFaceVertexHandleIdx == -1)
							{
								firstFaceVertexHandleIdx = vertexHandle.idx();
							}
							else if (firstFaceVertexHandleIdx == vertexHandle.idx())
							{
								break;
							}

							Vector3 componentVertex = Vector3(component.point(vertexHandle));
							for (size_t v = 0u; v < voroVertexCount; v++)
							{
								Vector3 cellVertex = vf.vertices[v];
								if (componentVertex == cellVertex)
								{
									vertexMatch[v] = true;
									break;
								}
							}
						} //End iterate component-faces against cell-faces
						  //if there are not enough vertex Matches we found a similar face

						for (size_t v = 0u; v < voroVertexCount; v++)
						{
							if (vertexMatch[v] == false)
							{
								outerVerticesUnique.insert(vf.vertices[v]);
							}
							else
							{
								innerVerticesUnique.insert(vf.vertices[v]);
							}
						}
						//if the faces matches we can break and check other faces
						break;
					} //end if(det)
				}
			}

			/*	At this point we have the outerVerticesUnique that are just outside the component and the insideVerticesUnique.
			faceState: 0 - possibly inside; 1 - outside, 2 - bridging faces, 3 - inside
			*/

			std::vector<int8_t> faceState = std::vector<int8_t>(voroCellFaceCount, 0);
			bool outerVertexMatched = false, innerVertexMatched = false;

			//Compute the faceState-Vector until nothing changed in a round (same as above but with an early exit)
			bool hasChanged;
			do
			{
				hasChanged = false;
				for (size_t voroFaceIndex = 0u; voroFaceIndex < voroCellFaceCount; voroFaceIndex++)
				{
					//If this face already has it's connectivity checked -> move on
					if (faceState[voroFaceIndex] != 0) continue;

					outerVertexMatched = false;
					innerVertexMatched = false;
					VoronoiFace& vf = voroCellFaces[voroFaceIndex];
					size_t vertexCount = vf.vertices.size();
					for (size_t v = 0u; v < vertexCount; v++)
					{
						if (outerVerticesUnique.count(vf.vertices[v]) == 1u)
						{
							outerVertexMatched = true;
						}
						if (innerVerticesUnique.count(vf.vertices[v]) == 1u)
						{
							innerVertexMatched = true;
						}
					}

					if (outerVertexMatched && innerVertexMatched)
					{
						faceState[voroFaceIndex] = 2;
						hasChanged = true;
					}
					else if (outerVertexMatched)
					{
						faceState[voroFaceIndex] = 1;
						outerVerticesUnique.insert(vf.vertices.begin(), vf.vertices.end());
						hasChanged = true;
					}
					else if (innerVertexMatched)
					{
						faceState[voroFaceIndex] = 3;
						innerVerticesUnique.insert(vf.vertices.begin(), vf.vertices.end());
						hasChanged = true;
					}
				}
			} while (hasChanged == true);

			//Now we know which faces are on the inside and need to be added
			for (size_t v = 0u; v < voroCellFaceCount; v++)
			{
				if (faceState[v] != 3) continue;
				VoronoiFace& vf = voroCellFaces[v];

				componentCurrentFace.clear();
				//We can add the faceHandles here, but the order is important.
				size_t vertexCount = vf.vertices.size();
				componentCurrentFace.resize(vertexCount);
				//Check the vertex-coordinates of the face with the vertices of the other component -> In case the vertex has already been added to avoid duplicates!
				for (MeshPolygon::VertexIter vIter = component.vertices_begin(); vIter != component.vertices_end(); vIter++)
				{
					MeshPolygon::VertexHandle vertexHandle = *vIter;
					Vector3 componentVertex = Vector3(component.point(vertexHandle));

					for (size_t cellVertexId = 0u; cellVertexId < vertexCount; cellVertexId++)
					{
						//Simple check if we found a vertexHandle for this vertex already
						if (componentCurrentFace[cellVertexId].idx() != -1) continue;

						Vector3 cellVertex = vf.vertices[cellVertexId];
						if (cellVertex == componentVertex)
						{
							componentCurrentFace[cellVertexId] = vertexHandle;
						}
					}
				}

				//Check if we found a VertexHandle for every faceVertex.
				for (size_t cellVertexId = 0u; cellVertexId < vertexCount; cellVertexId++)
				{
					if (componentCurrentFace[cellVertexId].idx() == -1)
					{
						//if this happens we need to add this vertex to the otherComponent. Otherwise we can't add the face.
						Vector3 cellVertex = vf.vertices[cellVertexId];
						MeshPolygon::Point point = MeshPolygon::Point(cellVertex.x, cellVertex.y, cellVertex.z);
						componentCurrentFace[cellVertexId] = component.add_vertex(point);
					}
				}

				//Finally add the face
				std::vector<ICE::FRAC::Vector3> verticesCopy = std::vector<ICE::FRAC::Vector3>(vf.vertices);

				SortVerticesOnFaceCCW(verticesCopy, componentCurrentFace, vf.normal);
				MeshPolygon::FaceHandle faceHandle = component.add_face(componentCurrentFace);
				if (faceHandle.idx() == -1)
				{
					std::reverse(componentCurrentFace.begin(), componentCurrentFace.end());
					faceHandle = component.add_face(componentCurrentFace);
				}

				if (faceHandle.idx() != -1)
				{
					MeshPolygon::Normal faceNormal = MeshPolygon::Normal(vf.normal.x, vf.normal.y, vf.normal.z);
					component.set_normal(faceHandle, faceNormal);
				}
			}

		} //end for(modifiedSurfaceComponentes)
#endif
		return compound;
	}
}

namespace ICE
{
	namespace
	{
		Vec3 colorBroken = Vec3(0.05f, 0.8f, 0.05f);
		Vec3 colorRippedOff = Vec3(0.05f, 0.05f, 0.8f);
	}

	namespace FRAC
	{
		std::vector<Compound> FractureCompoundAtLocation(Compound& compound, Vec3 impactLocation, float impactRadius)
		{
			Transform compoundTransform = compound.GetTransform();
			PHYS::CollisionData compoundColData = compound.GetCollisionData();
			Vec3 compoundLocation = compoundTransform.GetPosition();
			Vec3 localImpactLocation = impactLocation - compoundLocation;

			float timePattern = 0.0f, timeFracture = 0.0;
			Timer timer = Timer();
			/*	Voronoi data that is used in all the checks	*/	
			int maxParticleCount = 0;
			timer.Tick();
			voro::container* fracturePattern = CreateFracturePattern(compoundColData.aabb.min, compoundColData.aabb.max, localImpactLocation, maxParticleCount);
			timePattern = timer.Tick();
			FILE::WriteToFile("Filename: " + compound.GetFilename() + " Time taken in seconds: " + std::to_string(timePattern));

			std::vector<int> indexToPID = std::vector<int>();
			std::vector<voro::voronoicell_neighbor> voroCells = std::vector<voro::voronoicell_neighbor>();
			std::vector<ICE::FRAC::Vector3> voroParticleCenters = std::vector<ICE::FRAC::Vector3>();
			std::vector<std::vector<ICE::FRAC::VoronoiFace>> voroCellFaces = std::vector<std::vector<ICE::FRAC::VoronoiFace>>();
			int validParticles = 0;
			PrepareVoroCellFaces(fracturePattern, maxParticleCount, indexToPID, voroCells, voroParticleCenters, voroCellFaces, validParticles);

			/*		*/

			std::vector<Component> componentsInRange = compound.ComponentsInFractureRange(impactLocation, impactRadius);
			std::vector<Component> fracturedComponents = std::vector<Component>();
			fracturedComponents.reserve(componentsInRange.size() * 10u);

			size_t componentsInRangeCount = componentsInRange.size();
			timer.Tick();
			for (size_t i = 0u; i < componentsInRangeCount; i++)
			{
				Component& comp = componentsInRange[i];
				PHYS::CollisionData colData = comp.GetCollisionData();

				std::vector<MeshPolygon> polygons = ComputeFracture(comp.GetPolyMesh() , impactLocation, fracturePattern, maxParticleCount, validParticles, indexToPID, voroCells, voroParticleCenters, voroCellFaces);
				
				size_t polygonCount = polygons.size();
				if (polygonCount == 0u)
				{
					compound.AddComponent(comp);
					continue;
				}

				for(MeshPolygon& polyMesh : polygons)
				{
					if (!polyMesh.is_valid_handle(OpenMesh::FaceHandle(0)))
					{
						continue;
					}

					Component fracturedComponent = Component(polyMesh, colorRippedOff);
					if (fracturedComponent.IsInRange(compoundLocation, impactLocation, impactRadius))
					{
						fracturedComponents.push_back(fracturedComponent);
					}
					else
					{
						fracturedComponent = Component(polyMesh, colorBroken);
						fracturedComponent.SetFilename(compound.GetFilename());
						compound.AddComponent(fracturedComponent);
					}
				}
			}
			timeFracture = timer.Tick();
			FILE::WriteToFile("Filename: " + compound.GetFilename() + " Time taken in seconds: " + std::to_string(timeFracture));

			size_t fracturedComponentCount = fracturedComponents.size();
			std::vector<Compound> newCompounds = std::vector<Compound>();
			newCompounds.reserve(fracturedComponentCount);

			//compoundLocation -= Vec3(5.0f, 0.0f, 0.0f);
			for (size_t i = 0u; i < fracturedComponentCount; i++)
			{
				Vec3 newLocation = compoundLocation;
				//newlocation -= Vec3(-2.0f, 0.0f, 0.0f);
				//newLocation += Vec3(GetRandom(0.0f, 0.5f), GetRandom(0.1f, 1.0f), GetRandom(0.1f, 1.0f));
				newCompounds.push_back(Compound(fracturedComponents[i], newLocation));
			}

			delete fracturePattern;

			return newCompounds;
		}
	}
}