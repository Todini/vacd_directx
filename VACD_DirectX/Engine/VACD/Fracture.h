#pragma once
#include "..\Utility\Vec.h"
#include "Compound.h"

namespace voro
{
	class container;
}

namespace ICE
{
	namespace FRAC
	{
		/*	Fractures the given compound with the given variables.
			Returns all new compounds.
		*/
		std::vector<Compound> FractureCompoundAtLocation(Compound& compound, Vec3 impactLocation, float impactRadius);


	}
}