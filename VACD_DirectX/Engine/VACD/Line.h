///////////////////////////////////////////////////////////////////////////////
// Line.h
// ======
// class to construct a line with parametric form
// Line = p + aV (a point and a direction vector on the line)
//
// Dependency: Vector2, Vector3
//
//  AUTHOR: Song Ho Ahn (song.ahn@gmail.com)
// CREATED: 2015-12-18
// UPDATED: 2016-04-14
///////////////////////////////////////////////////////////////////////////////
//
//	Adapted by: Michael Knett
//	Date: 2018-07-11
//
//
#pragma once
#include <cmath>
#include "Vectors.h"
#include "..\VACD\MeshPolygon.h"

namespace ICE
{
	namespace FRAC
	{
		class Line
		{
		public:
			Line() : direction(Vector3(0, 0, 0)), point(Vector3(0, 0, 0)) {}
			Line(const Vector3& p0, const Vector3& p1) : direction(0.0, 0.0, 0.0), point(p0)
			{
				direction.x = p1[0] - p0[0];
				direction.y = p1[1] - p0[1];
				direction.z = p1[2] - p0[2];
				direction = direction.normalize();
			}
			Line(MeshPolygon::Point p0, MeshPolygon::Point p1) : direction(0.0, 0.0, 0.0), point(p0)
			{
				direction.x = p1[0] - p0[0];
				direction.y = p1[1] - p0[1];
				direction.z = p1[2] - p0[2];
				direction = direction.normalize();
			}
			~Line() {};

			// find intersect point with other line
			Vector3 intersect(const Line& line);
			Vector3 intersect(const Line& line, PRECISION& outAlpha);
			bool isIntersected(const Line& line);

			Vector3 direction;
			Vector3 point;
		};
	}
}