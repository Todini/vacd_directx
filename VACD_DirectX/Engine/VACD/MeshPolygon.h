#pragma once

#undef min
#undef max
#define OM_STATIC_BUILD 1
/* The following comment is found in MeshIO: */
// Issue warning if MeshIO was not included before Mesh Type
// Nobody knows why this order was enforced.
/* ... that is the only reason why this is included here */
#include <OpenMesh\Core\IO\MeshIO.hh>

#include <OpenMesh\Core\Mesh\PolyMesh_ArrayKernelT.hh>
#include <OpenMesh\Core\Mesh\TriMesh_ArrayKernelT.hh>
#include <OpenMesh\Core\Mesh\Attributes.hh>

namespace ICE
{
	namespace FRAC
	{

		//define traits for OpenMesh
		struct PolygonTraits : public OpenMesh::DefaultTraits
		{
			//use float coordinates
			typedef OpenMesh::Vec3f Point;
			typedef OpenMesh::Vec3f Normal;
			typedef OpenMesh::Vec4f Color;

			//typedef OpenMesh::Vec2f  TexCoord2D;
			/// The default texture index type
			//typedef unsigned int TextureIndex;

			/// The default color type is OpenMesh::Vec3uc.
			//typedef OpenMesh::Vec3uc Color;

			//define vertex attributes
			VertexAttributes(OpenMesh::Attributes::Normal |
				OpenMesh::Attributes::Color |
				OpenMesh::Attributes::Status);

			FaceAttributes(OpenMesh::Attributes::Normal |
				OpenMesh::Attributes::Status);

			HalfedgeAttributes(OpenMesh::Attributes::None);
			EdgeAttributes(OpenMesh::Attributes::Status);

			VertexTraits{};
			HalfedgeTraits{};
			EdgeTraits{};
			FaceTraits{};
		};

		typedef OpenMesh::PolyMesh_ArrayKernelT<PolygonTraits> MeshPolygon;

		struct VertexData
		{
			MeshPolygon::Point pos;
			MeshPolygon::Normal normal;
			MeshPolygon::TexCoord2D texCoord;
			MeshPolygon::TextureIndex texIndex;
		};
	}
}