///////////////////////////////////////////////////////////////////////////////
// Plane.h
// =======
// class for a 3D plane with normal vector (a,b,c) and a point (x0,y0,z0)
// ax + by + cz + d = 0,  where d = -(ax0 + by0 + cz0)
//
// NOTE:
// 1. The default plane is z = 0 (a plane on XY axis)
// 2. The distance is the length from the origin to the plane
//
// Dependencies: Vector3, Line
//
//  AUTHOR: Song Ho Ahn (song.ahn@gmail.com)
// CREATED: 2016-01-19
// UPDATED: 2016-04-14
///////////////////////////////////////////////////////////////////////////////
//
//	Adapted by: Michael Knett
//	Date: 2018-07-11
//
//
#pragma once

#include "Vectors.h"
#include "Line.h"

namespace ICE
{
	namespace FRAC
	{
		class Plane
		{
		public:
			// ctor/dtor
			Plane();
			Plane(PRECISION a, PRECISION b, PRECISION c, PRECISION d);          // 4 coeff of plane equation
			Plane(const Vector3& normal, const Vector3& point); // a point on the plane and normal vector
			~Plane() {}

			void set(PRECISION a, PRECISION b, PRECISION c, PRECISION d);
			void set(const Vector3& normal, const Vector3& point);  // set with a point on the plane and normal

			PRECISION getDistance() const { return distance; };         // return distance from the origin
			PRECISION getDistance(const Vector3& point);                // return distance from the point

			// convert plane equation with unit normal vector
			void normalize();

			// for intersection
			Vector3 intersect(const Line& line) const;              // intersect with a line
			Line intersect(const Plane& plane) const;               // intersect with another plane
			bool isIntersected(const Line& line) const;
			bool isIntersected(const Plane& plane) const;


			Vector3 normal;     // normal vector of a plane
			PRECISION d;            // coefficient of constant term: d = -(a*x0 + b*y0 + c*z0)
			PRECISION normalLength; // length of normal vector
			PRECISION distance;     // distance from origin to plane
		};
	}
}