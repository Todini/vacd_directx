///////////////////////////////////////////////////////////////////////////////
// Vectors.h
// =========
// 2D/3D/4D vectors
//
//  AUTHOR: Song Ho Ahn (song.ahn@gmail.com)
// CREATED: 2007-02-14
// UPDATED: 2016-04-04
//
// Copyright (C) 2007-2013 Song Ho Ahn
///////////////////////////////////////////////////////////////////////////////
//
//	Adapted by: Michael Knett
//	Date: 2018-07-11
//
//

#pragma once
#include <cmath>
#include "MeshPolygon.h"

#define FRACTURE_USE_FLOAT 0
typedef double PRECISION;

namespace
{
	const PRECISION EPSILON = 0.000001;
}

namespace ICE
{
	namespace FRAC
	{

		///////////////////////////////////////////////////////////////////////////////
		// 3D vector
		///////////////////////////////////////////////////////////////////////////////
		struct Vector3
		{
			PRECISION x;
			PRECISION y;
			PRECISION z;

			// ctors
			Vector3() : x(0), y(0), z(0) {};
			Vector3(PRECISION x, PRECISION y, PRECISION z) : x(x), y(y), z(z) {};
			Vector3(MeshPolygon::Point point) : x(point[0]), y(point[1]), z(point[2]) {};

			// utils functions
			void        set(PRECISION x, PRECISION y, PRECISION z);
			PRECISION       length() const;                         //
			PRECISION       lengthSq() const;                         //
			PRECISION       distance(const Vector3& vec) const;     // distance between two vectors
			PRECISION       distanceSq(const Vector3& vec) const;     // distance between two vectors squared
			PRECISION       angle(const Vector3& vec) const;        // angle between two vectors
			Vector3&    normalize();                            //
			PRECISION       dot(const Vector3& vec) const;          // dot product
			Vector3     cross(const Vector3& vec) const;        // cross product
			bool        equal(const Vector3& vec, PRECISION e) const; // compare with epsilon

			// operators
			Vector3     operator-() const;                      // unary operator (negate)
			Vector3     operator+(const Vector3& rhs) const;    // add rhs
			Vector3     operator-(const Vector3& rhs) const;    // subtract rhs
			Vector3&    operator+=(const Vector3& rhs);         // add rhs and update this object
			Vector3&    operator-=(const Vector3& rhs);         // subtract rhs and update this object
			Vector3     operator*(const PRECISION scale) const;     // scale
			Vector3     operator*(const Vector3& rhs) const;    // multiplay each element
			Vector3&    operator*=(const PRECISION scale);          // scale and update this object
			Vector3&    operator*=(const Vector3& rhs);         // product each element and update this object
			Vector3     operator/(const PRECISION scale) const;     // inverse scale
			Vector3&    operator/=(const PRECISION scale);          // scale and update this object
			bool        operator==(const Vector3& rhs) const;   // exact compare, no epsilon
			bool        operator!=(const Vector3& rhs) const;   // exact compare, no epsilon
			bool        operator<(const Vector3& rhs) const;    // comparison for sort
			PRECISION       operator[](int index) const;            // subscript operator v[0], v[1]
			PRECISION&      operator[](int index);                  // subscript operator v[0], v[1]

			friend Vector3 operator*(const PRECISION a, const Vector3 vec);
		};

		// fast math routines from Doom3 SDK
		inline PRECISION invSqrt(PRECISION x)
		{
			PRECISION xhalf = 0.5f * x;
			int i = *(int*)&x;          // get bits for PRECISIONing value
			i = 0x5f3759df - (i >> 1);    // gives initial guess
			x = *(PRECISION*)&i;            // convert bits back to PRECISION
			x = x * (1.5f - xhalf*x*x); // Newton step
			return x;
		}

		///////////////////////////////////////////////////////////////////////////////
		// inline functions for Vector3
		///////////////////////////////////////////////////////////////////////////////
		inline Vector3 Vector3::operator-() const {
			return Vector3(-x, -y, -z);
		}

		inline Vector3 Vector3::operator+(const Vector3& rhs) const {
			return Vector3(x + rhs.x, y + rhs.y, z + rhs.z);
		}

		inline Vector3 Vector3::operator-(const Vector3& rhs) const {
			return Vector3(x - rhs.x, y - rhs.y, z - rhs.z);
		}

		inline Vector3& Vector3::operator+=(const Vector3& rhs) {
			x += rhs.x; y += rhs.y; z += rhs.z; return *this;
		}

		inline Vector3& Vector3::operator-=(const Vector3& rhs) {
			x -= rhs.x; y -= rhs.y; z -= rhs.z; return *this;
		}

		inline Vector3 Vector3::operator*(const PRECISION a) const {
			return Vector3(x*a, y*a, z*a);
		}

		inline Vector3 Vector3::operator*(const Vector3& rhs) const {
			return Vector3(x*rhs.x, y*rhs.y, z*rhs.z);
		}

		inline Vector3& Vector3::operator*=(const PRECISION a) {
			x *= a; y *= a; z *= a; return *this;
		}

		inline Vector3& Vector3::operator*=(const Vector3& rhs) {
			x *= rhs.x; y *= rhs.y; z *= rhs.z; return *this;
		}

		inline Vector3 Vector3::operator/(const PRECISION a) const {
			return Vector3(x / a, y / a, z / a);
		}

		inline Vector3& Vector3::operator/=(const PRECISION a) {
			x /= a; y /= a; z /= a; return *this;
		}

		inline bool Vector3::operator==(const Vector3& rhs) const {
			return fabs(x - rhs.x) < EPSILON && fabs(y - rhs.y) < EPSILON && fabs(z - rhs.z) < EPSILON;
			//return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
		}

		inline bool Vector3::operator!=(const Vector3& rhs) const {
			return (x != rhs.x) || (y != rhs.y) || (z != rhs.z);
		}

		inline bool Vector3::operator<(const Vector3& rhs) const {
			if (x < rhs.x) return true;
			if (x > rhs.x) return false;
			if (y < rhs.y) return true;
			if (y > rhs.y) return false;
			if (z < rhs.z) return true;
			if (z > rhs.z) return false;
			return false;
		}

		inline PRECISION Vector3::operator[](int index) const {
			return (&x)[index];
		}

		inline PRECISION& Vector3::operator[](int index) {
			return (&x)[index];
		}

		inline void Vector3::set(PRECISION x_, PRECISION y_, PRECISION z_) {
			this->x = x_; this->y = y_; this->z = z_;
		}

		inline PRECISION Vector3::length() const {
			return sqrt(x*x + y*y + z*z);
		}
		inline PRECISION Vector3::lengthSq() const
		{
			return x*x + y*y + z*z;
		}

		inline PRECISION Vector3::distance(const Vector3& vec) const {
			return sqrt((vec.x - x)*(vec.x - x) + (vec.y - y)*(vec.y - y) + (vec.z - z)*(vec.z - z));
		}
		inline PRECISION Vector3::distanceSq(const Vector3& vec) const {
			return (vec.x - x)*(vec.x - x) + (vec.y - y)*(vec.y - y) + (vec.z - z)*(vec.z - z);
		}

		inline PRECISION Vector3::angle(const Vector3& vec) const {
			// return angle between [0, 180]
			PRECISION l1 = this->length();
			PRECISION l2 = vec.length();
			PRECISION d = this->dot(vec);
			PRECISION angle = acos(d / (l1 * l2)) / 3.141592f * 180.0f;
			return angle;
		}

		inline Vector3& Vector3::normalize()
		{
			if (abs(x) < EPSILON) x = 0.0f;
			if (abs(y) < EPSILON) y = 0.0f;
			if (abs(z) < EPSILON) z = 0.0f;

			//@@const PRECISION EPSILON = 0.000001f;
			PRECISION xxyyzz = x*x + y*y + z*z;
			//@@if(xxyyzz < EPSILON)
			//@@    return *this; // do nothing if it is ~zero vector

			//PRECISION invLength = invSqrt(xxyyzz);
			PRECISION invLength = 1.0f / sqrt(xxyyzz);
			x *= invLength;
			y *= invLength;
			z *= invLength;
			return *this;
		}

		inline PRECISION Vector3::dot(const Vector3& rhs) const {
			return (x*rhs.x + y*rhs.y + z*rhs.z);
		}

		inline Vector3 Vector3::cross(const Vector3& rhs) const {
			return Vector3(y*rhs.z - z*rhs.y, z*rhs.x - x*rhs.z, x*rhs.y - y*rhs.x);
		}

		inline bool Vector3::equal(const Vector3& rhs, PRECISION epsilon) const {
			return fabs(x - rhs.x) < epsilon && fabs(y - rhs.y) < epsilon && fabs(z - rhs.z) < epsilon;
		}

		inline Vector3 operator*(const PRECISION a, const Vector3 vec) {
			return Vector3(a*vec.x, a*vec.y, a*vec.z);
		}
	}
}