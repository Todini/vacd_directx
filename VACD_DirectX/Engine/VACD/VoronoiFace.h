#pragma once
#include "MeshPolygon.h"
#include "Plane.h"

namespace ICE
{
	namespace FRAC
	{
		struct ClipVertex
		{
			MeshPolygon::VertexHandle vertexHandle;
			uint32_t index;
			bool wasVisited;
			std::vector<uint32_t> edgeIndices;
		};

		struct VoronoiFace
		{
			//The vertices of the face
			std::vector<Vector3> vertices;
			Vector3 normal;
			//Computed plane for this face
			Plane plane;
			//If a voronoi-face clipped mutliple faces of the mesh, we need to know that to create a new face with all those new vertices. Otherwise there would be an opening.
			std::vector<ClipVertex> clippingVertices;

			//Vertices that would make up the cut convex-face -> In this case not all of the cell-face would be added
			std::vector<Vector3> clippedFaceInsideVertices;
			std::vector<MeshPolygon::VertexHandle> clippedFaceInsideVertexHandles;
			std::vector<Vector3> clippedFaceOutsideVertices;
		};

		ClipVertex& AddClipVertex(int vertexHandleIdx, std::vector<ClipVertex>& clipVertices)
		{
			size_t clipVertexCount = clipVertices.size();

			for (size_t i = 0u; i < clipVertexCount; i++)
			{
				if (clipVertices[i].vertexHandle.idx() == vertexHandleIdx)
				{
					return clipVertices[i];
				}
			}

			ClipVertex c;
			c.vertexHandle = MeshPolygon::VertexHandle(vertexHandleIdx);
			c.index = static_cast<uint32_t>(clipVertexCount);
			c.wasVisited = false;
			c.edgeIndices = std::vector<uint32_t>();
			c.edgeIndices.reserve(4u);

			clipVertices.push_back(c);
			return clipVertices[clipVertexCount];
		}
	}
}