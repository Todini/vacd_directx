#pragma once
#include "Window.h"
#include <windows.h>
#include <assert.h>

#include "File\MeshLoader.h"
#include "File\TextureLoader.h"
#include "Rendering\MaterialManager.h"

#include "Timer.h"

#include "World.h"

#include "..\TP\voro++\voro++.hh"

static const float MAX_DELTA = 1.0f / 60.0f;

namespace ICE
{
	namespace SYSTEM
	{
		Window* g_Window = nullptr;

		namespace
		{
			static const bool FULLSCREEN = false;
			static const bool VSYNC = false;

			static const float DISTANCE_NEAR = 0.1f;
			static const float DISTANCE_FAR = 500.0f;

			static const uint32_t MULTISAMPLING_COUNT = 1u;
			static const uint32_t MULTISAMPLING_QUALITY = 0u;
		}

		static LRESULT CALLBACK WindowCallback(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
		{
			switch (umessage)
			{
				// Check if the window is being destroyed.
				case WM_DESTROY:
				{
					PostQuitMessage(0);
					return 0;
				}

				// Check if the window is being closed.
				case WM_CLOSE:
				{
					PostQuitMessage(0);
					return 0;
				}
				default:
				{
					return DefWindowProc(hwnd, umessage, wparam, lparam);

					/*
					switch (umsg)
					{
					// Check if a key has been pressed on the keyboard.
					case WM_KEYDOWN:
					{
					// If a key is pressed send it to the input object so it can record that state.
					m_input->KeyDown((unsigned int)wparam);
					return 0;
					}

					// Check if a key has been released on the keyboard.
					case WM_KEYUP:
					{
					// If a key is released then send it to the input object so it can unset the state for that key.
					m_input->KeyUp((unsigned int)wparam);
					return 0;
					}

					// Any other messages send to the default message handler as our application won't make use of them.
					default:
					{
					return DefWindowProc(hwnd, umsg, wparam, lparam);
					}
					}
					*/
				}
			}
		}
	
		Window::Window()
		{
			assert(g_Window == nullptr);
			g_Window = this;

			//Get the instance of this application
			m_hinstance = GetModuleHandle(nullptr);
			m_applicationName = "Realtime Voronoi Fracture";

			//Default settings
			WNDCLASSEX wc = {};
			wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
			wc.lpfnWndProc = WindowCallback;
			wc.cbClsExtra = 0;
			wc.cbWndExtra = 0;
			wc.hInstance = m_hinstance;
			//wc.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
			//wc.hIconSm = wc.hIcon;
			wc.hCursor = LoadCursor(0, IDC_ARROW);
			wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
			wc.lpszMenuName = nullptr;
			wc.lpszClassName = m_applicationName;
			wc.cbSize = sizeof(WNDCLASSEX);

			//Register window class
			RegisterClassEx(&wc);

			//Get full screen values
			int32_t screenWidth = GetSystemMetrics(SM_CXSCREEN);;
			int32_t screenHeight = GetSystemMetrics(SM_CYSCREEN);;
			int32_t posX = 0;
			int32_t posY = 0;

			if (FULLSCREEN)
			{
				DEVMODE dmScreenSettings = {};
				memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
				dmScreenSettings.dmSize = sizeof(dmScreenSettings);
				dmScreenSettings.dmPelsWidth = screenWidth;
				dmScreenSettings.dmPelsHeight = screenHeight;
				dmScreenSettings.dmBitsPerPel = 32;
				dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

				//Change display settings to fullscreen
				ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
			}
			else
			{
				screenWidth = 1024;
				screenHeight = 768;

				// Place the window in the middle of the screen.
				posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
				posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
			}

			// Create the window with the screen settings and get the handle to it.
			m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName,
				WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
				posX, posY, screenWidth, screenHeight, nullptr, nullptr, m_hinstance, nullptr);

			//Bring the window up on the screen and set it as main focus
			ShowWindow(m_hwnd, SW_SHOW);
			SetForegroundWindow(m_hwnd);
			SetFocus(m_hwnd);

			//Hide the cursor
			//ShowCursor(false);

			m_screenWidth = screenWidth;
			m_screenHeight = screenHeight;

			
			//Initialize Modules that depend on the hinstance and window-handle
			m_input.Init(m_hinstance, m_hwnd);
			RENDER::GInit(screenWidth, screenHeight, m_hwnd, VSYNC, FULLSCREEN, DISTANCE_NEAR, DISTANCE_FAR, MULTISAMPLING_COUNT, MULTISAMPLING_QUALITY);
			m_renderer.Init();

			ICE::FILE::InitTextureLoader();
			ICE::RENDER::InitMaterialManager();
			ICE::FILE::InitMeshLoader();

		}
		
		Window::~Window()
		{
			assert(g_Window != nullptr);
			g_Window = nullptr;

			//Show the cursor
			ShowCursor(true);

			if (FULLSCREEN)
			{
				ChangeDisplaySettings(nullptr, 0);
			}

			//Remove the window
			DestroyWindow(m_hwnd);

			//Remove the application instance
			UnregisterClass(m_applicationName, m_hinstance);
			
			ICE::FILE::ShutdownMeshLoader();
			ICE::RENDER::ShutdownMaterialManager();
			ICE::FILE::ShutdownTextureLoader();

			m_renderer.Shutdown();
			RENDER::GShutdown();
			m_input.Shutdown();
		}

		void Window::Run()
		{
			World world;

			MSG msg = {};
			bool running = true;

			bool paused = false;
			bool nextFrame = false;

			//Tick once to update 
			Timer globalTime;
			float deltaTime = globalTime.Tick();

			//MAIN LOOP
			while (running)
			{
				while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);

					// If windows signals to end the application then exit out.
					if (msg.message == WM_QUIT)
					{
						running = false;
					}
				}

				if (m_input.IsKeyPressed(m_input.playerInputs.closeApplication))
				{
					running = false;
					return;
				}
				if (m_input.IsKeyPressedOnce(m_input.playerInputs.debugPause))
				{
					paused = !paused;
				}
				if (m_input.IsKeyPressedOnce(m_input.playerInputs.debugNextFrame))
				{
					nextFrame = true;
				}

				// ---------- Check Key-Input ------------------------

				deltaTime = globalTime.Tick();
				//Enforce a max deltaTime, for debugging
				deltaTime = (deltaTime > MAX_DELTA) ? MAX_DELTA : deltaTime;

				/*	Updating
					- Input Devices
					- World (and all objects within it, WorldState)
				*/
				m_input.Update();
				
				world.UpdateCamera(deltaTime);
				WorldState* worldState = world.GetCurrentWorldState();
				if (!paused)
				{
					worldState = world.Update(deltaTime);
				}
				else if (nextFrame == true)
				{
					worldState = world.Update(MAX_DELTA);
					nextFrame = false;
				}

				/*	Rendering
					- Renderer handles that with the help of the WorldState
				*/
				m_renderer.Render(worldState);
			}
		}
	}
}