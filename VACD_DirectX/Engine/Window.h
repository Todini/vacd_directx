#include <stdint.h>

#include "Input.h"
#include "Rendering\Renderer.h"
#include <assert.h>

#ifndef _WINDEF_
struct HINSTANCE__;
typedef HINSTANCE__* HINSTANCE;
struct HWND__;
typedef HWND__* HWND;
#endif

typedef const char* LPCSTR;

namespace ICE
{
	namespace SYSTEM
	{
		class Window;
		extern Window* g_Window;

		class Window
		{
		public:
			Window();
			~Window();

			void Run();

		private:
			Window(const Window& /*other*/) { assert(false); };
			Window& operator=(const Window& /*other*/) { assert(false); };

		private:
			LPCSTR m_applicationName;
			HINSTANCE m_hinstance;
			HWND m_hwnd;

			int32_t m_screenWidth;
			int32_t m_screenHeight;

			//Modules
			INPUT::Input m_input;
			RENDER::Renderer m_renderer;
		};
	}
}