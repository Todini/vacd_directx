#include "World.h"
#include "Rendering\Mesh.h"
#include "Rendering\Camera.h"
#include "Physics\PhysicsSystem.h"

#include "Physics\CollisionDataGenerator.h"

#include "Input.h"
#include "Utility\Raytrace.h"
#include "VACD\Fracture.h"
#include "Utility\Randomizer.h"

#include <d3d11.h>

namespace
{
	static const uint32_t MEGA_BYTE = 1024u * 1024u;

	static ICE::World* currentWorld = nullptr;



}

namespace ICE
{
	World::World()
#if USE_MEMORY_ARENA
		: m_transforms(512u)
		, m_rigidBodies(256u)
#else
		: m_transforms(new Transform[128u])
		, m_transformIndex(0u)
		, m_rigidBodies(new PHYS::RigidBody[128u])
		, m_rigidBodyIndex(0u)
#endif
		, m_worldState()
		, m_input(INPUT::g_Input)
		, m_hitMarker(nullptr)
	{
		assert(currentWorld == nullptr);
		currentWorld = this;

		m_worldState.meshesColor = m_meshesColor.GetItems();
		m_worldState.meshesTexture = m_meshesTexture.GetItems();

		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::PLANE, EMovement_t::STATIC, 0.0f, -0.5f, 0.0f));

		RENDER::MaterialInfo matInfo;
		matInfo.texName_diffuse = ETextureName_t::STONE01;
		m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::STATIC, 0.0f, -50.0f, 0.0f));
		m_worldState.actorsTexture[m_worldState.actorsTexture.size() - 1u].transform->SetScale(0.04f, 0.04f, 0.04f);
		m_hitMarker = &m_worldState.actorsTexture[m_worldState.actorsTexture.size() - 1u];

		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::DYNAMIC,  0.0f,  0.5f,  0.0f));
		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::DYNAMIC,  5.0f,  0.5f,  0.0f));
		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::DYNAMIC, -5.0f,  0.5f,  0.0f));
		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::DYNAMIC,  0.0f,  0.5f, -5.0f));
		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::DYNAMIC,  0.0f, -4.5f,  0.0f));
		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::DYNAMIC,  0.0f,  0.5f,  5.0f));

		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::DYNAMIC, 0.0f, 6.5f, 0.0f));
		//m_worldState.actorsTexture[m_worldState.actorsTexture.size() - 1u].rigidBody->ForceRotation(0.0f, 0.0f, 30.0f);
		//m_worldState.actorsTexture.push_back(Actor<VTPosNorTex>(EMeshName_t::CUBE, matInfo, EMovement_t::STATIC, 0.0f, 0.5f, 0.0f));

		m_worldState.compounds.push_back(FRAC::Compound("wall", 20u, Vec3(0.0f, 3.0f, -10.0f)));
		m_worldState.compounds.push_back(FRAC::Compound("wall", 0u, Vec3(2.0f, 3.0f, 10.0f)));

		//m_worldState.compounds.push_back(FRAC::Compound("bunny", 0u, Vec3(4.0f, 1.0f, 0.0f)));
		//m_worldState.compounds.push_back(FRAC::Compound("bunny", 34u, Vec3(5.0f, 1.0f, 0.0f)));

		//m_worldState.compounds.push_back(FRAC::Compound("HumanMesh", 0u, Vec3(-15.0f, 1.0f, 10.0f)));
		//m_worldState.compounds.push_back(FRAC::Compound("HumanMesh", 23u, Vec3(-15.0f, 1.0f, 0.0f)));

		//m_worldState.compounds.push_back(FRAC::Compound("HumanMesh", 23u, Vec3(-10.0f, 2.0f, 0.0f)));
		//m_worldState.compounds.push_back(FRAC::Compound("HumanMesh", 23u, Vec3(-15.0f, 2.0f, 0.0f)));

	}

	World::~World()
	{
		assert(currentWorld != nullptr);
		currentWorld = nullptr;

#if !USE_MEMORY_ARENA
		delete[] m_transforms;
		delete[] m_rigidBodies;
#endif
	}

	World::World(const World& /*other*/)
		: m_transforms(0u)
		, m_rigidBodies(0u)
		, m_worldState()
	{
		assert(false);
	}

	World& World::operator=(const World& other)
	{
		assert(false);
		if (this != &other)
		{

		}

		return *this;
	}

	WorldState* World::Update(float dt)
	{
		//Update Physics
		//PHYS::PUpdate(m_rigidBodies.objects, m_rigidBodies.count, dt);
		//Update Physics

		m_worldState.meshesColorCount = m_meshesColor.GetItemCount();
		m_worldState.meshesTextureCount = m_meshesTexture.GetItemCount();

		if (m_input->IsKeyPressedOnce(m_input->playerInputs.raytraceCompounds))
		{
			Vec3 hitLocation;
			int compoundIndex;
			if (RayTraceCompounds(m_worldState.camera->transform, m_worldState.compounds, hitLocation, compoundIndex))
			{
				//m_hitMarker->transform->SetPosition(hitLocation);
				
				FRAC::Compound& hitCompound = m_worldState.compounds[compoundIndex];
				//size_t fracturePatternIndex = ICE::GetRandom<size_t>(1u, m_worldState.fracturePatterns.size() - 1u);

				std::vector<FRAC::Compound> newCompounds = FractureCompoundAtLocation(hitCompound, hitLocation, 0.025f);

				if (hitCompound.IsEmpty())
				{
					m_worldState.compounds.erase(m_worldState.compounds.begin() + compoundIndex);
				}
				else
				{
					hitCompound.UpdateCollisionData();
				}

				for (FRAC::Compound& comp : newCompounds)
				{
					comp.ScaleComponent(0.98f);
				}

				m_worldState.compounds.insert(m_worldState.compounds.end(), newCompounds.begin(), newCompounds.end());
			}
		}
		else if (m_input->IsKeyPressedOnce(m_input->playerInputs.saveCompound))
		{
			Vec3 hitLocation;
			int compoundIndex;
			if (RayTraceCompounds(m_worldState.camera->transform, m_worldState.compounds, hitLocation, compoundIndex))
			{
				FRAC::Compound& hitCompound = m_worldState.compounds[compoundIndex];
				hitCompound.SaveMeshToFile("TEST");
			}
		}
		else if (m_input->IsKeyPressedOnce(m_input->playerInputs.deleteSingleCompound))
		{
			int compoundCount = static_cast<int>(m_worldState.compounds.size());

			for (int c = 0; c < compoundCount; c++)
			{
				if (m_worldState.compounds[c].HasSingleComponent())
				{
					m_worldState.compounds.erase(m_worldState.compounds.begin() + c);
					compoundCount--;
					c--;
				}
			}
		}

		return &m_worldState;
	}

	void World::UpdateCamera(float dt)
	{
		m_worldState.camera->Update(dt);
	}

	Transform* World::GetNewTransform()
	{
		return GetNewTransform(0.0f, 0.0f, 0.0f);
	}
	Transform* World::GetNewTransform(float posX, float posY, float posZ)
	{
#if USE_MEMORY_ARENA
		Transform* newT = m_transforms.objects + m_transforms.count;
		m_transforms.count++;
#else
		Transform* newT = m_transforms;
		m_transforms++;
		m_transformIndex++;
#endif

		newT->SetPosition(posX, posY, posZ);

		return newT;
	}

	PHYS::RigidBody* World::GetNewRigidBody(Transform* transform, EMeshName_t meshName, EMovement_t movement)
	{
#if USE_MEMORY_ARENA
		PHYS::RigidBody* newRigidBody = m_rigidBodies.objects + m_rigidBodies.count;
		m_rigidBodies.count++;
#else
		PHYS::RigidBody* newRigidBody = m_rigidBodies;
		m_rigidBodies++;
		m_rigidBodyIndex++;
#endif

		//TODO: Resolve MeshName to PhysicsData and copy it over!!!
		(*newRigidBody) = PHYS::RigidBody(transform, movement, PHYS::LoadCollisionData(meshName));
		return newRigidBody;
	}


	template<>
	MeshID World::CreateNewMesh<VTPosCol>(EMeshName_t meshName)
	{
		using namespace ICE::FILE;

		const MeshData& meshData = LoadMesh(meshName);

		ItemID itemID = m_meshesColor.AddItem();
		RENDER::Mesh<VTPosCol>* meshStored = m_meshesColor.Lookup(itemID);

		VTPosCol* verticesCol = new VTPosCol[meshData.vertexCount];
		for (uint32_t i = 0u; i < meshData.vertexCount; i++)
		{
			verticesCol[i].position = meshData.vertices[i].position;
			verticesCol[i].color.x = meshData.vertices[i].normal.x;
			verticesCol[i].color.y = meshData.vertices[i].normal.y;
			verticesCol[i].color.z = meshData.vertices[i].normal.z;
			verticesCol[i].color.w = 1.0f;

			if (verticesCol[i].color.x < -0.01f || verticesCol[i].color.y < -0.01f || verticesCol[i].color.z < -0.01f)
			{
				verticesCol[i].color.x *= -0.5f;
				verticesCol[i].color.y *= -0.5f;
				verticesCol[i].color.z *= -0.5f;
			}
		}

		(*meshStored) = RENDER::Mesh<VTPosCol>(verticesCol, meshData.vertexCount, meshData.indices, meshData.indexCount, sizeof(VTPosCol), 0u);

		delete[] verticesCol;

		MeshID meshID;
		meshID.id = itemID;
		return meshID;
	}

	template<>
	MeshID World::CreateNewMesh<VTPosNorTex>(EMeshName_t meshName)
	{
		using namespace ICE::FILE;

		const MeshData& meshData = LoadMesh(meshName);

		ItemID itemID = m_meshesTexture.AddItem();
		RENDER::Mesh<VTPosNorTex>* meshStored = m_meshesTexture.Lookup(itemID);

		(*meshStored) = RENDER::Mesh<VTPosNorTex>(meshData.vertices, meshData.vertexCount, meshData.indices, meshData.indexCount, sizeof(VTPosNorTex), 0u);

		MeshID meshID;
		meshID.id = itemID;
		return meshID;
	}

	template <>
	RENDER::Mesh<VTPosCol>* World::GetMesh<VTPosCol>(MeshID meshID)
	{
		return m_meshesColor.Lookup(meshID.id);
	}

	template <>
	RENDER::Mesh<VTPosNorTex>* World::GetMesh<VTPosNorTex>(MeshID meshID)
	{
		return m_meshesTexture.Lookup(meshID.id);
	}


}


namespace ICE
{
	World* GetWorld()
	{
		return currentWorld;
	}
}