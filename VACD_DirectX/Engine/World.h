#pragma once
#include "WorldState.h"

#include "Memory\MemoryHolder.h"
#include "Components\Transform.h"
#include "Utility\PackedArray.h"

#include "File\MeshLoader.h"
#include "EngineIDs.h"

#include "Physics\RigidBody.h"

namespace ICE
{
#define USE_MEMORY_ARENA 0

	namespace INPUT
	{
		class Input;
	}

	struct WorldState;

	class World
	{
	public:
		World();
		World(const World& other);
		World& operator=(const World& other);
		~World();

		inline WorldState* GetCurrentWorldState()
		{
			return &m_worldState;
		}

		WorldState* Update(float dt);
		void UpdateCamera(float dt);

		Transform* GetNewTransform();
		Transform* GetNewTransform(float posX, float posY, float posZ);

		PHYS::RigidBody* GetNewRigidBody(Transform* transform, EMeshName_t meshName, EMovement_t movement);

		template <typename VertexType>
		MeshID CreateNewMesh(EMeshName_t meshName);

		template <typename VertexType>
		RENDER::Mesh<VertexType>* GetMesh(MeshID id);

	private:
		WorldState m_worldState;
		INPUT::Input* m_input;
		Actor<VTPosNorTex>* m_hitMarker;

	private:
#if USE_MEMORY_ARENA
		MEMORY::MemoryHolder<Transform> m_transforms;
		MEMORY::MemoryHolder<PHYS::RigidBody> m_rigidBodies;
#else
		Transform* m_transforms;
		uint32_t m_transformIndex;
		PHYS::RigidBody* m_rigidBodies;
		uint32_t m_rigidBodyIndex;
#endif

		PackedArray<RENDER::Mesh<VTPosCol>> m_meshesColor;
		PackedArray<RENDER::Mesh<VTPosNorTex>> m_meshesTexture;

	};

	template <typename VertexType>
	MeshID World::CreateNewMesh(EMeshName_t meshName)
	{
		assert(false);
	}


	template <typename VertexType>
	RENDER::Mesh<VertexType>* World::GetMesh(MeshID meshID)
	{
		assert(false);
	}

}

namespace ICE
{
	World* GetWorld();
}