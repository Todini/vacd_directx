#include "WorldState.h"
#include "Rendering\Camera.h"

#include "..\TP\voro++\voro++.hh"
#include "Utility\Vec.h"
#include "Utility\Randomizer.h"

namespace
{
	static const size_t MAX_ACTOR_SIZE = 128u;
	static const ICE::Vec3 minVoroBox = ICE::Vec3(-1.0f, -1.0f, -1.0f);
	static const ICE::Vec3 maxVoroBox = ICE::Vec3(1.0f, 1.0f, 1.0f);


	voro::container* CreateFixedVoronoiFracture()
	{
		//Number of blocks the container is divided into. Needed for voro++
		const int blockSize = 6;

		//Create a voronoi container with the data above. Allocates space for 16 random points per block (for std::vectors inside)
		voro::container* con = new voro::container(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0, blockSize, blockSize, blockSize, false, false, false, 16);

		con->put(0, 0.15, 0.15, 0.15);
		con->put(1, 0.15, 0.15, -0.15);
		con->put(2, 0.15, -0.15, 0.15);
		con->put(3, 0.15, -0.15, -0.15);
		con->put(4, -0.15, 0.15, 0.15);
		con->put(5, -0.15, 0.15, -0.15);
		con->put(6, -0.15, -0.15, 0.15);
		con->put(7, -0.15, -0.15, -0.15);

		return con;
	}

	voro::container* CreateRandomVoronoiFracture(int randomPointsCount, ICE::Vec3 min, ICE::Vec3 max)
	{
		//Number of blocks the container is divided into. Needed for voro++
		const int blockSize = 6;

		//Create a voronoi container with the data above. Allocates space for 16 random points per block (for std::vectors inside)
		voro::container* con = new voro::container(min.x, max.x, min.y, max.y, min.z, max.z, blockSize, blockSize, blockSize, false, false, false, 16);

		int i = 0;
		con->put(i++, 0.0, 0.0, 0.0);

		for (; i < randomPointsCount; i++)
		{
			ICE::Vec3 randomPos = ICE::Vec3(ICE::GetRandom(min.x, max.x), ICE::GetRandom(min.y, max.y), ICE::GetRandom(min.z, max.z));
			con->put(i, randomPos.x, randomPos.y, randomPos.z);
		}

		return con;
	}
}

namespace ICE
{
	WorldState::WorldState()
		: camera(new RENDER::Camera(0.0f, 3.0f, 5.0f))
		, actorsColor()
		, actorsTexture()
		, compounds()
		, fracturePatterns()
	{
		camera->transform.AddRotationAngles(180.0f, 0.0f, 0.0f);
		actorsColor.reserve(MAX_ACTOR_SIZE);
		actorsTexture.reserve(MAX_ACTOR_SIZE);

		compounds.reserve(32u);
		fracturePatterns.reserve(8u);

		fracturePatterns.push_back(CreateFixedVoronoiFracture());
		fracturePatterns.push_back(CreateRandomVoronoiFracture(20, minVoroBox, maxVoroBox));
		fracturePatterns.push_back(CreateRandomVoronoiFracture(20, minVoroBox, maxVoroBox));
		fracturePatterns.push_back(CreateRandomVoronoiFracture(20, minVoroBox, maxVoroBox));
		fracturePatterns.push_back(CreateRandomVoronoiFracture(20, minVoroBox, maxVoroBox));
	}

	WorldState::~WorldState()
	{
		delete camera;

		for (size_t i = 0u; i < fracturePatterns.size(); i++)
		{
			delete fracturePatterns[i];
		}
	}
}