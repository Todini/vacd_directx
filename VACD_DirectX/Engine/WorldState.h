#pragma once
#include <vector>

#include "Rendering\VertexTypes.h"
#include "Actor.h"

#include "VACD\Compound.h"

namespace voro
{
	class container;
}

namespace ICE
{
	namespace RENDER
	{
		class Camera;
	}


	struct WorldState
	{
		WorldState();
		~WorldState();

		RENDER::Camera* camera;
		const RENDER::Mesh<VTPosCol>* meshesColor;
		uint32_t meshesColorCount;
		const RENDER::Mesh<VTPosNorTex>* meshesTexture;
		uint32_t meshesTextureCount;

		std::vector< Actor<VTPosCol> > actorsColor;
		std::vector< Actor<VTPosNorTex> > actorsTexture;

		std::vector<FRAC::Compound> compounds;

		std::vector<voro::container*> fracturePatterns;

	};

}