#pragma once

#define OM_STATIC_BUILD 1

#include <windows.h>


#include "Engine\Window.h"

#include "Engine\Memory\MemorySystem.h"

#include "Engine\Utility\PackedArray.h"
#include "Engine\Rendering\Mesh.h"
#include "Engine\Rendering\VertexTypes.h"

struct Vec3
{
	Vec3()
		: x(10), y(20), z(50)
	{
	}

	~Vec3()
	{
		x = 5;
		y = 5;
		z = 5;
	}

public:
	int32_t x, y, z;
};

void TestStuff()
{
	using namespace ICE::MEMORY;

	{
		ICE::MEMORY::HeapArea heapArea = ICE::MEMORY::HeapArea(1024u);
		SimpleArena arena = SimpleArena(heapArea);

		Vec3* vec = ICE_NEW_ALIGNED((&arena), Vec3, "[LOL]", alignof(Vec3));
		vec->~Vec3();
		ICE_DELETE((&arena), vec);
	}

	{
		ICE::MEMORY::HeapArea heapArea = ICE::MEMORY::HeapArea(1024u);
		SimpleArena arena = SimpleArena(heapArea);

		Vec3* vectors = ICE_NEW_ARRAY_ALIGNED((&arena), "[LOL]", alignof(Vec3), Vec3, 5);
		//Vec3* vectors = ICE::MEMORY::AllocateArray<decltype(arena), Vec3>((&arena), alignof(Vec3), 5, ICE::MEMORY::SourceInfo());

		vectors[0].y = 100;
		vectors[1].y = 110;
		vectors[2].y = 120;
		vectors[3].y = 130;
		vectors[4].y = 140;

		ICE_DELETE_ARRAY((&arena), vectors);
	}
	
}

int WINAPI WinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, LPSTR /*cmdLine*/, int /*cmdShow*/)
{
	TestStuff();

	ICE::SYSTEM::Window baseWindow;
	baseWindow.Run();

	return 0;
}